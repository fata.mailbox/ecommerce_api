<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function get_url_api()
{
    $url_api = "https://ecommerce-semipro.uni-health.com/";

    return $url_api;
}

/*
function get_token()
{

    $data = [
        'username' => 'solutech',
        'password' => 'Admin123'
    ];

    $url = get_url_api() . 'rest/V1/integration/admin/token';

    $ch = curl_init($url);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);

    if ($error !== '') {
        throw new \Exception($error);
    };

    //$token = 'fg1uuipy1ttro5r3wnrmyi6ywo5bq8cq'; //Sample token
    //return $token;
    return json_decode($response);
}
*/

function get_token()
{

    $data = json_encode(array('username' => 'solutech','password' => 'Admin123'));
   
    $url = get_url_api() . 'rest/V1/integration/admin/token';
    
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>$data,
        CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
        ),
    ));

    $response = curl_exec($curl);
    //print_r($response); die();
    $error = curl_error($curl);
    curl_close($curl);

    if ($error !== '') {
        throw new \Exception($error);
    };

    return json_decode($response);
}

function getWhsName($whs_id)
{

    $ci = get_instance();
    $a = $ci->db->query("SELECT name from warehouse where id = '$whs_id'")->row_array();
    $name = $a['name'];
    return $name;
}

function getItemWhsId($item_id)
{

    $ci = get_instance();
    $a = $ci->db->query("SELECT warehouse_id from item where id = '$item_id'")->row_array();
    $warehouse_id = intVal($a['warehouse_id']);
    return $warehouse_id;
}

function get_item_be_by_item_id($item_id)
{

    $ci = get_instance();
    $q = $ci->db->query("SELECT * from item where id = '$item_id'");

    if($q->num_rows()>0){
        return $q->row_array();
    }else{
        return false;
    }
}

function get_member_id_from_id_fe($id_fe)
{

    $ci = get_instance();
    $a = $ci->db->query("SELECT temp_member_id from temp_member where member_id_fe = '$id_fe'")->row_array();
    $id_be = intVal($a['temp_member_id']);
    return $id_be;
}

function activity_log($key, $type, $data, $url, $response)
{

    $CI = &get_instance();

    $param['key'] = $key;
    $param['type'] = $type;
    $param['input'] = $data;
    $param['url'] = $url;
    $param['response'] = $response;
    $param['ip_address'] = get_real_ip();
    $param['created_at'] = date('Y-m-d H:i:s');
    
    $CI->load->model('MLog_ecommerce');

    $CI->MLog_ecommerce->save_log($param);

}

function get_client_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function get_real_ip()
{

    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}
