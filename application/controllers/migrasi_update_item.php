<?php

function get_url_api()
{
    //$url_api = "https://ecommerce-qa.uni-health.com/";
    //$url_api = "https://ecommerce-semipro.uni-health.com/";
    $url_api = "https://ecommerce-semipro.uni-health.com/";
    return $url_api;
}

function get_token()
{
    // $data = [
    //     'username' => 'solutechglobal',
    //     'password' => 'Admin123'
    // ];

    $data = [
        'username' => 'solutech',
        'password' => 'Admin123'
    ];

    $url = get_url_api() . 'rest/V1/integration/admin/token';

    $ch = curl_init($url);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);

    if ($error !== '') {
        throw new \Exception($error);
    };

    return json_decode($response);
}

function get_real_ip()
{

    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

$nama_svr = 'localhost';
$nama_db = 'db_soho';
$nama_usr = 'root';
$pwd_usr = '';

// $nama_svr = 'localhost';
// $nama_db = 'soho_dev_20200813';
// $nama_usr = 'admin';
// $pwd_usr = 'P@ssw0rd';


$con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
if (!$con) {
    trigger_error("Problem connecting to server");
}
$db =  mysqli_select_db($con, $nama_db);
if (!$db) {
    trigger_error("Problem selecting database");
}
//--------------------------- EOF database ------------------------//


function activity_log($key, $type, $data, $url, $response)
{

    $nama_svr = 'localhost';
    $nama_db = 'db_soho';
    $nama_usr = 'root';
    $pwd_usr = '';

    // $nama_svr = 'localhost';
    // $nama_db = 'soho_dev_20200813';
    // $nama_usr = 'admin';
    // $pwd_usr = 'P@ssw0rd';

    $con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
    if (!$con) {
        trigger_error("Problem connecting to server");
    }
    $db =  mysqli_select_db($con, $nama_db);
    if (!$db) {
        trigger_error("Problem selecting database");
    }

    $qinsLog = "
    insert into log_api_ecommerce_update_item (key,type,input,url,response,ip_address,created_at)
    values
    ('" . $key . "','" . $type . "','" . $data . "','" . $url . "','" . $response . "','127.0.0.1','" . date('Y-m-d H:i:s') . "')
    ";
    mysqli_query($con, $qinsLog) or die(mysqli_error($con));
}

$query_stock = "SELECT
                sum( b.qty ) AS qty, CASE When a.sales='Yes' Then 4 When a.sales='No' Then 1 End as visibility, weight/1000 as weight,
                a.id,
                a.product_id_fe,
                a.NAME 
                FROM
                item a
                JOIN stock b ON b.item_id = a.id 
                GROUP BY
                b.item_id";

$queryData    = mysqli_query($con, $query_stock) or die(mysqli_error($con));

// var_dump($queryData); die();

echo "start retrive data \n";

$xx = 1;
while ($row = mysqli_fetch_array($queryData, MYSQLI_ASSOC)) {

    $item_id        = $row['id'];
    $product_id_fe  = $row['product_id_fe'];

    if (!empty($product_id_fe)) {

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/products/' . $row['id'];
        $ch = curl_init($url);

        $product_detail = array("id" => $row['product_id_fe'], "visibility" => $row['visibility'], "weight" => $row['weight'], "extension_attributes" => array("stock_item" => array("qty" => $row['qty'])));
        $data_post = array("product" => $product_detail);
        $post = json_encode($data_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        //echo json_encode(json_decode($result));
        //activity_log($key, 'outbound', $post, $url, $result);
    }

    //var_dump($result);
    if ($result) {
        // start activity log
        $tglsekarang = date('Y-m-d H:i:s');
        $qinsLog = "
        INSERT INTO log_api_ecommerce_update_item (`key`, `type`, `input`, `url`, `response`, `ip_address`, `created_at`) 
        VALUES ('update_stock_item', '$item_id', '$post','$url','".mysqli_real_escape_string($con,$result)."','127.0.0.1','$tglsekarang')
        ";

        mysqli_query($con,$qinsLog) or die(mysqli_error($con));

        $status_update_stock = json_decode($result, true);
        //var_dump($status_update_stock);
        if ($status_update_stock['id']) {
            echo "[" . date('Y-m-d H:i:s') . "] - No. " . $xx . " - " . $row['id'] . " - success \n";
        } else {
            echo "[" . date('Y-m-d H:i:s') . "] - No. " . $xx . " - " . $row['id'] . " - failed \n";
        }

        $xx++;
    }
}
echo 'done /n/r';
//----------------------------- database ---------------------------//
mysqli_close($con);
//--------------------------- EOF database ------------------------//
