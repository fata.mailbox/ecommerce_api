<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends CI_Controller

{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('auth_model', 'MEwallet', 'MSales', 'MOutbound', 'MSignup', 'MActivationcode'));
    $this->load->helper('date');
    $this->load->library('email');
    $this->load->helper(array('url', 'my'));

    ini_set('max_execution_time', 0);
    ini_set('memory_limit', '4096M');
  }

  function cron_cancelorderppending()
  {

    $getListTempSo = $this->MSales->getListTempSo();

    $result = 'Data Kosong';
    $id_list = '';
    //check temp_so

    if ($getListTempSo) {

      $data_param = array();
      foreach ($getListTempSo as $TempSo) {

        $item_id = $TempSo['id'];

        array_push($data_param, $TempSo['id']);

        //if ada temp_so

        if ($item_id) {

          //check temp_ref_trx_nc
          $getTempreftrxncid = $this->MSales->getTempreftrxncid($TempSo['id']);
          //if ada temp_ref_trx_nc
          if ($getTempreftrxncid > 0) {
            //loop ncm_d
            $this->MSales->cancelTempNcmD($TempSo['id']);
            // update stock ke be
            // update stock, qty_reserved (-), qty (+)
            //eof loop ncm_d

            // del temp_ncm_d
            $this->MSales->delTempNcmD($TempSo['id']);

            // del temp_ncm
            $this->MSales->delTempNcm($TempSo['id']);

            // del temp_ref_trx_nc
            $this->MSales->delTempreftrxnc($TempSo['id']);
          }

          //endif ada temp_ref_trx_nc

          //loop temp_so_d
          $this->MSales->cancelTempSo($TempSo['id']);
          // update stock, qty_reserved (-), qty (+)

          //eof loop temp_so_d

          //del temp_so_d
          $this->MSales->delTempSod($TempSo['id']);
          //del temp_so
          $this->MSales->delTempSo($TempSo['id']);
        }
      }

      $id_list = array("id_list" => $data_param);

      $result = 'Berhasil';
    }

    $url_log = base_url()  . "inbound/cron/cron_cancelorderpending";
    activity_log('Cronjob Cancel Order Pending', 'inbound', json_encode($id_list), $url_log, $result);

    echo "Berhasil";
  }

  function cron_update_stock_product()
  {

    $getListProductSales = $this->MSales->getListProductStock();

    $result = 'Data Kosong';
    $id_list = '';

    if ($getListProductSales) {

      $data_param = array();
      foreach ($getListProductSales as $item) {

        $item_id        = $item['id'];
        $product_id_fe  = $item['product_id_fe'];

        //print_r($item); die();

        array_push($data_param, $item['id']);

        if (!empty($product_id_fe)) {

          $token = get_token();

          $url = get_url_api() . 'rest/default/V1/products/' . $item['id'];
          $ch = curl_init($url);

          $product_detail = array("id" => $item['product_id_fe'], "extension_attributes" => array("stock_item" => array("qty" => $item['qty'])));
          $data_post = array("product" => $product_detail);
          $post = json_encode($data_post);

          $authorization = "Authorization: Bearer $token";

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);

          //echo json_encode(json_decode($result));
          //activity_log($key, 'outbound', $post, $url, $result);
        }
      }

      $id_list = array("id_list" => $data_param);

      $result = 'Berhasil';
    }

    $url_log = base_url()  . "inbound/cron/cron_update_stock_product";
    activity_log('Cronjob Sinkron Stock', 'inbound', json_encode($id_list), $url_log, $result);

    echo "Berhasil";
  }

  function cron_update_create_product()
  {

    $getListProduct = $this->MSales->getListProduct();

    $result = 'Data Kosong';
    $id_list = '';

    if ($getListProduct) {

      $data_param = array();
      foreach ($getListProduct as $item) {

        $item_id    = $item['id'];
        $item_name  = $item['name'];
        $topup      = $item['topup'];
        //$satuan     = $data_product['satuan'];
        //$hpp        = $data_product['hpp'];
        $sales      = $item['sales'];
        $created    = $item['created'];
        //$createdby  = $data_product['createdby'];
        //$material   = $data_product['material'];
        $price      = $item['price'];
        $price_cust = $item['pricecust'];
        $price_bv   = $item['bv'];
        $price_pv   = $item['pv'];
        $type_id    = $item['type_id'];
        $type_id_fe    = "simple";

        $is_starter_kit = ($type_id == "1" ? 1 : 0);
        $is_topup = ($topup == "Yes" ? "1" : "0");

        $visibility = ($sales == 'Yes' ? "4" : "1");

        $stock_item_attr = array(
          "is_in_stock" => true,
          "is_qty_decimal" => false,
          "show_default_notification_message" => false,
          "use_config_min_qty" => true,
          "min_qty" => 0,
          "use_config_min_sale_qty" => 1,
          "min_sale_qty" => 1,
          "use_config_max_sale_qty" => true,
          "max_sale_qty" => 10000,
          "use_config_backorders" => true,
          "backorders" => 0,
          "use_config_notify_stock_qty" => true,
          "notify_stock_qty" => 1,
          "use_config_qty_increments" => true,
          "qty_increments" => 0,
          "use_config_enable_qty_inc" => true,
          "enable_qty_increments" => false,
          "use_config_manage_stock" => true,
          "manage_stock" => true,
          "low_stock_date" => null,
          "is_decimal_divided" => false,
          "stock_status_changed_auto" => 0
        );

        $extension_attributes = array(
          "website_ids" => array(1),
          "stock_item" => $stock_item_attr
        );

        $data_post = array(
          "sku" => $item_id,
          "name" => $item_name,
          "attribute_set_id" => 4,
          "price" => $price_cust,
          "status" => 1, //1=Enable, 1=Disable 
          "visibility" => $visibility, // Default New product
          "type_id" => $type_id_fe,
          "created_at" => $created,
          "updated_at" => $created,
          "weight" => 1,
          "extension_attributes" => $extension_attributes,
          "product_links" => [],
          "options" => [],
          "tier_prices" => array(array("customer_group_id" => 1, "qty" => 1, "value" => $price, "extension_attributes" => array("website_id" => 0))),
          "custom_attributes" => array(array("attribute_code" => "is_promo_topup", "value" => $is_topup), array("attribute_code" => "is_starter_kit", "value" => $is_starter_kit), array("attribute_code" => "informasi_bv", "value" => $price_bv), array("attribute_code" => "informasi_pv", "value" => $price_pv))
        );

        $data_post = array("product" => $data_post);

        $post = json_encode($data_post, true);

        //print_r($post); die();

        $url = get_url_api() . 'rest/all/V1/products';
        $ch = curl_init($url);

        $token = get_token();

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        if (isset(json_decode($result)->id)) {
          $itemid_fe = json_decode($result)->id;

          $this->MOutbound->update_item_id_fe($item_id, $itemid_fe);
        }

        $item_id        = $item['id'];

        array_push($data_param, $item['id']);
      }

      $id_list = array("id_list" => $data_param);

      $result = 'Berhasil';
    }

    $url_log = base_url()  . "inbound/cron/cron_update_create_product";
    activity_log('Cronjob Sinkron Product', 'inbound', json_encode($id_list), $url_log, $result);

    echo "Berhasil";
  }


  function cron_insert_customer()
  {

    $memberList = $this->MSignup->getListMember();

    $result = 'Data Kosong';
    $id_list = '';

    if ($memberList) {

      $data_param = array();
      foreach ($memberList as $member) {


        $bank_id = $member['bankidfe'];
        $kota_id_fe = $member['kota_id_fe'];
        $kota_name = $member['kota'];
        //$member_id_fe = $this->getMemberIdFe($this->input->post('id'));
        $member_name = $member['nama'];
        $propinsi = $member['propinsi'];

        if ($member['sponsor_id'] != '' || $member['enroller_id'] != '') {
          $placementid = $member['sponsor_id'];
          $introducerid = $member['enroller_id'];
        } else {
          $placementid = "-";
          $introducerid = "-";
        }

        //$member_id_address = $this->getMemberAddressIdFe($this->input->post('id'));

        $firstname  =   explode(" ", $member_name)[0];
        //$last  =   explode(" ", $member_name)[1]  . ' ' . explode(" ", $member_name)[2];

        if (sizeof(explode(" ", $member_name)) > 2) {
          $last  =   explode(" ", $member_name)[1];
          for ($u = 2; $u < sizeof(explode(" ", $member_name)); $u++) {
            $last = $last  . ' ' . explode(" ", $member_name)[$u];
          }
        } else if (sizeof(explode(" ", $member_name)) == 2) {
          $last  =   explode(" ", $member_name)[1];
        } else {
          $last = ' ';
        }
        //echo $last;die();

        if ($last == ' ') {
          $lastname = '-';
        } else {
          $lastname = $last;
        }

        $password = base64_encode($member['id'] . trim($member['tgllahir'], "-"));

        $customer_array = array(
          "lastname" => $lastname,
          "firstname" => $firstname,
          "email" => $member['email'],
          "store_id" => "1",
          "website_id" => "1",
          "dob" => $member['tgllahir'],
        );

        $pr_tempatlahir = preg_replace(array("~`~", "~'~"), "", $member['tempatlahir']);
        $pr_ahliwaris = preg_replace(array("~`~", "~'~"), "", $member['ahliwaris']);
        $pr_kecamatan = preg_replace(array("~`~", "~'~"), "", $member['kecamatan']);
        $pr_kelurahan = preg_replace(array("~`~", "~'~"), "", $member['kelurahan']);
        $pr_area = preg_replace(array("~`~", "~'~"), "", $member['area']);
        $pr_norekening = preg_replace(array("~`~", "~'~"), "", $member['norekening']);

        //echo $pr_tempatlahir." - ".$pr_ahliwaris." - ".$pr_kecamatan." - ".$pr_kelurahan." - ".$pr_area."\n";die();

        $is_anonymized = array("attribute_code" => "is_anonymized", "value" => "0");
        $ca_jenis_kelamin = array("attribute_code" => "ca_jenis_kelamin", "value" => $member['jk']);
        $ca_bank = array("attribute_code" => "ca_bank", "value" => $bank_id);
        $ca_status_aktif_member = array("attribute_code" => "ca_status_aktif_member", "value" => $member['banned']);
        $ca_direct_upline_id = array("attribute_code" => "ca_direct_upline_id", "value" => $placementid);
        $ca_id_sponsor = array("attribute_code" => "ca_id_sponsor", "value" => $introducerid);
        $ca_id_anggota = array("attribute_code" => "ca_id_anggota", "value" => $member['id']);
        $ca_kode_aktivasi = array("attribute_code" => "ca_kode_aktivasi", "value" => $member['activation_code']);
        $ca_tempat_lahir = array("attribute_code" => "ca_tempat_lahir", "value" => $pr_tempatlahir);
        $ca_nomor_ktp = array("attribute_code" => "ca_nomor_ktp", "value" => $member['noktp']);
        $ca_nomor_handphone = array("attribute_code" => "ca_nomor_handphone", "value" => $member['hp']);
        $ca_ahli_waris = array("attribute_code" => "ca_ahli_waris", "value" => $pr_ahliwaris);
        $ca_nomor_rekening = array("attribute_code" => "ca_nomor_rekening", "value" => $pr_norekening);
        $ca_cabang = array("attribute_code" => "ca_cabang", "value" => $pr_area);
        $ca_tanggal_lahir = array("attribute_code" => "ca_tanggal_lahir", "value" => $member['tgllahir']);
        $ca_kecamatan = array("attribute_code" => "ca_kecamatan", "value" => $pr_kecamatan);
        $ca_kelurahan = array("attribute_code" => "ca_kelurahan", "value" => $pr_kelurahan);
        $ca_kode_pos = array("attribute_code" => "ca_kode_pos", "value" => $member['kodepos']);
        $ca_provinsi = array("attribute_code" => "ca_provinsi", "value" => $member['propinsi']);
        $ca_alamat_lengkap = array("attribute_code" => "ca_alamat_lengkap", "value" => preg_replace("[\n]", ", ", $this->input->post('alamat')));

        $customer_array['custom_attributes'] = array(
          $is_anonymized, $ca_jenis_kelamin, $ca_bank, $ca_status_aktif_member, $ca_direct_upline_id, $ca_id_sponsor, $ca_id_sponsor, $ca_id_anggota, $ca_kode_aktivasi, $ca_tempat_lahir, $ca_nomor_ktp, $ca_nomor_handphone, $ca_ahli_waris, $ca_nomor_rekening, $ca_cabang, $ca_tanggal_lahir, $ca_kecamatan, $ca_kecamatan, $ca_kelurahan, $ca_kode_pos, $ca_provinsi, $ca_alamat_lengkap
        );

        $alamatPregRaw = preg_replace(array("~`~", "~'~"), "", $member['alamat']);
        $alamatPreg = preg_replace("~[\r\n]~", ", ", $alamatPregRaw);

        $adress_arr = array(
          "defaultBilling" => "true", "defaultShipping" => "true", "firstname" => $firstname,  "lastname" => $lastname,
          "region" => array("region_code" => "$kota_name", "region_id" => $kota_id_fe, "region" => "$kota_name"), "countryId" => "ID", "postcode" => $member['kodepos'], "city" => $kota_id_fe,
          "street" => array($alamatPreg), "telephone" => $member['hp']
        );

        $customer_array['addresses'] = array($adress_arr);

        $customer['customer'] = $customer_array;
        $customer['password'] = $password;

        $data_member_post = json_encode($customer);

        //print_r($item); die();

        array_push($data_param, $member['id']);

        $token = get_token();

        $url = get_url_api() . 'rest/V2/customers';
        //$url = 'https://ecommerce-semipro.uni-health.com/rest/V2/customers';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_member_post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Create Member', 'outbound', $data_member_post, $url, $result);

        $member_fe = json_decode($result, true);

        $id_member_fe = $member_fe['id'];
        $address_id_fe = $member_fe['addresses'][0]['id'];

        $this->db->update('member', array('member_id_fe' => $id_member_fe, 'address_id_fe' => $address_id_fe), array('email' => $data_param['customer']['email']));
      
        $id_list = array("id_list" => $data_param);

        $result = 'Berhasil';
        
      }

      $url_log = base_url()  . "inbound/cron/cron_insert_customer";
      activity_log('Cronjob Create Member', 'inbound', json_encode($id_list), $url_log, $result);

      echo "Berhasil";

    }
  }
}
