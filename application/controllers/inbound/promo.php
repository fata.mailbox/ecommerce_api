<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Promo extends REST_Controller

{
  function __construct()
	{
    
    parent::__construct();
    $this->load->model(array('MPromo'));
		$this->load->helper('date');
		$this->load->library('email');
    $this->load->helper(array('url', 'my'));

	}

  function gettopupvalue_post(){
    
        $data_param = json_decode(file_get_contents('php://input') , true);
        
        $result = '';

        if(isset($data_param)){
          if (!isset($data_param['member_id'])) {

            $result = array(
              'message' => 'member_id is required'
            );

            $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupvalue";
            activity_log('Get topup Value', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 405);
          }
          if (!isset($data_param['totalamount'])) {

            $result = array(
              'message' => 'totalamount is required'
            );

            $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupvalue";
            activity_log('Get topup Value', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 405);
          }
    
    
          $member_id  = $data_param['member_id'];
          $totalamount     = $data_param['totalamount'];
          
          $get_topup = $this->MPromo->gettopupvalue($totalamount);

          //var_dump($get_topup);
              
          if ($get_topup) {

            $this->response(array(
            'member_id'     => $member_id,
            'totalamount'   => $totalamount,
            'item_id'       => $get_topup['item_id'],
            'item_name'     => $get_topup['item_name'],
            'qty'           => $get_topup['qty'],
            'price'         => $get_topup['price'],
            'periode_start' => $get_topup['periode_start'],
            'periode_end'   => $get_topup['periode_end']

            ) , 200);
           }
    
           else {
            
            $result = array(
              'message' => ' Promo is not Found'
            );

            $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupvalue";
            activity_log('Get topup Value', 'inbound', json_encode($data_param), $url_log, json_encode($result));

             $this->response($result , 405);
           }
    
        } else {

          $result = array(
            'message' => 'gagal'
          );

          $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupvalue";
          activity_log('Get topup Value', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 404);
        }
    }
    
  function gettopupdemand_post(){
    
        $data_param = json_decode(file_get_contents('php://input') , true);

        $result = '';
        if(isset($data_param)){
          if (!isset($data_param['member_id'])) {

            $result = array(
              'message' => 'member_id is required'
            );

            $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupdemand";
            activity_log('Get topup Demand', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 405);
          }
    
    
          $member_id  = $data_param['member_id'];
          
          $get_topup = $this->MPromo->gettopupbydemand($member_id);

          //var_dump($get_topup);
              
          if ($get_topup) {
            /*
            $this->response(array(
                'member_id'     => $member_id,
                'totalamount'   => $totalamount,
                'item_id'       => $get_topup['item_id'],
                'item_name'     => $get_topup['item_name'],
                'qty'           => $get_topup['qty'],
                'price'         => $get_topup['price'],
                'periode_start' => $get_topup['periode_start'],
                'periode_end'   => $get_topup['periode_end']
    
                ) , 200);
            */ 
            $result = array($get_topup);

            $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupdemand";
            activity_log('Get topup Demand', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 200);
          }
    
           else {

            $result = array(
              'message' => ' Promo is not Found'
            );

            $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupdemand";
            activity_log('Get topup Demand', 'inbound', json_encode($data_param), $url_log, json_encode($result));

             $this->response($result , 405);
           }
    
        } else {

          $result = array(
            'message' => 'gagal'
          );

          $url_log = "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/promo/gettopupdemand";
          activity_log('Get topup Demand', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result , 404);
        }
        
    }

}
