<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Member extends REST_Controller

{
  function __construct()
  {

    parent::__construct();
    $this->load->model(array('MOutbound', 'MSignup', 'MActivationcode', 'MMember', 'MSales', 'MEwallet'));
    $this->load->helper('date');
    $this->load->library('email');
    $this->load->helper(array('url', 'my'));
  }

  function signup_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      if (!isset($data_param['customer'])) {

        $result = array(
          'massage' => 'Required Data Member'
        );

        $url_log = base_url()  . "inbound/member/signup";
        activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 404);
        
      }

      if (!isset($data_param['so_header'])) {

        $result = array(
          'massage' => 'Required Data So Header'
        );

        $url_log = base_url()  . "inbound/member/signup";
        activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 404);

      }

      if (!isset($data_param['so_detail'])) {

        $result = array(
          'massage' => 'Required Data So Detail'
        );

        $url_log = base_url()  . "inbound/member/signup";
        activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 404);
        
      }

      $data_member    = $data_param['customer'];
      $data_so_header = $data_param['so_header'];
      $data_so_detail = $data_param['so_detail'];

      $createdby = $data_so_header['createdby'];
      $city_id_delv = $data_so_header['delivery_city_id'];
      $delivery_addr_id = $data_so_header['delivery_addr_id'];
      //$item_id = $data_so_detail['item_id'];
      //$qty = $data_so_detail['qty'];
      $kota_id = $data_member['city'];

      $id_kota_be = $this->MSignup->getKotaIdByIdFe($kota_id);
      $city_id_delv_be = $this->MSignup->getKotaIdByIdFe($city_id_delv);
      $reg_pos = $this->MActivationcode->getPosReg($id_kota_be);

      $res_pos_id = ($reg_pos == NULL ? 0 : intVal($reg_pos));

      $data_stc = $this->MActivationcode->getStockiestByCityId($res_pos_id, $id_kota_be);
      $whsid = $this->MSignup->getWhsidByCityId($city_id_delv_be);
      //var_dump($data_stc); die();

      $x = 1;
      $item_id = 0;

      foreach ($data_so_detail as $so_d) {

        $skit = $this->MSignup->cek_item_skit($so_d['item_id']);

        if ($skit == true) {
          $item_id = $so_d['item_id'];
          $qty = $so_d['qty'];
        }else{
          $item_id = false;
          $qty = false;
        }

        $x++;

      }

      if($item_id==false){

        $result = array(
          'massage' => 'Required Starter Kit Item'
        );

        $url_log = base_url()  . "inbound/member/signup";
        activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 404);
      
      }

      if ($data_stc <> 0) {
        $stcid = $data_stc['id'];
      } else {
        $stcid = 0;
      }
    

      $so_id_fe = $data_so_header['so_id_fe'];

      if (isset($data_member['activation_code'])) {

        $data_act = $this->MActivationcode->getActivationCode($data_member['activation_code'], $data_member['member_id']);
        if ($data_act == 0) {

          $result = array(
            'message' => 'Activation Code Not Found or Invalid'
          );

          $url_log = base_url()  . "inbound/member/signup";
          activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 405);
        }

        $data_member['activation_code']     = $data_member['activation_code'];
        $data_member['temp_member_id']      = $data_member['member_id'];
        $data_member['createdby']           = $createdby;
        unset($data_member['member_id']);

      } else {

        //var_dump($whsid); die();
        $cek_qty = $this->MSignup->cek_stock_item_whsid($item_id, $qty, $whsid);

        if ($cek_qty == false) {

          $this->MSales->update_status_order_cancel($so_id_fe);
          $this->MSales->send_message_order_cancel($so_id_fe);

          $result = array(
            'message' => 'Item ' . $item_id . ' has not enough stock on warehouse ' . getWhsName($whsid)
          );

          $url_log = base_url()  . "inbound/member/signup";
          activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        $item_detail = $this->MSales->get_detail_item_by_id($so_d['item_id']);

        if (($item_detail['price'] != $so_d['harga']) || ($item_detail['pv'] != $so_d['pv']) || ($item_detail['bv'] != $so_d['bv'])) {

          $this->MSales->update_status_order_cancel($so_id_fe);
          $this->MSales->send_message_order_cancel_price($so_id_fe);

          $product_id_fe = $this->MOutbound->get_product_id_by_item_id($so_d['item_id']);

          //Update Product
          $url =  base_url()  . "outbound/outbound_api/update_product";
          $ch = curl_init($url);

          $custom_attributes = array(
            array("attribute_code" => "informasi_pv", "value" => $item_detail['pv']),
            array("attribute_code" => "informasi_bv", "value" => $item_detail['bv']),
          );
          $data_post = array(
            "sku" => $so_d['item_id'],
            "id" => $product_id_fe,
            "price" => $item_detail['price'],
            "custom_attributes" => $custom_attributes
          );

          $data = array("product" => $data_post);

          $post = json_encode($data);

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);
          //END Update Product

          activity_log('Update Product Price', 'outbound', json_encode($data), $url, $result);

          $result = array(
            'message' => 'Prices Item ' . $so_d['item_id'] . ' not match'
          );

          $url_log = base_url()  . "inbound/sales_order/create";
          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        $data_act = $this->MActivationcode->addActivationCode($item_id, $stcid, $whsid, $createdby);

        $data_member['activation_code']      = $data_act['code'];
        $data_member['temp_member_id']       = $data_act['member_id'];
        $data_member['createdby']            = $data_act['createdby'];
      }

      //var_dump($data_member); die();

      if (isset($data_member['sponsor_id'])) {
        $data_member['sponsor_id']           = $data_member['sponsor_id'];
      } else {
        $data_member['sponsor_id']           = $stcid;
      }
      if (isset($data_member['upline_id'])) {
        $data_member['upline_id']           = $data_member['upline_id'];
      } else {
        $data_member['upline_id']            = $stcid;
      }

      if (isset($data_so_header['stockiest_id'])) {
        $data_so_header['stockiest_id'] = $data_so_header['stockiest_id'];
      } else {
        $data_so_header['stockiest_id'] = 0;
      }

      $data_member['item_id']          = $item_id;
      $data_so_header['status1']       = 'Pending';
      $data_so_header['status2']       = 'Pending';
      $data_so_header['status']        = 'Pending';
      $data_so_header['warehouse_id']  = $whsid;
      $data_so_header['member_id']     = $data_member['temp_member_id'];

      $insert_temp_mem = $this->MSignup->add_temp_member($data_member);

      $data_delivery = array(
        "member_id"     => $data_so_header['member_id'],
        "kota"          => $id_kota_be,
        "alamat"        => $data_so_header['delivery_address'],
        "pic_hp"        => $data_so_header['delivery_pic_hp'],
        "kodepos"       => $data_so_header['delivery_postcode'],
        "pic_name"      => $data_so_header['delivery_pic_name'],
        "created"       => "system",
        "created_date"  => date('Y-m-d H:i:s', now()),
        "id_fe"         => $data_so_header['delivery_addr_id']
      );

      $temp_delv_addr_id = $this->MSales->add_member_delivery($data_delivery);

      $update_addr_memmber = $this->MSignup->update_member_addr($data_so_header['member_id'], $temp_delv_addr_id);

      $data_so_header['delivery_addr_id'] = $temp_delv_addr_id;

      if ($insert_temp_mem == false) {

        $result = array(
          'message' => 'Member ID Already Exist'
        );

        $url_log = base_url()  . "inbound/member/signup";
        activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $id_so_temp = $this->MSales->add_temp_so($data_so_header);
      $so_id_fe = $data_so_header['so_id_fe'];
      $item_whsid = getItemWhsId($item_id);

      if ($item_whsid == 0) {
        $in_whsid = $whsid;
      } else {
        $in_whsid = 1;
      }

      //$data_so_detail['so_id']  = $id_so_temp;

      $x = 1;

      /*
      foreach($data_so_detail as $so_d){
        $so_d['so_id']  = $id_so_temp;
        $so_d['warehouse_id']  = $whsid;
        // var_dump($so_d); die();
        $insert_so_d_temp = $this->MSales->add_temp_so_d($so_d);
        $x++;
      }

      */

      foreach ($data_so_detail as $so_d) {

        $item_whsid = getItemWhsId($so_d['item_id']);

        if ($item_whsid == 0) {
          $in_whsid = $whsid;
        } else {
          $in_whsid = 1;
        }

        $so_d['so_id']  = $id_so_temp;
        $so_d['warehouse_id']  = $whsid;

        $cek_qty = $this->MSales->cek_stock_item_whsid($so_d['item_id'], $so_d['qty'], $in_whsid);

        if ($cek_qty == false) {

          $this->MSales->update_status_order_cancel($so_id_fe);
          $this->MSales->send_message_order_cancel($so_id_fe);
          $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);

          if ($data_so_header['payment_type'] == "3") {

            //Update Stock
            $url =  base_url()  . "outbound/outbound_api/update_product_stock";
            $ch = curl_init($url);

            $data_post = array(
              "sku" => $so_d['item_id'],
              "qty_plus" => intval($so_d['qty']),
              "qty_minus" => "0"
            );

            $data = array("stockItems" => array($data_post));
            $post = json_encode(array("data" => $data, "key" => "Item SO Refund"));

            $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            //Update Ewallet FE
            $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($data_so_header['member_id']);

            $walletdata = array(
                "customer_id" => intval($member_id_fe),
                "walletamount" => intval($data_so_header['total_harga'])
            );

            $data_deposit_post = array(
                "walletdata"        => array($walletdata),
                "walletactiontype"  => 'credit',
                "walletnote"        => 'Pembatalan id Order : ' . $so_id_fe,
            );

            $token = get_token();

            $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
            $ch = curl_init($url);

            $post = json_encode($data_deposit_post);

            $authorization = "Authorization: Bearer $token";

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            activity_log('Refund Cancel Signup', 'outbound', $post, $url, $result);

          }

          $result = array(
            'message' => 'Item ' . $so_d['item_id'] . ' has not enough stock on warehouse ' . getWhsName($whsid)
          );

          $url_log = base_url()  . "inbound/member/signup";
          activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        $item_detail = $this->MSales->get_detail_item_by_id($so_d['item_id']);

        if (($item_detail['price'] != $so_d['harga']) || ($item_detail['pv'] != $so_d['pv']) || ($item_detail['bv'] != $so_d['bv'])) {

          $this->MSales->update_status_order_cancel($so_id_fe);
          $this->MSales->send_message_order_cancel_price($so_id_fe);
          $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);

          $product_id_fe = $this->MOutbound->get_product_id_by_item_id($so_d['item_id']);

          //Update Product
          $url =  base_url()  . "outbound/outbound_api/update_product";
          $ch = curl_init($url);

          $custom_attributes = array(
            array("attribute_code" => "informasi_pv", "value" => $item_detail['pv']),
            array("attribute_code" => "informasi_bv", "value" => $item_detail['bv']),
          );
          $data_post = array(
            "sku" => $so_d['item_id'],
            "id" => $product_id_fe,
            "price" => $item_detail['price'],
            "custom_attributes" => $custom_attributes
          );

          $data = array("product" => $data_post);

          $post = json_encode($data);

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);
          //END Update Product

          activity_log('Update Product Price', 'outbound', json_encode($data), $url, $result);

          $result = array(
            'message' => 'Prices Item ' . $so_d['item_id'] . ' not match'
          );

          $url_log = base_url()  . "inbound/sales_order/create";
          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        $insert_so_d_temp = $this->MSales->add_temp_so_d($so_d);

        $this->MSignup->stock_reserved($whsid, $id_so_temp, $so_d['item_id'], $so_d['qty'], $data_so_header['createdby'], $in_whsid);

        $x++;

      }

      $insert_tail = $this->MSignup->proses_tail($id_so_temp, $in_whsid, 'system');
      $data_ncm = $this->MSignup->get_temp_ncm($id_so_temp, $data_member['temp_member_id'], $so_id_fe);
      
      if ($data_ncm != false) {

        $ncm_item = array();

        foreach ($data_ncm as $ncm) {

          $item_whsid = getItemWhsId($ncm['ncm_item_id']);

          if ($item_whsid == 0) {
            $in_whsid = $whsid;
          } else {
            $in_whsid = 1;
          }

          $cek_qty = $this->MSales->cek_stock_item_whsid($ncm['ncm_item_id'], $ncm['ncm_qty'], $in_whsid);

          if ($cek_qty == false) {

            $this->MSales->update_status_order_cancel($so_id_fe);
            $this->MSales->send_message_order_cancel($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);

            if ($data_so_header['payment_type'] == "3") {

              //Update Stock NC
              $url =  base_url()  . "outbound/outbound_api/update_product_stock";
              $ch = curl_init($url);

              $data_post = array(
                "sku" => $ncm['ncm_item_id'],
                "qty_plus" => intval($ncm['ncm_qty']),
                "qty_minus" => "0"
              );

              $data = array("stockItems" => array($data_post));
              $post = json_encode(array("data" => $data, "key" => "Item NC Refund"));

              $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

              curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
              curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
              $result = curl_exec($ch);
              curl_close($ch);

            }

            $result = array(
              'message' => 'Item ' . $ncm['ncm_item_id'] . ' has not enough stock on warehouse ' . getWhsName($whsid)
            );

            $url_log = base_url()  . "inbound/member/signup";
            activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 200);

          }

          //Update Stock NC
          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $ncm['ncm_item_id'],
            "qty_plus" => "0",
            "qty_minus" => intval($ncm['ncm_qty'])
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item NC"));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);

          $status = json_decode($result);

          $array_ncm = array(
            "so_id_be"        => $id_so_temp,
            "so_id_fe"        => $ncm['so_id_fe'],
            "ncm_id"          => $ncm['ncm_id'],
            "ncm_remark"      => 'Free of SO No. ' . $id_so_temp,
            "ncm_createddate" => $ncm['ncm_createddate'],
            "ncm_d_id"        => $ncm['ncm_d_id'],
            "ncm_item_id"     => $ncm['ncm_item_id'],
            "ncm_qty"         => $ncm['ncm_qty']
          );

          if (isset($status[0]->success) == true || isset($status[0]->success) == 1) {
            array_push($ncm_item, $array_ncm);
          } else {

            $result = array(
              'message' => 'update product stock gagal'
            );

            $this->response($result, 404);
          }
        }

        if ($insert_so_d_temp) {

          if ($data_so_header['payment_type'] == "3") {

            $so_id_temp = $this->MSales->get_so_id_temp($data_member['temp_member_id'], $data_so_header['so_id_fe']);

            $active_member = $this->MSales->member_activation($data_member['temp_member_id'], $so_id_fe, $so_id_temp);

            $data_deposit = array(
              'member_id'     => $data_member['temp_member_id'],
              'total_amount'  => $data_so_header['total_harga'],
              'total_trx'     => $data_so_header['total_harga'],
              'bankid'        => 'BCA',
              'transfer_date' =>  date('Y-m-d H:i:s', now()),
              'remark'        => 'SO Kit',
              'deposit_id_fe' => null,
            );
            
            $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
            $so_id_be = $this->MSales->payment_confirmation_order_cc($data_member['temp_member_id'], $data_so_header['so_id_fe'], $so_id_temp);

            $result = array(
              'activation_code' =>  $data_member['activation_code'],
              'member_id' =>  $data_member['temp_member_id'],
              'upline_id' => $data_member['upline_id'],
              'sponsor_id' => $data_member['sponsor_id'],
              'so_id_fe' => $so_id_fe,
              'so_id_be' =>  $so_id_be,
              'free_item' => $ncm_item
            );

            $url_log = base_url()  . "inbound/member/signup";
            activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 200);

          } else {

            $result = array(
              'activation_code' =>  $data_member['activation_code'],
              'member_id' =>  $data_member['temp_member_id'],
              'upline_id' => $data_member['upline_id'],
              'sponsor_id' => $data_member['sponsor_id'],
              'so_id_fe' => $so_id_fe,
              'so_id_be_temp' =>  $id_so_temp,
              'free_item' => $ncm_item
            );

            $url_log = base_url()  . "inbound/member/signup";
            activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 200);
          }
        } else {

          $result = array(
            'massage' => 'Register failed'
          );

          $url_log = base_url()  . "inbound/member/signup";
          activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 404);
        }

      } else {

        if ($insert_so_d_temp) {

          if ($data_so_header['payment_type'] == "3") {

            $so_id_temp = $this->MSales->get_so_id_temp($data_member['temp_member_id'], $data_so_header['so_id_fe']);

            $active_member = $this->MSales->member_activation($data_member['temp_member_id'], $so_id_fe, $so_id_temp);

            $data_deposit = array(
              'member_id'     => $data_member['temp_member_id'],
              'total_amount'  => $data_so_header['total_harga'],
              'total_trx'     => $data_so_header['total_harga'],
              'bankid'        => 'BCA',
              'transfer_date' =>  date('Y-m-d H:i:s', now()),
              'remark'        => 'SO Kit',
              'deposit_id_fe' => null,
            );
            
            $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
            $so_id_be = $this->MSales->payment_confirmation_order_cc($data_member['temp_member_id'], $data_so_header['so_id_fe'], $so_id_temp);
            
            $result = array(
              'activation_code' =>  $data_member['activation_code'],
              'member_id' =>  $data_member['temp_member_id'],
              'upline_id' => $data_member['upline_id'],
              'sponsor_id' => $data_member['sponsor_id'],
              'so_id_fe' => $so_id_fe,
              'so_id_be' =>  $so_id_be
            );

            $url_log = base_url()  . "inbound/member/signup";
            activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 200);
          } else {
            $result = array(
              'activation_code' =>  $data_member['activation_code'],
              'member_id' =>  $data_member['temp_member_id'],
              'upline_id' => $data_member['upline_id'],
              'sponsor_id' => $data_member['sponsor_id'],
              'so_id_fe' => $so_id_fe,
              'so_id_be_temp' =>  $id_so_temp
            );

            $url_log = base_url()  . "inbound/member/signup";
            activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 200);
          }
        } else {

          $result = array(
            'massage' => 'Register failed'
          );

          $url_log = base_url()  . "inbound/member/signup";
          activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 404);
        }
      }

    } else {

      $result = array(
        'massage' => 'Register failed'
      );

      $url_log = base_url()  . "inbound/member/signup";
      activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }

  }

  function sponsor_validate_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {
      $sponsor_id = $data_param['sponsor_id'];
      $upline_id  = $data_param['upline_id'];

      $array_data = $this->MSignup->sponsor_validate($sponsor_id, $upline_id);

      if (!empty($array_data)) {
        if ($array_data == 'sponsor_id_gagal') {

          $result = array(
            'message' => 'Make sure Sponsor ID you entered is correct'
          );

          $url_log = base_url()  . "inbound/member/sponsor_validate";
          activity_log('Sponsor validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 405);
        } elseif ($array_data == 'upline_id_gagal') {

          $result = array(
            'message' => 'Make sure Upline ID you entered is correct'
          );

          $url_log = base_url()  . "inbound/member/sponsor_validate";
          activity_log('Sponsor validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 405);
        } else {
          $result = array(
            'success' => 'Berhasil'
          );

          $url_log = base_url()  . "inbound/member/sponsor_validate";
          activity_log('Sponsor validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 405);
        }
      } else {

        $result = $result = array(
          'message' => 'Make sure Sponsor ID or Upline ID you entered is correct'
        );

        $url_log = base_url()  . "inbound/member/sponsor_validate";
        activity_log('Sponsor validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }
    } else {

      $result = array(
        'message' => 'gagal'
      );

      $url_log = base_url()  . "inbound/member/sponsor_validate";
      activity_log('Sponsor validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  function password_validate_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {
      $member_id = $data_param['member_id'];
      $password  = $data_param['password'];

      $array_data = $this->MSignup->password_validate($member_id, $password);

      if (!empty($array_data)) {

        $result = array(
          'massage' => 'Berhasil'
        );

        $url_log = base_url()  . "inbound/member/password_validate";
        activity_log('Password validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      } else {

        $result = array(
          'massage' => 'Make sure Member ID or Password you entered is correct'
        );

        $url_log = base_url()  . "inbound/member/password_validate";
        activity_log('Password validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }
    } else {

      $result = array(
        'message' => 'gagal'
      );

      $url_log = base_url()  . "inbound/member/password_validate";
      activity_log('Password validate', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  function memberupdateprofile_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);
    //var_dump($data_param);

    $result = '';
    if (isset($data_param)) {
      if (!isset($data_param['member_id']) || empty($data_param['member_id'])) {

        $result = array(
          'message' => 'member_id is required'
        );

        $url_log = base_url()  . "inbound/member/memberupdateprofile";
        activity_log('Member Update Profile', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }


      $member_id  = $data_param['member_id'];
      $gender     = $data_param['gender'];
      $address    = $data_param['address'];
      $city       = $data_param['city'];
      $kelurahan  = $data_param['kelurahan'];
      $kecamatan  = $data_param['kecamatan'];
      $kodepos    = $data_param['kodepos'];
      $pob        = $data_param['pob'];
      $dob        = $data_param['dob'];
      $ktp        = $data_param['ktp'];
      $hp         = $data_param['hp'];
      $email      = $data_param['email'];
      $ahliwaris  = $data_param['ahliwaris'];

      $id_kota_be = $this->MSignup->getKotaIdByIdFe($city);

      $data_confirm = array(
        "member_id" => $member_id,
        "gender" => $gender,
        "address" => $address,
        "city" => $id_kota_be,
        "kelurahan" => $kelurahan,
        "kecamatan" => $kecamatan,
        "kodepos" => $kodepos,
        "pob" => $pob,
        "dob" => $dob,
        "ktp" => $ktp,
        "hp" => $email,
        "ahliwaris" => $ahliwaris
      );

      $update_confirm = $this->MMember->updateProfile($data_confirm);

      if ($update_confirm == true) {

        $result = array(
          'message' => 'Berhasil'
        );

        $url_log = base_url()  . "inbound/member/memberupdateprofile";
        activity_log('Member Update Profile', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      } else {

        $result = array(
          'message' => ' Member ' . $member_id . ' is not Found'
        );

        $url_log = base_url()  . "inbound/member/memberupdateprofile";
        activity_log('Member Update Profile', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }
    } else {

      $result = array(
        'message' => 'gagal'
      );

      $url_log = base_url()  . "inbound/member/memberupdateprofile";
      activity_log('Member Update Profile', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  function memberupdatebank_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {
      if (!isset($data_param['member_id'])) {
        $this->response($result = array(
          'message' => 'member_id is required'
        ), 405);
      }
      if (!isset($data_param['bankid'])) {
        $this->response($result = array(
          'message' => 'bankid is required'
        ), 405);
      }
      if (!isset($data_param['no_rekening'])) {
        $this->response($result = array(
          'message' => 'no_rekening is required'
        ), 405);
      }
      if (!isset($data_param['bankarea'])) {
        $this->response($result = array(
          'message' => 'bankarea is required'
        ), 405);
      }
      if (!isset($data_param['nama_nasabah'])) {
        $this->response($result = array(
          'message' => 'nama_nasabah is required'
        ), 405);
      }


      $member_id  = $data_param['member_id'];
      $bankid     = $data_param['bankid'];
      $bankarea    = $data_param['bankarea'];
      $no_rekening       = $data_param['no_rekening'];
      $nama_nasabah  = $data_param['nama_nasabah'];

      $id_bank_be = $this->MMember->getBankIdByIdFe($bankid);

      $data_confirm = array(
        "member_id" => $member_id,
        "bankid" => $id_bank_be,
        "bankarea" => $bankarea,
        "no_rekening" => $no_rekening,
        "nama_nasabah" => $nama_nasabah
      );

      $update_confirm = $this->MMember->updateBank($data_confirm);

      if ($update_confirm == true) {
        $this->response($result = array(
          'message' => 'Berhasil'
        ), 405);
      } else {
        $this->response($result = array(
          'message' => ' Member ' . $member_id . ' is not Found'
        ), 405);
      }
    } else {
      $this->response($result = array(
        'message' => 'gagal'
      ), 404);
    }

    activity_log('Update Bank Profil', 'inbound', json_encode($data_param), '', $result);
  }

  function memberupdatenpwp_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {
      if (!isset($data_param['member_id'])) {
        $this->response($result = array(
          'message' => 'member_id is required'
        ), 405);
      }
      if (!isset($data_param['npwp_no'])) {
        $this->response($result = array(
          'message' => 'npwp_no is required'
        ), 405);
      }
      if (!isset($data_param['npwp_name'])) {
        $this->response($result = array(
          'message' => 'npwp_name is required'
        ), 405);
      }
      if (!isset($data_param['npwp_date'])) {
        $this->response($result = array(
          'message' => 'npwp_date is required'
        ), 405);
      }
      if (!isset($data_param['npwp_address'])) {
        $this->response($result = array(
          'message' => 'npwp_address is required'
        ), 405);
      }


      $member_id  = $data_param['member_id'];
      $npwp_no     = $data_param['npwp_no'];
      $npwp_name    = $data_param['npwp_name'];
      $npwp_date       = $data_param['npwp_date'];
      $npwp_address  = $data_param['npwp_address'];


      $data_confirm = array(
        "member_id" => $member_id,
        "npwp_no" => $npwp_no,
        "npwp_name" => $npwp_name,
        "npwp_date" => $npwp_date,
        "npwp_address" => $npwp_address
      );

      $update_confirm = $this->MMember->updateNpwp($data_confirm);

      if ($update_confirm == true) {
        $this->response($result = array(
          'message' => 'Berhasil'
        ), 405);
      } else {
        $this->response($result = array(
          'message' => ' Member ' . $member_id . ' is not Found'
        ), 405);
      }
    } else {
      $this->response($result = array(
        'message' => 'gagal'
      ), 404);
    }

    activity_log('Member Update WP', 'inbound', json_encode($data_param), '', $result);
  }
}
