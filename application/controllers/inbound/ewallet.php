<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Ewallet extends REST_Controller

{
  function __construct()
	{
    parent::__construct();
    $this->load->model(array('auth_model', 'MEwallet', 'MActivationcode'));
		$this->load->helper('date');
		$this->load->library('email');
    $this->load->helper(array('url', 'my'));
	}

// START Ewallet Deposit ASP 20200624
function ewalletdepositreq_post(){
    
  $data_param = json_decode(file_get_contents('php://input') , true);

  $result = '';
  if(isset($data_param)){
    if (!isset($data_param['deposit_id_fe'])) {
      $this->response($result = array(
        'message' => 'deposit_id_fe is required') , 405);
    }
    if (!isset($data_param['member_id'])) {
      $this->response($result = array(
        'message' => 'member_id is required') , 405);
    }
    if (!isset($data_param['bankid'])) {
      $this->response($result = array(
        'message' => 'bankid is required') , 405);
    }
    if (!isset($data_param['payment_type'])) {
      $this->response($result = array(
        'message' => 'payment_type is required') , 405);
    }
    if (!isset($data_param['total_amount'])) {
      $this->response($result = array(
        'message' => 'total_amount is required') , 405);
    }
    if (!isset($data_param['transfer_date'])) {
      $this->response($result = array(
        'message' => 'transfer_date is required') , 405);
    }
    if (!isset($data_param['remark'])) {
      $this->response($result = array(
        'message' => 'remark is required') , 405);
    }
  
    $deposit_id_fe  = $data_param['deposit_id_fe'];
    $member_id      = $data_param['member_id'];
    $bankid         = $data_param['bankid'];
    $payment_type   = $data_param['payment_type'];
    $total_amount   = $data_param['total_amount'];
    //$transfer_date  = $data_param['transfer_date'];
    $transfer_date  = date('Y-m-d');
    $remark         = $data_param['remark'];

    $id_bank_be = $this->MEwallet->getBankIdByIdFe($bankid);
    if (!$id_bank_be) {
      $this->response($result = array(
        'message' => 'Bank ID FE Mapping Not Found') , 405);
    }


    $data_confirm = array(
      "deposit_id_fe"   => $deposit_id_fe,
      "member_id"       => $member_id,
      "bankid"          => $id_bank_be,
      "payment_type"    => $payment_type,
      "total_amount"    => $total_amount,
      "transfer_date"   => $transfer_date,
      "remark"          => $remark
    );

    $add_confirm = $this->MEwallet->addDeposit($data_confirm);
        
    if ($add_confirm == true) {

      $deposit_id_be = $this->MEwallet->getDepositByIdFE($deposit_id_fe);

      $result = array(
        'deposit_id_fe' => $deposit_id_fe,
        'member_id'     => $member_id,
        'bankid'        => $bankid,
        'payment_type'  => $payment_type,
        'total_amount'  => $total_amount,
        'transfer_date' => $transfer_date,
        'remark'        => $remark,
        'deposit_id_be' => $deposit_id_be
      );

      $url_log = base_url()  . "inbound/ewallet/ewalletdepositreq";
      activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      if ($payment_type == "3") {

        $deposit_id_be = $this->MEwallet->deposit_confirmation(intval($deposit_id_fe), $payment_type);

      }
      
      $this->response($result , 200);

     }

     else {

      $result = array(
        'message' => ' Member ' . $member_id . ' is not Found'
      );

      $url_log = base_url()  . "inbound/ewallet/ewalletdepositreq";
      activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result , 405);

     } 

  } else {

    $result = array(
      'message' => 'gagal'
    );

    $url_log = base_url()  . "inbound/ewallet/ewalletdepositreq";
    activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));
    
    $this->response($result , 404);

  }
}

// EOF Ewallet Deposit ASP 20200624

  function balance_ewallet_post(){
	
    $data_param = json_decode(file_get_contents('php://input') , true);

        $result = '';
        if (isset($data_param)) {
          $member_id      = $data_param['member_id'];
          $periode_start  = $data_param['periode_start'];
          $periode_end    = $data_param['periode_end'];
  
          $array_data = $this->MEwallet->get_saldo_ewallet($member_id, $periode_start, $periode_end);

          if (!empty($array_data)) {

            $result = $array_data;

            $url_log = base_url()  . "inbound/ewallet/balance_ewallet";
            activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 405);
          } else {

            $result = array(
              'message' => 'gagal'
            );

            $url_log = base_url()  . "inbound/ewallet/balance_ewallet";
            activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 405);
          }
  
        } else {

          $result = array(
            'message' => 'gagal'
          );

          $url_log = base_url()  . "inbound/ewallet/balance_ewallet";
          activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result , 404);
        }
  }  
  
  function deposit_ewallet_post(){
	
    $data_param = json_decode(file_get_contents('php://input') , true);

        $result = '';
        if (isset($data_param)) {
  
          $array_data = $this->MEwallet->desposit_ewallet($data_param);

          if (!empty($array_data)) {

            $result = $array_data;

            $url_log = base_url()  . "inbound/ewallet/balance_ewallet";
            activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 405);
          } else {

            $result = array(
              'message' => 'gagal'
            );

            $url_log = base_url()  . "inbound/ewallet/balance_ewallet";
            activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result , 405);
          }
  
        } else {

          $result = array(
            'message' => 'gagal'
          );

          $url_log = base_url()  . "inbound/ewallet/balance_ewallet";
          activity_log('Ewallet Deposit Req', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result , 404);
        }
  }  

}