<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Sales_order extends REST_Controller

{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array('auth_model', 'MEwallet', 'MSales', 'MOutbound', 'MSignup', 'MActivationcode'));
    $this->load->helper('date');
    $this->load->library('email');
    $this->load->helper(array('url', 'my'));
  }

  function create_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';

    if (isset($data_param)) {

      if (!isset($data_param['so_header'])) {
        $this->response($result = array(
          'massage' => 'Required Data So Header'
        ), 404);
      }

      if (!isset($data_param['so_detail'])) {
        $this->response($result = array(
          'massage' => 'Required Data So Detail'
        ), 404);
      }

      $data_so_header = $data_param['so_header'];
      $data_so_detail = $data_param['so_detail'];

      $cek_id_fe_exsist = $this->MSales->cek_id_fe_exsist($data_so_header['so_id_fe']);

      if ($cek_id_fe_exsist==true) {
        $this->response($result = array(
          'massage' => 'SO ID FE Already Exsist'
        ), 404);
      }

      $createdby = $data_so_header['createdby'];
      $city_id = $data_so_header['delivery_city_id'];
      $created = $data_so_header['created'];
      //$item_id = $data_so_detail['item_id'];
      //$qty = $data_so_detail['qty'];

      $id_kota_be = $this->MSignup->getKotaIdByIdFe($city_id);
      $whsid = $this->MSales->getWhsidByCityId($id_kota_be);
      //var_dump($id_kota_be); die();
      $data_so_header['delivery_city_id'] = $id_kota_be;

      $data_so_header['stockiest_id'] = 0;

      $data_so_header['status1']       = 'Pending';
      $data_so_header['status2']       = 'Pending';
      $data_so_header['status']        = 'Pending';
      $data_so_header['warehouse_id']  = $whsid;

      $delv_addr_id_be = $this->MSales->get_delv_addr_be($data_so_header['delivery_addr_id']);

      if ($delv_addr_id_be == false) {

        $data_delivery = array(
          "member_id"     => $data_so_header['member_id'],
          "kota"          => $id_kota_be,
          "alamat"        => $data_so_header['delivery_address'],
          "pic_hp"        => $data_so_header['delivery_pic_hp'],
          "kodepos"       => $data_so_header['delivery_postcode'],
          "pic_name"      => $data_so_header['delivery_pic_name'],
          "created"       => "system",
          "created_date"  => date('Y-m-d H:i:s', now()),
          "id_fe"         => $data_so_header['delivery_addr_id']
        );

        $temp_delv_addr_id = $this->MSales->add_member_delivery($data_delivery);

        $data_so_header['delivery_addr_id'] = $temp_delv_addr_id;

      } else {

        $data_delivery = array(
          "member_id"     => $data_so_header['member_id'],
          "kota"          => $id_kota_be,
          "alamat"        => $data_so_header['delivery_address'],
          "pic_hp"        => $data_so_header['delivery_pic_hp'],
          "kodepos"       => $data_so_header['delivery_postcode'],
          "pic_name"      => $data_so_header['delivery_pic_name']
        );

        $update_member_delivery = $this->MSales->update_member_delivery($delv_addr_id_be, $data_delivery);

        $data_so_header['delivery_addr_id'] = $delv_addr_id_be;

      }

      $id_so_temp = $this->MSales->add_temp_so($data_so_header);

      $x = 1;

      $so_id_fe = $data_so_header['so_id_fe'];

      foreach ($data_so_detail as $so_d) {

        $cek_item_be = get_item_be_by_item_id($so_d['item_id']);

        if ($cek_item_be == false) {

          if ($data_so_header['payment_type'] == "0" || $data_so_header['payment_type'] == "3") { 

            $total_harga = intval($data_so_header['total_harga'])+intval($data_so_header['shipping_price']);
            $this->MSales->update_status_order_cancel_cc_ewallet($so_id_fe);

            if($data_so_header['payment_type'] == "0"){
              $this->MSales->refund_payment_to_ewallet($data_so_header['member_id'], $so_id_fe, $total_harga);
            }
            
            $this->MSales->send_message_order_cancel_item_notfound($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);

          }else{

            $this->MSales->update_status_order_cancel($so_id_fe);
            $this->MSales->send_message_order_cancel_item_notfound($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);
          }

          $result = array(
            'message' => 'Item ' . $so_d['item_id'] . ' not found'
          );

          $url_log = base_url()  . "inbound/sales_order/create";
          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        $item_whsid = getItemWhsId($so_d['item_id']);

        if ($item_whsid == 0) {
          $in_whsid = $whsid;
        } else {
          $in_whsid = 1;
        }

        $so_d['so_id']  = $id_so_temp;
        $so_d['warehouse_id']  = $whsid;

        //Cek Stock Item
        $cek_qty = $this->MSales->cek_stock_item_whsid($so_d['item_id'], $so_d['qty'], $in_whsid);

        if ($cek_qty == false) {

          if ($data_so_header['payment_type'] == "0" || $data_so_header['payment_type'] == "3") { 

            $total_harga = intval($data_so_header['total_harga'])+intval($data_so_header['shipping_price']);
            $this->MSales->update_status_order_cancel_cc_ewallet($so_id_fe);

            if($data_so_header['payment_type'] == "0"){
              $this->MSales->refund_payment_to_ewallet($data_so_header['member_id'], $so_id_fe, $total_harga);
            }

            $this->MSales->send_message_order_cancel($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);

          }else{

            $this->MSales->update_status_order_cancel($so_id_fe);
            $this->MSales->send_message_order_cancel($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);
          }

          $result = array(
            'message' => 'Item ' . $so_d['item_id'] . ' has not enough stock on warehouse ' . getWhsName($whsid)
          );

          $url_log = base_url()  . "inbound/sales_order/create";
          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        //Update Stock NC
        $url =  base_url()  . "outbound/outbound_api/update_product_stock";
        $ch = curl_init($url);

        $data_post = array(
          "sku" => $so_d['item_id'],
          "qty_plus" => "0",
          "qty_minus" => "0"
        );

        $data = array("stockItems" => array($data_post));
        $post = json_encode(array("data" => $data, "key" => "Item SO Adjustment Stock"));

        $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        //Cek Harga Product
       /*
        $product_detail = $this->MSales->get_detail_product_fe($so_d['item_id']);

        $item_stock = $product_detail['price'];
        $harga_ = $product_detail['custom_attributes']['harga'];
        $pv_ = $product_detail['custom_attributes']['15']['value'];
        $bv_ = $product_detail['custom_attributes']['17']['value'];
        */

        $item_detail = $this->MSales->get_detail_item_by_id($so_d['item_id']);

        if (($item_detail['price'] != $so_d['harga']) || ($item_detail['pv'] != $so_d['pv']) || ($item_detail['bv'] != $so_d['bv'])) {

          if ($data_so_header['payment_type'] == "0" || $data_so_header['payment_type'] == "3") { 

            $total_harga = intval($data_so_header['total_harga'])+intval($data_so_header['shipping_price']);
            $this->MSales->update_status_order_cancel_cc_ewallet($so_id_fe);

            if($data_so_header['payment_type'] == "0"){
              $this->MSales->refund_payment_to_ewallet($data_so_header['member_id'], $so_id_fe, $total_harga);
            }
            $this->MSales->send_message_order_cancel_price($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);

          }else{

            $this->MSales->update_status_order_cancel($so_id_fe);
            $this->MSales->send_message_order_cancel_price($so_id_fe);
            $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);
          
          }

          $product_id_fe = $this->MOutbound->get_product_id_by_item_id($so_d['item_id']);

          //Update Product
          $url =  base_url()  . "outbound/outbound_api/update_product";
          $ch = curl_init($url);

          $custom_attributes = array(
            array("attribute_code" => "informasi_pv", "value" => $item_detail['pv']),
            array("attribute_code" => "informasi_bv", "value" => $item_detail['bv']),
          );
          $data_post = array(
            "sku" => $so_d['item_id'],
            "id" => $product_id_fe,
            "price" => $item_detail['price'],
            "custom_attributes" => $custom_attributes
          );

          $data = array("product" => $data_post);

          $post = json_encode($data);

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);
          //END Update Product

          activity_log('Update Product Price', 'outbound', json_encode($data), $url, $result);

          $result = array(
            'message' => 'Prices Item ' . $so_d['item_id'] . ' not match'
          );

          $url_log = base_url()  . "inbound/sales_order/create";
          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 200);

        }

        $insert_so_d_temp = $this->MSales->add_temp_so_d($so_d);

        $insert_reserv = $this->MSignup->stock_reserved($whsid, $id_so_temp, $so_d['item_id'], $so_d['qty'], 'system', $in_whsid);

        $x++;
        
      }

      //$insert_reserv = $this->MSignup->stock_reserved($whsid ,$id_so_temp, $so_d['item_id'], $so_d['qty'],$data_so_header['createdby'],$in_whsid);
      $insert_tail = $this->MSignup->proses_tail($id_so_temp, $in_whsid, 'system');
      $data_ncm = $this->MSignup->get_temp_ncm(intval($id_so_temp), $data_so_header['member_id'], intval($data_so_header['so_id_fe']));

      $so_id_be = 0;

      if ($data_so_header['payment_type'] == "0") { //Bayar Pake Ewallet

        $so_id_temp = $this->MSales->get_so_id_temp($data_so_header['member_id'], $data_so_header['so_id_fe']);
        $so_id_be = $this->MSales->payment_confirmation_order_ewallet($data_so_header['member_id'], $data_so_header['so_id_fe'], $so_id_temp);
        $id_so_temp = 0;

      } elseif ($data_so_header['payment_type'] == "3") {

        $data_deposit = array(
          'member_id'     => $data_so_header['member_id'],
          'total_amount'  => intval($data_so_header['total_harga'])+intval($data_so_header['shipping_price']),
          'total_trx'     => intval($data_so_header['total_harga'])+intval($data_so_header['shipping_price']),
          'bankid'        => 'BCA',
          'transfer_date' =>  date('Y-m-d H:i:s', now()),
          'remark'        => 'Sales Order',
          'deposit_id_fe' => null,
        );

        $so_id_temp = $this->MSales->get_so_id_temp($data_so_header['member_id'], $data_so_header['so_id_fe']);
        $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
        $so_id_be = $this->MSales->payment_confirmation_order_cc($data_so_header['member_id'], $data_so_header['so_id_fe'], $so_id_temp);
      
      }

      if ($data_ncm != false) {

        $ncm_item = array();
        foreach ($data_ncm as $ncm) {

          $item_whsid = getItemWhsId($ncm['ncm_item_id']);

          if ($item_whsid == 0) {
            $in_whsid = $whsid;
          } else {
            $in_whsid = 1;
          }

          $cek_qty = $this->MSales->cek_stock_item_whsid($ncm['ncm_item_id'], $ncm['ncm_qty'], $in_whsid);

          if ($cek_qty == false) {

            if ($data_so_header['payment_type'] == "0" || $data_so_header['payment_type'] == "3") { 

              $total_harga = intval($data_so_header['total_harga'])+intval($data_so_header['shipping_price']);
              $this->MSales->update_status_order_cancel_cc_ewallet($so_id_fe);

              if($data_so_header['payment_type'] == "0"){
                $this->MSales->refund_payment_to_ewallet($data_so_header['member_id'], $so_id_fe, $total_harga);
              }
              
              $this->MSales->send_message_order_cancel($so_id_fe);
              $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);
  
            }else{
  
              $this->MSales->update_status_order_cancel($so_id_fe);
              $this->MSales->send_message_order_cancel($so_id_fe);
              $this->MSales->delete_so_temp_by_so_id_fe($so_id_fe);
              
            }

            //Update Stock NC
            $url =  base_url()  . "outbound/outbound_api/update_product_stock";
            $ch = curl_init($url);

            $data_post = array(
              "sku" => $ncm['ncm_item_id'],
              "qty_plus" => "0",
              "qty_minus" => "0"
            );

            $data = array("stockItems" => array($data_post));
            $post = json_encode(array("data" => $data, "key" => "Item NC Adjustment Stock"));

            $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            $result = array(
              'message' => 'Item ' . $ncm['ncm_item_id'] . ' has not enough stock on warehouse ' . getWhsName($whsid)
            );

            $url_log = base_url()  . "inbound/sales_order/create";
            activity_log('Signup Member', 'inbound', json_encode($data_param), $url_log, json_encode($result));

            $this->response($result, 200);

          }

           //Update Stock Item
          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $ncm['ncm_item_id'],
            "qty_plus" => "0",
            "qty_minus" => $ncm['ncm_qty']
          );
          
          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "SO Item NC"));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);

          $status = json_decode($result);

          $array_ncm = array(
            "so_id_be"        => $so_id_be,
            "so_id_fe"        => $ncm['so_id_fe'],
            "ncm_id"          => $ncm['ncm_id'],
            "ncm_remark"      => 'Free of SO No. ' . $so_id_be,
            "ncm_createddate" => $ncm['ncm_createddate'],
            "ncm_d_id"        => $ncm['ncm_d_id'],
            "ncm_item_id"     => $ncm['ncm_item_id'],
            "ncm_qty"         => $ncm['ncm_qty']
          );

          if (isset($status[0]->success) == true || isset($status[0]->success) == 1) {
            array_push($ncm_item, $array_ncm);
          } else {

            $result = array(
              'message' => 'update product stock gagal'
            );

            $this->response($result, 404);
          }
        
        }

        if ($data_so_header['payment_type'] == "0" || $data_so_header['payment_type'] == "3") {

          $result = array(

            'so_id_fe' => $data_so_header['so_id_fe'],
            'so_id_be' => intval($so_id_be),
            'free_item' => $ncm_item

          );

          $url_log = base_url()  . "inbound/sales_order/create";

          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 405);
          
        } else {

          $result = array(

            'so_id_fe' => $data_so_header['so_id_fe'],
            'so_id_be' => intval($so_id_be),
            'so_id_be_temp' =>  $id_so_temp,
            'free_item' => $ncm_item

          );

          $url_log = base_url()  . "inbound/sales_order/create";
          activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

          $this->response($result, 405);
        }
          
      } else {

        $ncm_item = array();

        $result = array(

          'so_id_fe' => $data_so_header['so_id_fe'],
          'so_id_be' => intval($so_id_be),
          'so_id_be_temp' =>  $id_so_temp

        );

        $url_log = base_url()  . "inbound/sales_order/create";
        activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

    } else {

      $result = array(
        'message' => 'gagal'
      );

      $url_log = base_url()  . "inbound/sales_order/create";
      activity_log('Create SO', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  function payment_confirm_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {

      if (!isset($data_param['member_id'])) {
        $this->response($result = array(
          'message' => 'member_id is required'
        ), 405);
      }

      if (!isset($data_param['so_id_fe'])) {
        $this->response($result = array(
          'message' => 'so_id_fe is required'
        ), 405);
      }

      if (!isset($data_param['payment_type'])) {
        $this->response($result = array(
          'message' => 'payment_type is required'
        ), 405);
      }

      if (!isset($data_param['bank_from'])) {
        $this->response($result = array(
          'message' => 'bank_from is required'
        ), 405);
      }

      if (!isset($data_param['bank_to'])) {
        $this->response($result = array(
          'message' => 'bank_to is required'
        ), 405);
      }

      if (!isset($data_param['no_rek'])) {
        $this->response($result = array(
          'message' => 'no_rek is required'
        ), 405);
      }

      if (!isset($data_param['nama_rek'])) {
        $this->response($result = array(
          'message' => 'nama_rek is required'
        ), 405);
      }

      if (!isset($data_param['amount'])) {
        $this->response($result = array(
          'message' => 'amount is required'
        ), 405);
      }

      if ($data_param['payment_type'] == "2" || $data_param['payment_type'] == 2) {

        if ($data_param['bank_from'] == "") {
          $this->response($result = array(
            'message' => 'bank_from required value'
          ), 405);
        }

        if ($data_param['bank_to'] == "") {
          $this->response($result = array(
            'message' => 'bank_to is required value'
          ), 405);
        }

        if ($data_param['no_rek'] == "") {
          $this->response($result = array(
            'message' => 'no_rek is required value'
          ), 405);
        }

        if ($data_param['nama_rek'] == "") {
          $this->response($result = array(
            'message' => 'nama_rek is required value'
          ), 405);
        }

        if ($data_param['trx_type'] == "") {
          $this->response($result = array(
            'message' => 'trx_type is required value'
          ), 405);
        }
      }

      $bank_from   = $data_param['bank_from'];
      $bank_to   = $data_param['bank_to'];
      $no_rek   = $data_param['no_rek'];
      $nama_rek   = $data_param['nama_rek'];
      $amount   = $data_param['amount'];
      $payment_type   = $data_param['payment_type'];
      $member_id   = $data_param['member_id'];
      $so_id_fe   = $data_param['so_id_fe'];
      $trx_type   = $data_param['trx_type'];

      $bank_from = $this->MEwallet->getBankIdByIdFe($bank_from);

      if (!$bank_from) {
        $this->response($result = array(
          'message' => 'Bank ID FE Mapping Not Found'
        ), 405);
      }

      $bank_to = $this->MEwallet->getBankIdByIdFe($bank_to);

      if (!$bank_to) {
        $this->response($result = array(
          'message' => 'Bank ID FE Mapping Not Found'
        ), 405);
      }

      $data_payment = array(
        "member_id" => $member_id,
        "so_id_fe" => $so_id_fe,
        "bank_from" => $bank_from,
        "bank_to" => $bank_to,
        "no_rek" => $no_rek,
        "nama_rek" => $nama_rek,
        "payment_type" => $payment_type,
        "trx_type" => $trx_type,
        "amount" => $amount,
        "status" => 'pending',
        "created" => date('Y-m-d', now())
      );

      if ($data_param['trx_type'] == "2" || $data_param['trx_type'] == 2) { // 2 = SO, 1 = DEPOSIT
        $data_reff = $this->MSales->get_sales_order_pending($member_id, $so_id_fe);

        if ($data_reff == false) {

          $this->response($result = array(
            'message' => 'Invoice No. ' . $so_id_fe . ' is not found or Member ID ' . $member_id . ' is not Found'
          ), 405);
          
        }

        $so_id_temp = $data_reff['id'];

      } else {

        $data_reff = $this->MEwallet->get_deposit_ewallet_pending($member_id, $so_id_fe);

        if ($data_reff == false) {

          $this->response($result = array(
            'message' => 'Invoice No. ' . $so_id_fe . ' is not found or Member ID ' . $member_id . ' is not Found'
          ), 405);
          
        }

        $so_id_temp = $data_reff['deposit_id_fe'];
        
      }

      $so_id_be = 0;

      if ($data_param['payment_type'] == "1" || $data_param['payment_type'] == 1) { // Pake Virtual Account

        if ($data_param['trx_type'] == "2" || $data_param['trx_type'] == 2) { // 2 = SO, 1 = DEPOSIT

          $status_kit = $this->MSales->cek_kit_yes_no($so_id_fe, $so_id_temp);

          if ($status_kit == true) {
            $active_member = $this->MSales->member_activation($member_id, $so_id_fe, $so_id_temp);
          }

          $data_deposit = array(
            'member_id'     => $member_id,
            'total_amount'  => $amount,
            'total_trx'     => $data_reff['totalharga'],
            'bankid'        => $bank_to,
            'transfer_date' => date('Y-m-d H:i:s', now()),
            'remark'        => 'Sales Order',
            'deposit_id_fe' => null
          );

          $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
          $so_id_temp = $this->MSales->get_so_id_temp($member_id, $so_id_fe);
          $so_id_be = $this->MSales->payment_confirmation_order_va($member_id, $so_id_fe, $so_id_temp);
        } else {
          $so_id_be = $this->MEwallet->deposit_confirmation(intval($so_id_fe), $data_param['payment_type']);
        }

        $so_id_be = $so_id_be;

      } else {

        $data_payment['status'] = "pending";

        $insert_payment = $this->MSales->insert_payment_confirm($data_payment);

      }

      $result = array(
        'member_id' => $member_id,
        'so_id_fe' => $so_id_fe,
        'so_id_be' =>  $so_id_be
      );

      $url_log = base_url()  . "inbound/sales_order/payment_confirm";
      activity_log('Payment Confirm', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 405);

    } else {
      $this->response($result = array(
        'message' => 'gagal'
      ), 404);
    }

  }

  public function payment_approved_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';

    if (isset($data_param)) {

      if (!isset($data_param['id_list'])) {

        $result = array(
          'message' => 'Payment ID is required'
        );

        $url_log = base_url()  . "inbound/sales_order/payment_approve";
        activity_log('Payment Approved', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $id_list = $data_param['id_list'];
      $remark = $data_param['remark'];

      $approved_payment = $this->MSales->payment_approved($id_list, $remark);
      $data_payment_list = $this->MSales->get_payment_confirm_by_id_list($id_list);

      foreach ($data_payment_list as $payment_d) {

        if ($payment_d['trx_type'] == "2" || $payment_d['trx_type'] == 2) { // 2 = SO, 1 = DEPOSIT

          $data_reff = $this->MSales->get_sales_order_pending($payment_d['member_id'], $payment_d['so_id_fe']);
          $so_id_temp = $data_reff['id'];

          $status_kit = $this->MSales->cek_kit_yes_no($payment_d['so_id_fe'], $so_id_temp);

          if ($status_kit == true) {
            $active_member = $this->MSales->member_activation($payment_d['member_id'], $payment_d['so_id_fe'], $so_id_temp);
          }

          $data_deposit = array(
            'total_amount'  => $payment_d['amount'],
            'total_trx'     => $data_reff['totalharga'],
            'bankid'        => $payment_d['bank_to'],
            'transfer_date' => date('Y-m-d H:i:s', now()),
            'remark'        => 'Sales Order',
            'deposit_id_fe' => null,
            'member_id'     => $payment_d['member_id']
          );

          $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
          $so_id_temp = $this->MSales->get_so_id_temp($payment_d['member_id'], $payment_d['so_id_fe']);
          $so_id_be = $this->MSales->payment_confirmation_order($payment_d['member_id'], $payment_d['so_id_fe'], $so_id_temp);

        } else {

          $data_reff = $this->MEwallet->get_deposit_ewallet_pending($payment_d['member_id'], $payment_d['so_id_fe']);
          $so_id_temp = $data_reff['deposit_id_fe'];
          $so_id_be = $this->MEwallet->deposit_confirmation(intval($payment_d['so_id_fe']), $payment_d['payment_type']);
        }

        $post = json_encode(array("so_id_be" => $so_id_be));

        $url =  base_url()  . "outbound/outbound_api/update_soid_be_to_fe";
        $ch = curl_init($url);

        $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

      }

      $result = array(
        'status' => "Berhasil"
      );

      $url_log = base_url()  . "inbound/sales_order/payment_approve";
      activity_log('Payment Approved', 'inbound', json_encode($data_param), $url_log, ($result));

      $this->response($result, 405);

    } else {

      $result = array(
        'status' => "Gagal"
      );

      $url_log = base_url()  . "inbound/sales_order/payment_approve";
      activity_log('Payment Approved', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  function sales_order_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      if (!isset($data_param['member_id'])) {
        $this->response(array(
          'message' => 'member_id is required'
        ), 405);
      }

      if (!isset($data_param['so_id_fe'])) {
        $this->response(array(
          'message' => 'member_id is required'
        ), 405);
      }

      if (!isset($data_param['so_id_be'])) {
        $this->response(array(
          'message' => 'member_id is required'
        ), 405);
      }

      $member_id   = $data_param['member_id'];
      $so_id_fe   = $data_param['so_id_fe'];
      $so_id_temp   = $data_param['so_id_be'];

      $status_kit = $this->MSales->cek_kit_yes_no($so_id_fe, $so_id_temp);

      if ($status_kit == true) {

        $array_data = $this->MSales->member_activation($member_id, $so_id_fe, $so_id_temp);
      } else {

        $so_id_be = $this->MSales->payment_confirmation($member_id, $so_id_fe);
      }

      if (!empty($so_id_be)) {
        $this->response(array(
          'message' => 'Berhasil'
        ), 405);
      } else {
        $this->response(array(
          'message' => 'Invoice ' . $so_id_fe . ' is not found. Member ' . $member_id . ' is not Found'
        ), 405);
      }
    } else {
      $this->response(array(
        'message' => 'gagal'
      ), 404);
    }
  }

  function cancelorderppending_post()
  {
    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {
      if (!isset($data_param['so_id_fe'])) {

        $result = array(
          'message' => 'so_id_fe is required'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
        activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }
      $so_id_fe         = $data_param['so_id_fe'];

      //check temp_so
      $getTempSoid = $this->MSales->getTempSoid($so_id_fe);

      //if ada temp_so
      if ($getTempSoid) {

        //check temp_ref_trx_nc
        $getTempreftrxncid = $this->MSales->getTempreftrxncid($getTempSoid);
        //if ada temp_ref_trx_nc
        if ($getTempreftrxncid > 0) {
          //loop ncm_d
          $this->MSales->cancelTempNcmD($getTempSoid);
          // update stock ke be
          // update stock, qty_reserved (-), qty (+)
          //eof loop ncm_d
          
          // del temp_ncm_d
          $this->MSales->delTempNcmD($getTempSoid);

          // del temp_ncm
          $this->MSales->delTempNcm($getTempSoid);
          
          // del temp_ref_trx_nc
          $this->MSales->delTempreftrxnc($getTempSoid);
          
        }
        //endif ada temp_ref_trx_nc

        //loop temp_so_d
        $this->MSales->cancelTempSo($getTempSoid);
        // update stock, qty_reserved (-), qty (+)

        //eof loop temp_so_d
        
        //del temp_so_d
        $this->MSales->delTempSod($getTempSoid);
        //del temp_so
        $this->MSales->delTempSo($getTempSoid);
        
        $result = array(
          'message' => 'Berhasil'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
        activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 200);
      } else {

        $result = array(
          'message' => 'Sales Order Not Found'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
        activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }
    } else {

      $result = array(
        'message' => 'gagal'
      );

      $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
      activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  function cancelorderppending_signup_post()
  {
    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';
    if (isset($data_param)) {

      if (!isset($data_param['member_id_fe'])) {

        $result = array(
          'message' => 'member_id_fe is required'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderppending_signup";
        activity_log('Cancel Order Pending Signup', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $member_id_fe         = $data_param['member_id_fe'];

      //check temp_so
      $member_id_be = $this->MSales->getTempMemberId($member_id_fe);
      //$member_id_be = "10317163";
      $getTempSo = $this->MSales->getTempSoByMemberId($member_id_be);

      //if ada temp_so
      if ($getTempSo) {

        foreach ($getTempSo as $temp_so) {

          $getTempreftrxncid = $this->MSales->getTempreftrxncid($temp_so->id);
          //if ada temp_ref_trx_nc
          if ($getTempreftrxncid > 0) {
            //loop ncm_d
            $this->MSales->cancelTempNcmD($temp_so->id);
            // update stock ke be
            // update stock, qty_reserved (-), qty (+)
            //eof loop ncm_d

            // del temp_ncm_d
            $this->MSales->delTempNcmD($temp_so->id);
            
            // del temp_ncm
            $this->MSales->delTempNcm($temp_so->id);
            
            // del temp_ref_trx_nc
            $this->MSales->delTempreftrxnc($temp_so->id);
          }
          //endif ada temp_ref_trx_nc
          
          //loop temp_so_d
          $this->MSales->cancelTempSo($temp_so->id);
          //del temp_so_d
          $this->MSales->delTempSod($temp_so->id);
          //del temp_so
          $this->MSales->delTempSo($temp_so->id);

          //del temp_member
          $this->MSales->cancelTempMember($member_id_be);
         
        }

        $result = array(
          'message' => 'Berhasil'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderppending_signup";
        activity_log('Cancel Order Pending Signup', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 200);
      } else {

        $result = array(
          'message' => 'Sales Order Not Found'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
        activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $getTempMemberid = $this->MSales->getTempMemberId($member_id_fe);

      if ($getTempMemberid) {

        $this->MSales->cancelTempMember($getTempMemberid);

        $result = array(
          'message' => 'Berhasil'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderppending_signup";
        activity_log('Cancel Order Pending Signup', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 200);
      } else {

        $result = array(
          'message' => 'Member ID FE Not Found'
        );

        $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
        activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }
    } else {

      $result = array(
        'message' => 'gagal'
      );

      $url_log = base_url()  . "inbound/sales_order/cancelorderpending";
      activity_log('Cancel Order Pending', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  public function order_temp_approved_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';

    if (isset($data_param)) {

      if (!isset($data_param['id_list'])) {

        $result = array(
          'message' => 'SO ID FE is required'
        );

        $url_log = base_url()  . "inbound/sales_order/order_temp_approved";
        activity_log('Order Approved', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $id_list = $data_param['id_list'];

      $data_order_list = $this->MSales->get_temp_so_by_so_id_fe($id_list);

      foreach ($data_order_list as $payment_d) {

          $status_kit = $this->MSales->cek_kit_yes_no($payment_d['so_id_fe'], $payment_d['id']);

          if ($status_kit == true) {
            $active_member = $this->MSales->member_activation($payment_d['member_id'], $payment_d['so_id_fe'], $payment_d['id']);
          }

          $data_deposit = array(
            'member_id'     => $payment_d['member_id'],
            'total_amount'  => intval($payment_d['totalharga'])+intval($payment_d['shipping_price']),
            'total_trx'     => intval($payment_d['totalharga'])+intval($payment_d['shipping_price']),
            'bankid'        => 'BCA',
            'transfer_date' => date('Y-m-d H:i:s', now()),
            'remark'        => 'Sales Order',
            'deposit_id_fe' => null
          );

          if ($payment_d['payment_type'] == "1") {

            $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
            $so_id_temp = $this->MSales->get_so_id_temp($payment_d['member_id'], $payment_d['so_id_fe']);
            $so_id_be = $this->MSales->payment_confirmation_order_va($payment_d['member_id'], $payment_d['so_id_fe'], $payment_d['id']);
    
          } elseif ($payment_d['payment_type'] == "3") {
    
            $so_id_temp = $this->MSales->get_so_id_temp($payment_d['member_id'], $payment_d['so_id_fe']);
            $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
            $so_id_be = $this->MSales->payment_confirmation_order_cc($payment_d['member_id'], $payment_d['so_id_fe'], $payment_d['id']);
          
          }else{

            $deposit_id = $this->MEwallet->insert_deposit_ewallet($data_deposit);
            $so_id_temp = $this->MSales->get_so_id_temp($payment_d['member_id'], $payment_d['so_id_fe']);
            $so_id_be = $this->MSales->payment_confirmation_order($payment_d['member_id'], $payment_d['so_id_fe'], $payment_d['id']);

          }

        $post = json_encode(array("so_id_be" => $so_id_be));

        $url =  base_url()  . "outbound/outbound_api/update_soid_be_to_fe";
        $ch = curl_init($url);

        $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

      }

      $result = array(
        'status' => "Berhasil"
      );

      $url_log = base_url()  . "inbound/sales_order/order_temp_approved";
      activity_log('Order Approved', 'inbound', json_encode($data_param), $url_log, ($result));

      $this->response($result, 405);

    } else {

      $result = array(
        'status' => "Gagal"
      );

      $url_log = base_url()  . "inbound/sales_order/order_temp_approved";
      activity_log('Order Approved', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
  }

  public function reject_order_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    $result = '';

    if (isset($data_param)) {

      if (!isset($data_param['id_list'])) {

        $result = array(
          'message' => 'SO ID FE is required'
        );

        $url_log = base_url()  . "inbound/sales_order/reject_order";
        activity_log('Order Approved', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $id_list = $data_param['id_list'];

      $data_order_list = $this->MSales->get_temp_so_by_so_id_fe($id_list);

      foreach ($data_order_list as $payment_d) {

        $getTempreftrxncid = $this->MSales->getTempreftrxncid($payment_d['id']);
        //if ada temp_ref_trx_nc
        if ($getTempreftrxncid > 0) {
          //loop ncm_d
          $this->MSales->cancelTempNcmD($payment_d['id']);
          // update stock ke be
          // update stock, qty_reserved (-), qty (+)
          //eof loop ncm_d

          // del temp_ncm_d
          $this->MSales->delTempNcmD($payment_d['id']);
          
          // del temp_ncm
          $this->MSales->delTempNcm($payment_d['id']);
          
          // del temp_ref_trx_nc
          $this->MSales->delTempreftrxnc($payment_d['id']);
        }
        //endif ada temp_ref_trx_nc
        
        //loop temp_so_d
        $this->MSales->cancelTempSo($payment_d['id']);
        //del temp_so_d
        $this->MSales->delTempSod($payment_d['id']);
        //del temp_so
        $this->MSales->delTempSo($payment_d['id']);

        if($payment_d['kit']=="Y"){
          //del temp_member
          $this->MSales->cancelTempMember($payment_d['member_id']);
        }

      }

      $result = array(
        $data_order_list
        //'status' => "Berhasil"
      );

      $url_log = base_url()  . "inbound/sales_order/reject_order";
      activity_log('Reject Order', 'inbound', json_encode($data_param), $url_log, ($result));

      $this->response($result, 405);

    } else {

      $result = array(
        'status' => "Gagal"
      );

      $url_log = base_url()  . "inbound/sales_order/reject_order";
      activity_log('Order Approved', 'inbound', json_encode($data_param), $url_log, json_encode($result));

      $this->response($result, 404);
    }
    
  }
  
}
