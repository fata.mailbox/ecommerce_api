<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Outbound_api extends REST_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model(array('MOutbound', 'MEwallet', 'MSales'));
    $this->load->helper(array('date', 'my'));
    $this->load->library(array('email', 'user_agent'));
    $this->load->helper('url');
  }

  function create_order_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $so_id_be = $data_param['so_id_be'];

      $data_so  = $this->MSales->get_so_from_so_id_be($so_id_be);
      $member    = $this->MSales->get_member_detail_by_member_id($data_so->member_id);

      if ($member != false) {

        if ($member->email == null || $member == '') {
          $email = $member->id . '@gmail.com';
        } else {
          $email = $member->email;
        }

        $member_id = $member->id;
      } else {
        $this->response(array(
          'message' => 'Member ID Not Found'
        ), 404);
      }

      $data_delivery = $this->MSales->get_memberdelv_by_delivery_addr($data_so->delivery_addr);

      if ($data_delivery != false || $data_delivery != 0) {

        $firstname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 0, 1));
        $lastname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 1));
        if ($lastname == "") {
          $lastname = '-';
        }
        $data_kota = $this->MSales->get_kota_from_id_be($data_delivery->kota);
        $region     = $data_kota->name;
        $city       = $data_delivery->kecamatan;
        $street     = $data_delivery->alamat;
        $postcode  = $data_delivery->kodepos;
        $telephone  = $data_delivery->pic_hp;
      } else {

        $data_delivery = $this->MSales->get_memberdelv_by_delivery_by_member_id($member_id);

        $firstname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 0, 1));
        $lastname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 1));
        if ($lastname == "") {
          $lastname = '-';
        }
        $data_kota = $this->MSales->get_kota_from_id_be($data_delivery->kota);
        $region     = $data_kota->name;
        $city       = $data_delivery->kecamatan;
        $street     = $data_delivery->alamat;
        $postcode  = $data_delivery->kodepos;
        $telephone  = $data_delivery->pic_hp;
      }

      $delivery_address = array(

        "firstname" => $firstname,
        "lastname" => $lastname,
        "country_id" => "ID",
        "region" => $region,
        "city" => $region,
        "street" => $street,
        "postcode" => $postcode,
        "telephone" => $telephone,
        "same_as_billing" => 1

      );

      $so_detail = $this->MSales->get_so_detail_by_so_id_be($so_id_be);

      $item = array();
      $diskon = 0;

      if ($so_detail) {

        foreach ($so_detail as $so_d) {

          $product_id_fe = $this->MOutbound->get_product_id_by_item_id($so_d['item_id']);

          $sod['product_id'] = $product_id_fe;
          $sod['qty'] = $so_d['qty'];
          $sod['price'] = $so_d['harga'];
          $sod['pv'] = $so_d['pv'];
          $sod['bv'] = $so_d['bv'];
          $sod['rowdiscount'] = intval($so_d['qty'])*intval($so_d['ipddiskon']);
          $sod['rowtotal'] = $so_d['jmlharga'];
          array_push($item, $sod);

          $diskon += $so_d['ipddiskon'];

          $item_stock = $this->MSales->get_item_stock_fe($so_d['item_id']);

          if ($item_stock < intval($so_d['qty'])) {
            //Start Update Stock Fe
            $url =  base_url()  . "outbound/outbound_api/update_product_stock";
            $ch = curl_init($url);

            $data_post = array(
              "sku" => $so_d['item_id'],
              "qty_plus" => $so_d['qty'],
              "qty_minus" => 0
            );

            $data = array("stockItems" => array($data_post));
            $post = json_encode(array("data" => $data, "key" => "Item SO " . $so_id_be));

            $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);
            //End Update Stock Fe
          }
        }
      }

      $data_ncm = $this->MOutbound->get_ncm($so_id_be);

      $customer_note = '';

      if ($data_ncm != false) {

        $ncm_array = array();
        foreach ($data_ncm as $ncm) {

          $item_ncm_array = array(
            "qty" => $ncm->ncm_qty,
            "sku" => $ncm->ncm_item_id
          );
          array_push($ncm_array, $item_ncm_array);

          $item_stock = $this->MSales->get_item_stock_fe($ncm->ncm_item_id);

          if ($item_stock < intval($ncm->ncm_qty)) {
            //Start Update Stock Fe
            $url =  base_url()  . "outbound/outbound_api/update_product_stock";
            $ch = curl_init($url);

            $data_post = array(
              "sku" => $ncm->ncm_item_id,
              "qty_plus" => $ncm->ncm_qty,
              "qty_minus" => 0
            );

            $data = array("stockItems" => array($data_post));
            $post = json_encode(array("data" => $data, "key" => "Item NCM " . $so_id_be));

            $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);
            //End Update Stock Fe
          }
        }

        $customer_note = json_encode($ncm_array);
      }

      $order = array(
        "email" => $email,
        "shipping_address" => $delivery_address,
        "billing_address" => $delivery_address,
        "items" => $item,
        "shipping_method" => "freeshipping_freeshipping",
        "payment_method" => "banktransfer",
        "customer_note" => $customer_note,
        "so_id_be" => "$so_id_be",
        "member_id" => "$member_id",
        "kit" => "0",
        "total_discount" => "$diskon"
      );

      $data_so_post = json_encode($order);

      $token = get_token();

      $url = get_url_api() . 'rest/V1/order/create';
      $ch = curl_init($url);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_so_post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      if ($result) {

        $so_data_fe_new = json_decode($result, true);

        $so_id_fe_new = $so_data_fe_new['0']['data']['so_id_fe'];

        $this->MSales->insert_update_so_id_fe($so_id_be, $so_id_fe_new);
        $this->MSales->update_status_order_shipping($so_id_fe_new);
        $this->MSales->update_status_order_completed($so_id_fe_new);
      }

      if ($data_ncm != false) {

        $ncm_array = array();
        foreach ($data_ncm as $ncm) {

          //Start Update Stock Fe
          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $ncm->ncm_item_id,
            "qty_plus" => 0,
            "qty_minus" => $ncm->ncm_qty
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item NCM - SO " . $so_id_be));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);

          //End Update Stock Fe

        }
      }

      // PUSH API Ewallet Kredit
      $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

      $walletdata = array(
        "customer_id" => intval($member_id_fe),
        "walletamount" => intval($data_so->totalharga)
      );

      $data_deposit_post = array(
        "walletdata"        => array($walletdata),
        "walletactiontype"  => 'credit',
        "walletnote"        => "Desposit id order - $so_id_be",
      );

      $token = get_token();

      $url1 = get_url_api() . 'rest/V1/ewallet/adjustment/data';
      $ch = curl_init($url1);

      $post = json_encode($data_deposit_post);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result1 = curl_exec($ch);
      curl_close($ch);

      activity_log('Deposit Order', 'outbound', $post, $url1, $result1);

      // PUSH API Ewallet Debit

      $walletdata = array(
        "customer_id" => intval($member_id_fe),
        "walletamount" => intval($data_so->totalharga)
      );

      $data_deposit_post = array(
        "walletdata"        => array($walletdata),
        "walletactiontype"  => 'debit',
        "walletnote"        => "Pembayaran id order - $so_id_be",
      );

      $token = get_token();

      $url2 = get_url_api() . 'rest/V1/ewallet/adjustment/data';
      $ch = curl_init($url2);

      $post = json_encode($data_deposit_post);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result2 = curl_exec($ch);
      curl_close($ch);

      activity_log('Withdrawal Order', 'outbound', $post, $url2, $result2);

      activity_log('Create SO', 'outbound', $data_so_post, $url, $result);
    }
  }

  function create_order_stc_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $so_id_be = $data_param['so_id_be'];

      $data_so  = $this->MSales->get_so_from_so_id_be($so_id_be);
      $member    = $this->MSales->get_member_detail_by_member_id($data_so->member_id);

      if ($member != false) {

        if ($member->email == null || $member == '') {
          $email = $member->id . '@unihealth.com';
        } else {
          $email = $member->email;
        }

        $member_id = $member->id;
      } else {
        $this->response(array(
          'message' => 'Member ID Not Found'
        ), 404);
      }

      $data_delivery = $this->MSales->get_memberdelv_by_delivery_addr($data_so->delivery_addr);

      if ($data_delivery != false || $data_delivery != 0) {

        $firstname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 0, 1));
        $lastname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 1));
        if ($lastname == "") {
          $lastname = '-';
        }
        $data_kota = $this->MSales->get_kota_from_id_be($data_delivery->kota);
        $region     = $data_kota->name;
        $city       = $data_delivery->kecamatan;
        $street     = $data_delivery->alamat;
        $postcode  = $data_delivery->kodepos;
        $telephone  = $data_delivery->pic_hp;
      } else {

        $data_delivery = $this->MSales->get_memberdelv_by_delivery_by_member_id($member_id);

        $firstname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 0, 1));
        $lastname = implode(" ", array_slice(explode(" ", $data_delivery->pic_name), 1));
        if ($lastname == "") {
          $lastname = '-';
        }
        $data_kota = $this->MSales->get_kota_from_id_be($data_delivery->kota);
        $region     = $data_kota->name;
        $city       = $data_delivery->kecamatan;
        $street     = $data_delivery->alamat;
        $postcode  = $data_delivery->kodepos;
        $telephone  = $data_delivery->pic_hp;
      }

      $delivery_address = array(

        "firstname" => $firstname,
        "lastname" => $lastname,
        "country_id" => "ID",
        "region" => $region,
        "city" => $region,
        "street" => $street,
        "postcode" => $postcode,
        "telephone" => $telephone,
        "same_as_billing" => 1

      );

      $so_detail = $this->MSales->get_so_detail_by_so_id_be($so_id_be);

      $item = array();
      $diskon = 0;

      if ($so_detail) {

        foreach ($so_detail as $so_d) {

          $product_id_fe = $this->MOutbound->get_product_id_by_item_id($so_d['item_id']);

          $sod['product_id'] = $product_id_fe;
          $sod['qty'] = $so_d['qty'];
          $sod['price'] = $so_d['harga'];
          $sod['pv'] = $so_d['pv'];
          $sod['bv'] = $so_d['bv'];
          $sod['rowdiscount'] = intval($so_d['qty']) * intval($so_d['ipddiskon']);
          $sod['rowtotal'] = $so_d['jmlharga'];
          array_push($item, $sod);

          $diskon += $so_d['ipddiskon'];

          $item_stock = $this->MSales->get_item_stock_fe($so_d['item_id']);

          //Start Update Stock Fe
          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $so_d['item_id'],
            "qty_plus" => $so_d['qty'],
            "qty_minus" => 0
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item SO " . $so_id_be));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);
          //End Update Stock Fe
        }
      }

      $data_ncm = $this->MOutbound->get_ncm($so_id_be);

      $customer_note = '';

      if ($data_ncm != false) {

        $ncm_array = array();
        foreach ($data_ncm as $ncm) {

          $item_ncm_array = array(
            "qty" => $ncm->ncm_qty,
            "sku" => $ncm->ncm_item_id
          );
          array_push($ncm_array, $item_ncm_array);

          $item_stock = $this->MSales->get_item_stock_fe($ncm->ncm_item_id);

          //Start Update Stock Fe
          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $ncm->ncm_item_id,
            "qty_plus" => $ncm->ncm_qty,
            "qty_minus" => 0
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item NCM " . $so_id_be));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);
          //End Update Stock Fe

        }

        $customer_note = json_encode($ncm_array);
      }

      $order = array(
        "email" => $email,
        "shipping_address" => $delivery_address,
        "billing_address" => $delivery_address,
        "items" => $item,
        "shipping_method" => "freeshipping_freeshipping",
        "payment_method" => "banktransfer",
        "customer_note" => $customer_note,
        "so_id_be" => "$so_id_be",
        "member_id" => "$member_id",
        "kit" => "1",
        "total_discount" => "$diskon"
      );

      $data_so_post = json_encode($order);

      $token = get_token();

      $url = get_url_api() . 'rest/V1/order/create';
      $ch = curl_init($url);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data_so_post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      if ($result) {

        $so_data_fe_new = json_decode($result, true);

        $so_id_fe_new = $so_data_fe_new['0']['data']['so_id_fe'];

        $this->MSales->insert_update_so_id_fe($so_id_be, $so_id_fe_new);
        $this->MSales->update_status_order_shipping($so_id_fe_new);
        $this->MSales->update_status_order_completed($so_id_fe_new);
      }

      if ($data_ncm != false) {

        $ncm_array = array();
        foreach ($data_ncm as $ncm) {

          //Start Update Stock Fe
          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $ncm->ncm_item_id,
            "qty_plus" => 0,
            "qty_minus" => $ncm->ncm_qty
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item NCM - SO " . $so_id_be));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);

          //End Update Stock Fe

        }
      }

      // PUSH API Ewallet Kredit
      $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

      $walletdata = array(
        "customer_id" => intval($member_id_fe),
        "walletamount" => intval($data_so->totalharga)
      );

      $data_deposit_post = array(
        "walletdata"        => array($walletdata),
        "walletactiontype"  => 'credit',
        "walletnote"        => "Desposit id order - $so_id_be",
      );

      $token = get_token();

      $url1 = get_url_api() . 'rest/V1/ewallet/adjustment/data';
      $ch = curl_init($url1);

      $post = json_encode($data_deposit_post);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result1 = curl_exec($ch);
      curl_close($ch);

      activity_log('Deposit Order', 'outbound', $post, $url1, $result1);

      // PUSH API Ewallet Debit

      $walletdata = array(
        "customer_id" => intval($member_id_fe),
        "walletamount" => intval($data_so->totalharga)
      );

      $data_deposit_post = array(
        "walletdata"        => array($walletdata),
        "walletactiontype"  => 'debit',
        "walletnote"        => "Pembayaran id order - $so_id_be",
      );

      $token = get_token();

      $url2 = get_url_api() . 'rest/V1/ewallet/adjustment/data';
      $ch = curl_init($url2);

      $post = json_encode($data_deposit_post);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result2 = curl_exec($ch);
      curl_close($ch);

      activity_log('Withdrawal Order', 'outbound', $post, $url2, $result2);

      activity_log('Create SO', 'outbound', $data_so_post, $url, $result);
    }
  }

  function cancel_order_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $so_id_cancel = $data_param['so_id_be'];

      $so_id_be = $this->MSales->get_so_id_exec_by_so_id($so_id_cancel);
      $remark = $data_param['remark'];

      $data_so_cancel = $this->MSales->get_sales_order_by_so_id_be($so_id_cancel);
      $data_so = $this->MSales->get_sales_order_by_so_id_be($so_id_be);

      $so_id_fe = $data_so_cancel['so_id_fe'];

      $update_order_canceled = $this->MSales->update_status_order_cancel($so_id_fe);

      if (isset($update_order_canceled)) {

        $so_id_be =  $data_so['id'];
        $data_so_d =  $this->MSales->get_so_detail_by_so_id_be($so_id_be);

        $item = array();
        //$data_item = 
        foreach ($data_so_d as $so_d) {

          $product_id_fe = $this->MOutbound->get_product_id_by_item_id($so_d['item_id']);

          $sod['product_id'] = $product_id_fe;
          $sod['qty'] = $so_d['qty'];
          $sod['price'] = $so_d['harga'];
          $sod['pv'] = $so_d['pv'];
          $sod['bv'] = $so_d['bv'];
          $sod['rowdiscount'] = intval($so_d['qty'])*intval($so_d['ipddiskon']);
          $sod['rowtotal'] = $so_d['jmlharga'];
          array_push($item, $sod);
        }

        $detail_member = $this->get_member_by_id($data_so['member_id']);

        // print_r($detail_member); die();

        $firstname = implode(" ", array_slice(explode(" ", $detail_member->nama), 0, 1));
        $lastname = implode(" ", array_slice(explode(" ", $detail_member->nama), 1));
        if ($lastname == "") {
          $lastname = '-';
        }
        $kota_id_fe = $this->MOutbound->getKotaIdByIdBe($detail_member->kota_id);
        $city_name = $this->MOutbound->get_city_name($detail_member->kota_id);

        $shipping_address = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "country_id" => "ID",
          "region" => $city_name,
          "city" => $city_name,
          "street" => $detail_member->alamat,
          "postcode" => $detail_member->kodepos,
          "telephone" => $detail_member->hp
        );

        $billing_address = array(
          "firstname" => $firstname,
          "lastname" => $lastname,
          "country_id" => "ID",
          "region" => $city_name,
          "city" => $city_name,
          "street" => $detail_member->alamat,
          "postcode" => $detail_member->kodepos,
          "telephone" => $detail_member->hp
        );

        $member_id = $data_so['member_id'];

        $order = array(
          "email" => $detail_member->email,
          "shipping_address" => $shipping_address,
          "billing_address" => $billing_address,
          "items" => $item,
          "shipping_method" => "freeshipping_freeshipping",
          "payment_method" => "banktransfer",
          "customer_note" => "Cancel Order - $so_id_fe",
          "so_id_be" => "$so_id_be",
          "member_id" => "$member_id",
          "kit" => "0",
          "total_discount" => "0"
        );

        $data_so_post = json_encode($order);

        $token = get_token();

        $url = get_url_api() . 'rest/V1/order/create';
        $ch = curl_init($url);

        //$post = json_encode($post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_so_post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Cancel SO', 'outbound', $data_so_post, $url, $result);

        $so_id_fe_new = 0;

        if ($result) {
          $so_data_fe_new = json_decode($result, true);

          $so_id_fe_new = $so_data_fe_new['0']['data']['so_id_fe'];

          $this->MSales->insert_update_so_id_fe($so_id_be, $so_id_fe_new);
        }

        //echo json_encode(json_decode($result));

        foreach ($data_so_d as $so_d) {

          //Push To API

          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $so_d['item_id'],
            "qty_plus" => 0,
            "qty_minus" => intval($so_d['qty'])
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item - Cancel SO"));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);
          //echo json_encode(json_decode($result));

        }

        $data_ncm = $this->MOutbound->get_ncm($so_id_be);

        if ($data_ncm != false) {

          //Push To API

          $url =  base_url()  . "outbound/outbound_api/update_product_stock";
          $ch = curl_init($url);

          $data_post = array(
            "sku" => $data_ncm['ncm_item_id'],
            "qty_plus" => 0,
            "qty_minus" => $data_ncm['ncm_qty']
          );

          $data = array("stockItems" => array($data_post));
          $post = json_encode(array("data" => $data, "key" => "Item NCM - Cancel SO"));

          $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

          curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          $result = curl_exec($ch);
          curl_close($ch);

          $status = json_decode($result);
          // if ($status[0]->success == true || $status[0]->success == 1) {
          //   $ncm_item = array($data_ncm);
          // } else {
          //   $this->response(array(
          //     'message' => 'update product stock gagal'
          //   ), 404);
          // }
        }

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
          "customer_id" => intval($member_id_fe),
          "walletamount" => intval($data_so_cancel['totalharga'])
        );

        $data_deposit_post = array(
          "walletdata"        => array($walletdata),
          "walletactiontype"  => 'credit',
          "walletnote"        => "Cancel SO - $remark",
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        //print_r($post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $this->response(array(
          'message' => 'sukses'
        ), 200);
      }

      $this->response(array(
        'message' => 'gagal'
      ), 200);
    }
  }

  function create_product_post()
  {
    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $data_product    = $data_param['product'];

      //print_r($data_product); die();

      $item_id    = $data_product['item_id'];
      $item_name  = $data_product['item_name'];
      $topup     = $data_product['topup'];
      //$satuan     = $data_product['satuan'];
      //$hpp        = $data_product['hpp'];
      //$sales      = $data_product['sales'];
      $created    = $data_product['created'];
      //$createdby  = $data_product['createdby'];
      //$material   = $data_product['material'];
      $price      = $data_product['price'];
      $price_cust = $data_product['price_cust'];
      $price_bv   = $data_product['price_bv'];
      $price_pv   = $data_product['price_pv'];
      $type_id    = $data_product['type_id'];
      $type_id_fe = "simple";
      $weight     = 1;

      $is_starter_kit = ($type_id == "1" ? 1 : 0);
      $is_topup = ($topup == "Yes" ? "1" : "0");

      $visibility = ($data_product['is_sales'] == 'Yes' ? "4" : "1");

      //insertitem($item_id,$name,$satuan,$hpp,$sales,$createdf,$createdby,$material,$purchasing,$updatedf,$updateby,$price,$price2)
      $stock_item_attr = array(
        "is_in_stock" => true,
        "is_qty_decimal" => false,
        "show_default_notification_message" => false,
        "use_config_min_qty" => true,
        "min_qty" => 0,
        "use_config_min_sale_qty" => 1,
        "min_sale_qty" => 1,
        "use_config_max_sale_qty" => true,
        "max_sale_qty" => 10000,
        "use_config_backorders" => true,
        "backorders" => 0,
        "use_config_notify_stock_qty" => true,
        "notify_stock_qty" => 1,
        "use_config_qty_increments" => true,
        "qty_increments" => 0,
        "use_config_enable_qty_inc" => true,
        "enable_qty_increments" => false,
        "use_config_manage_stock" => true,
        "manage_stock" => true,
        "low_stock_date" => null,
        "is_decimal_divided" => false,
        "stock_status_changed_auto" => 0
      );

      $extension_attributes = array(
        "website_ids" => array(1),
        "stock_item" => $stock_item_attr
      );

      $data_post = array(
        "sku" => $item_id,
        "name" => $item_name,
        "attribute_set_id" => 4,
        "price" => $price_cust,
        "status" => 1, //1=Enable, 1=Disable 
        "visibility" => $visibility, // Default New product
        "type_id" => $type_id_fe,
        "created_at" => $created,
        "updated_at" => $created,
        "weight" => $weight,
        "extension_attributes" => $extension_attributes,
        //"stock_item" => $stock_item,
        "product_links" => [],
        "options" => [],
        "tier_prices" => array(array("customer_group_id" => 1, "qty" => 1, "value" => $price, "extension_attributes" => array("website_id" => 0))),
        "custom_attributes" => array(array("attribute_code" => "is_promo_topup", "value" => $is_topup), array("attribute_code" => "is_starter_kit", "value" => $is_starter_kit), array("attribute_code" => "informasi_bv", "value" => $price_bv), array("attribute_code" => "informasi_pv", "value" => $price_pv))
      );

      $data_post = array("product" => $data_post);

      $post = json_encode($data_post, true);

      //print_r($post); die();

      $url = get_url_api() . 'rest/all/V1/products';
      $ch = curl_init($url);

      $token = get_token();

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      activity_log('Create Product', 'outbound', $post, $url, $result);

      //echo json_encode(json_decode($result)); die();

      if (isset(json_decode($result)->id)) {
        $itemid_fe = json_decode($result)->id;
      } else {
        $this->response(array(
          'message' => "Gagal"
        ), 200);
      }

      $this->MOutbound->update_item_id_fe($item_id, $itemid_fe);

      $this->response(array(
        'product' => json_decode($result)
      ), 200);
    }
  }

  function get_member_by_id($member_id)
  {

    $array = array('id' => $member_id);
    $q = $this->db->select("*")
      ->from('member')
      ->where($array)
      ->get();
    return $var = ($q->num_rows() > 0) ? $q->row() : false;
  }

  function update_product_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $data_product   = $data_param['product'];
      $item_id        = $data_product['sku'];

      $data_post = array("product" => $data_product);

      $token = get_token();

      $post = json_encode($data_post, true);

      $url = get_url_api() . 'rest/all/V1/products/' . $item_id;

      $ch = curl_init($url);

      $authorization = "Authorization: Bearer $token";
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);

      //echo json_encode(json_decode($result)); die();
      activity_log('Update Product', 'outbound', $post, $url, $result);

      $this->response(array(
        'product' => json_decode($result)
      ), 200);
    }
  }

  function memberupdateprofile_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $data_customer = $data_param['data_customer'];
      $member_id_fe = $data_param['member_id_fe'];

      $token = get_token();

      $url = get_url_api() . 'rest/default/V1/customers/' . $member_id_fe;
      $ch = curl_init($url);

      $post = json_encode($data_customer);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);

      $member_fe = json_decode($result, true);

      $id_member_fe = $member_fe['id'];
      $address_id_fe = $member_fe['addresses'][0]['id'];

      $this->db->update('member', array('address_id_fe' => $address_id_fe), array('member_id_fe' => $id_member_fe));

      curl_close($ch);

      activity_log('Member Update Profile', 'outbound', $post, $url, $result);

      $this->response(array(
        'message' => json_decode($result)
      ), 200);

    }
  }

  function create_customer_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    //print_r($data_param); die();

    if (isset($data_param)) {

      $token = get_token();

      $url = get_url_api() . 'rest/V2/customers';
      //$url = 'https://ecommerce-semipro.uni-health.com/rest/V2/customers';
      $ch = curl_init($url);

      $post = json_encode($data_param);

      $authorization = "Authorization: Bearer " . $token;
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);

      curl_close($ch);

      activity_log('Create Member', 'outbound', $post, $url, $result);

      $member_fe = json_decode($result, true);

      $id_member_fe = $member_fe['id'];
      $address_id_fe = $member_fe['addresses'][0]['id'];

      $this->db->update('member', array('member_id_fe' => $id_member_fe, 'address_id_fe' => $address_id_fe), array('email' => $data_param['customer']['email']));

      $this->response(json_encode(json_decode($result, true)));
    }
  }

  function update_insert_customer_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $token = get_token();

      $url = get_url_api() . 'rest/default/v1/customers';
      $ch = curl_init($url);

      $post = json_encode($data_param);

      $authorization = "Authorization: Bearer " . $token;
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);

      curl_close($ch);

      $member_fe = json_decode($result, true);

      $id_member_fe = $member_fe['id'];
      $address_id_fe = $member_fe['addresses'][0]['id'];

      $this->db->update('member', array('member_id_fe' => $id_member_fe, 'address_id_fe' => $address_id_fe), array('email' => $data_param['email']));

      $this->response(json_encode(json_decode($result, true)));
    }
  }

  function create_order_registrasi_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $token = get_token();

      $url = get_url_api() . 'rest/V1/order/create';
      $ch = curl_init($url);

      $post = json_encode($data_param);

      $authorization = "Authorization: Bearer " . $token;
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);

      curl_close($ch);

      activity_log('Create Order Register', 'outbound', $post, $url, $result);

      $this->response(json_encode(json_decode($result, true)));
    }
  }

  function update_product_stock_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $token = get_token();

      $url = get_url_api() . 'rest/V1/updateBatchStock';
      $ch = curl_init($url);

      $post = json_encode($data_param['data']);
      $key = $data_param['key'];

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      //echo json_encode(json_decode($result));
      activity_log($key, 'outbound', $post, $url, $result);

      $this->response(
        json_decode($result)
      );
    }
  }

  //START deposit outbound ASP 20200624
  function depositewallet_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $deposit_id_be  = $data_param['deposit_id_be'];
      $member_id      = $data_param['member_id'];
      $total_amount   = $data_param['total_amount'];
      $remark         = $deposit_id_be . " - " . $this->db->escape_str($data_param['remark']);
      $actiontype     = $data_param['actiontype'];

      $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);
      if (!$member_id_fe) {
        $this->response(array(
          'message' => 'Member ID Mapping Not Found'
        ), 405);
      }

      $walletdata = array(
        "customer_id" => intval($member_id_fe),
        "walletamount" => intval($total_amount)
      );

      $data_deposit_post = array(
        "walletdata"        => array($walletdata),
        "walletactiontype"  => $actiontype,
        "walletnote"        => $remark
      );

      $token = get_token();

      $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
      $ch = curl_init($url);

      $post = json_encode($data_deposit_post);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      activity_log('Deposit Ewallet', 'outbound', $data_deposit_post, $url, $result);

      return json_encode(json_decode($result));
    }
  }

  //EOF deposit outbound ASP 20200624

  function update_product2_put()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $data_product    = $data_param['product'];

      //print_r($data_product); die();

      $id_fe    = $data_product['id_fe'];
      $item_id    = $data_product['item_id'];
      $item_name  = $data_product['item_name'];
      //$satuan     = $data_product['satuan'];
      //$hpp        = $data_product['hpp'];
      //$sales      = $data_product['sales'];
      $updated    = $data_product['updated'];
      //$createdby  = $data_product['createdby'];
      //$material   = $data_product['material'];
      $price      = $data_product['price'];
      $price_cust = $data_product['price_cust'];
      $price_bv   = $data_product['price_bv'];
      $price_pv   = $data_product['price_pv'];
      $type_id    = $data_product['type_id'];
      $type_id_fe    = "simple";

      $is_starter_kit = ($type_id == "1" ? 1 : 0);

      $data_post = array(
        "id" => $id_fe,
        "sku" => $item_id,
        "name" => $item_name,
        "attribute_set_id" => 4,
        "price" => $price_cust,
        "status" => 1,
        "visibility" => 4,
        "type_id" => $type_id_fe,
        "updated_at" => $updated,
        "weight" => 1,
        "product_links" => [],
        "options" => [],
        "tier_prices" => [],
        "custom_attributes" => array(array("attribute_code" => "is_starter_kit", "value" => $is_starter_kit), array("attribute_code" => "informasi_bv", "value" => $price_bv), array("attribute_code" => "informasi_pv", "value" => $price_pv), array("attribute_code" => "am_shipping_type", "value" => "0"))
      );

      $data_post = array("product" => $data_post);



      $post = json_encode($data_post, true);

      $url = get_url_api() . 'rest/all/V1/products/' . $item_id;
      $ch = curl_init($url);

      $token = get_token();

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      activity_log('Update Product 2', 'outbound', $post, $url, $result);
      //echo json_encode(json_decode($result)); die();

      $this->response(array(
        'product' => json_decode($result)
      ), 200);
    }

    //START deposit outbound ASP 20200624
    function depositewallet_post()
    {

      $data_param = json_decode(file_get_contents('php://input'), true);

      if (isset($data_param)) {

        $deposit_id_be  = $data_param['deposit_id_be'];
        $member_id      = $data_param['member_id'];
        $total_amount   = $data_param['total_amount'];
        $remark         = $deposit_id_be . " - " . $this->db->escape_str($data_param['remark']);
        $actiontype     = $data_param['actiontype'];

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
          "customer_id" => $member_id_fe,
          "walletammount" => $total_amount
        );

        $data_deposit_post = array(
          "walletdata"        => $walletdata,
          "walletactiontype"  => $actiontype,
          "walletnote"        => $remark,
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V2/ewallet/adjustment/data';

        if (!$member_id_fe) {

          activity_log('Deposit Ewallet', 'outbound', $data_deposit_post, $url, 'Failed : Member ID Mapping Not Found');

          $this->response(array(
            'message' => 'Member ID Mapping Not Found'
          ), 405);
        }

        $ch = curl_init($url);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_deposit_post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Deposit Ewallet', 'outbound', $data_deposit_post, $url, $result);

        echo json_encode(json_decode($result));
      }
    }
    //EOF deposit outbound ASP 20200624


  }

  function update_status_complete_order_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $so_id_be  = $data_param['so_id_be'];

      $so_id_fe = $this->MSales->getSoIdFeFromIdBe($so_id_be);

      if (!$so_id_fe) {
        $this->response(array(
          'message' => 'SO ID FE Not Found'
        ), 405);
      }

      $entity = array(
        "entity_id" => $so_id_fe,
        "state" => "completed",
        "status" => "completed"
      );

      $data_post = array("entity" => $entity);

      $token = get_token();

      $url = get_url_api() . 'rest/default/V1/orders';
      $ch = curl_init($url);

      $post = json_encode($data_post);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);
      curl_close($ch);

      activity_log('Update Status Completed', 'outbound', $post, $url, $result);

      $this->response(array(
        'message' => 'sukses'
      ), 200);
    }
  }

  function update_soid_be_to_fe_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      $so_id_be  = $data_param['so_id_be'];

      $so_id_fe = $this->MSales->getSoIdFeFromIdBe($so_id_be);

      if (!$so_id_fe) {
        $this->response(array(
          'message' => 'SO ID FE Not Found'
        ), 405);
      }


      $entity = array(
        "entity_id" => intval($so_id_fe),
        "extension_attributes" => array("mp_order_attributes" => array(array("attribute_code" => "so_id_be", "value" => "$so_id_be", "show_in_frontend_order" => 1)))
      );

      $data_post = array("entity" => $entity);

      $token = get_token();

      $url = get_url_api() . 'rest/V1/order/' . $so_id_fe;
      $ch = curl_init($url);

      $post = json_encode($data_post);

      //activity_log('Update so_id_be', 'outbound', $post, $url, $result);

      $authorization = "Authorization: Bearer $token";

      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      $result = curl_exec($ch);

      curl_close($ch);

      activity_log('Update so_id_be', 'outbound', $post, $url, $result);

      //print_r(json_encode(json_decode($result)));

      $this->response(array(
        $result
      ), 200);
    }
  }

  function reject_payment_confirm_post()
  {

    $data_param = json_decode(file_get_contents('php://input'), true);

    if (isset($data_param)) {

      if (!isset($data_param['id_list'])) {

        $result = array(
          'message' => 'Payment ID is required'
        );

        $url_log = base_url()  . "outbound/outbound_api/reject_payment_confirm";
        activity_log('Reject Payment Confirm', 'inbound', json_encode($data_param), $url_log, json_encode($result));

        $this->response($result, 405);
      }

      $id_list = $data_param['id_list'];
      $remark = $data_param['remark'];

      $rejected_payment = $this->MSales->payment_rejected($id_list, $remark);
      $data_payment_list = $this->MSales->get_payment_confirm_by_id_list($id_list);

      foreach ($data_payment_list as $payment_d) {
        $so_id_fe  = $payment_d['so_id_fe'];

        if (!$so_id_fe) {
          $this->response(array(
            'message' => 'SO ID FE Not Found'
          ), 405);
        }

        $data_post = array("data" => array("order_id" => $so_id_fe));

        $token = get_token();

        $url = get_url_api() . 'rest/V1/payment/deletePayment';
        $ch = curl_init($url);

        $post = json_encode($data_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Reject Payment SO', 'outbound', $post, $url, $result);

        //print_r(json_encode(json_decode($result)));

        $this->response(array(
          $result
        ), 200);
      }
    }
  }
}
