<?php

function get_url_api()
{
    //$url_api = "https://ecommerce-qa.uni-health.com/";
    //$url_api = "https://ecommerce-semipro.uni-health.com/";
    $url_api = "https://ecommerce-semipro.uni-health.com/";
    return $url_api;
}

function get_token()
{
    // $data = [
    //     'username' => 'solutechglobal',
    //     'password' => 'Admin123'
    // ];

    $data = [
        'username' => 'solutech',
        'password' => 'Admin123'
    ];

    $url = get_url_api() . 'rest/V1/integration/admin/token';

    $ch = curl_init($url);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);

    if ($error !== '') {
        throw new \Exception($error);
    };

    return json_decode($response);
}

function get_real_ip()
{

    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

$nama_svr = 'localhost';
$nama_db = 'db_soho';
$nama_usr = 'root';
$pwd_usr = '';

// $nama_svr = 'localhost';
// $nama_db = 'soho_dev_20200813';
// $nama_usr = 'admin';
// $pwd_usr = 'P@ssw0rd';


$con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
if (!$con) {
    trigger_error("Problem connecting to server");
}
$db =  mysqli_select_db($con, $nama_db);
if (!$db) {
    trigger_error("Problem selecting database");
}
//--------------------------- EOF database ------------------------//


function activity_log($key, $type, $data, $url, $response)
{

    $nama_svr = 'localhost';
    $nama_db = 'db_soho';
    $nama_usr = 'root';
    $pwd_usr = '';

    // $nama_svr = 'localhost';
    // $nama_db = 'soho_dev_20200813';
    // $nama_usr = 'admin';
    // $pwd_usr = 'P@ssw0rd';

    $con = mysqli_connect($nama_svr, $nama_usr, $pwd_usr);
    if (!$con) {
        trigger_error("Problem connecting to server");
    }
    $db =  mysqli_select_db($con, $nama_db);
    if (!$db) {
        trigger_error("Problem selecting database");
    }

    $qinsLog = "
    insert into log_api_ecommerce (key,type,input,url,response,ip_address,created_at)
    values
    ('" . $key . "','" . $type . "','" . $data . "','" . $url . "','" . $response . "','127.0.0.1','" . date('Y-m-d H:i:s') . "')
    ";
    mysqli_query($con, $qinsLog) or die(mysqli_error($con));
}

$queryMember = "
    SELECT m.id, m.nama
    ,m.ewallet
    ,m.member_id_fe
    ,CASE WHEN u.banned_id > 0 THEN 0 ELSE 1 END AS banned
    FROM member m
    LEFT JOIN users u ON u.id = m.id
    where m.ewallet <> 0
    LIMIT 0,10000";

$queryData    = mysqli_query($con, $queryMember) or die(mysqli_error($con));

// var_dump($queryData); die();

echo "start retrive data \n";

$xx = 1;
while ($row = mysqli_fetch_array($queryData, MYSQLI_ASSOC)) {

    $member_id      = $row['id'];
    $member_id_fe   = $row['member_id_fe'];
    $ewallet        = $row['ewallet'];

    $walletdata = array(
        "customer_id" => intval($member_id_fe),
        "walletamount" => intval($ewallet)
    );

    $data_deposit_post = array(
        "walletdata"        => array($walletdata),
        "walletactiontype"  => 'credit',
        "walletnote"        => 'Saldo Awal Ewallet',
    );

    $url = 'https://ecommerce-semipro.uni-health.com/rest/V1/ewallet/adjustment/data';
    $ch = curl_init($url);
    $post = json_encode($data_deposit_post);

    $token = get_token();

    $authorization = "Authorization: Bearer $token";

    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    curl_close($ch);
    //End Update Ewallet

    //var_dump($result);
    if ($result) {
        // start activity log
        $tglsekarang = date('Y-m-d H:i:s');
        $qinsLog = "
        INSERT INTO log_api_ecommerce_ewallet (`key`, `type`, `input`, `url`, `response`, `ip_address`, `created_at`) 
        VALUES ('migrasi_ewallet', '$member_id', '$post','$url','".mysqli_real_escape_string($con,$result)."','127.0.0.1','$tglsekarang')
        ";

        mysqli_query($con,$qinsLog) or die(mysqli_error($con));

        $status_update_ewallet = json_decode($result, true);
        if (($status_update_ewallet[0]['success']) == 1) {
            echo "[" . date('Y-m-d H:i:s') . "] - No. " . $xx . " - " . $row['id'] . " - success \n";
        } else {
            echo "[" . date('Y-m-d H:i:s') . "] - No. " . $xx . " - " . $row['id'] . " - failed \n";
        }

        $xx++;
    }
}
echo 'done /n/r';
//----------------------------- database ---------------------------//
mysqli_close($con);
//--------------------------- EOF database ------------------------//
