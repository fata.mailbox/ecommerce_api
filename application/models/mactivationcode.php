<?php
class MActivationcode extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    public function addActivationCode($item_id, $stcid, $whsid, $createdby){
        $qty = 1;
        $yn = 1;

        //$empid = 'API';
        $empid = $createdby;
        
        //$huruf_master = 'QWERTYUIOPASDFGHJKLZXCVBNM';
        $angka_master = '01234567890123456789012345678901234567890123456789';
        
        # Generate dynamic salt
        $hash = sha1(microtime()); 
        
        $digit = $this->_getCountActivationCode();
        $digit = strlen($digit->l_count + $qty)+1;
        
        $x = 0;
        while ($x < $qty) {
            /* $huruf = '';
            for ($i=0; $i<2; $i++) {
                $huruf .= $huruf_master{rand(0,25)};
            }
            */
            $angka = '';
            for ($i=0; $i<$digit; $i++) {
                $angka .= $angka_master{rand(0,49)};
            }
            //$userid = $huruf.$angka;
            switch ($digit){
                case 1:
                    $userid = "0000000".$angka;
                    break;
                case 2:
                    $userid = "000000".$angka;
                    break;
                case 3:
                    $userid = "00000".$angka;
                    break;
                case 4:
                    $userid = "0000".$angka;
                    break;
                case 5:
                    $userid = "000".$angka;
                    break;
                case 6:
                    $userid = "00".$angka;
                    break;
                case 7:
                    $userid = "0".$angka;
                    break;
                case 8:
                    $userid = $angka;
                    break;
            }
			
			/* Created by Boby 20130620 */
			if($yn==1){
				$userid = '1'.substr($userid, -7);
			}else{
				if(substr($userid, -8,1)=='1'){
					$userid = '2'.substr($userid, -7);
				}
			}
			/* End created by Boby 20130620 */
            
            if(!$this->_check_memberid($userid)){
                $code = md5(sha1(sha1($x.$userid.$qty.$hash)));
                $code = strtolower(substr($code,9,12));
                $data = array(
                    'stockiest_id' => 0,
                    'member_id' => $userid,
                    'code'  => $code,
					'item_id' => $item_id,
                    'warehouse_id'  => $whsid,
                    'status'  => 'used',
                    'created' => date('Y-m-d H:i:s', now()),
                    'createdby' => $empid
                );
                $this->db->insert('activation_code',$data);
                $x++;
            }
        }
        return $data;
    }

    public function getActivationCode($act_code, $member_id){
		$q = $this->db->select("*",false)
            ->from('activation_code')
            ->where('member_id',$member_id)
            ->where('status','not used')
			->where('code',$act_code)
            ->get();
            //echo $this->db->last_query();
        return $var = ($q->num_rows() > 0) ? $q->row_array() : 0;
    }

    public function getPosReg($kota_id){
        
        $q = $this->db->select("reg_position",false)
        ->from('kota')
        ->where('id',$kota_id)
        ->get();
        
        $pos_reg = $q->row_array(); 
        return $pos_reg['reg_position'];

    }

    public function getStockiestByCityId($res_pos_id, $kota_id){
        
        //$res_pos_id = 937;
		$pq = $this->db->select("a.*",false)
            ->from('member a')
            ->join('users b','a.id=b.id','left')
            ->where('a.kota_id',$kota_id)
            ->where('a.jenjang_id >=', 2)
            ->where('b.banned_id',0)
            ->order_by('a.created','ASC')
            ->get();
            
            $counti = $pq->num_rows();

            $pos_id = ($res_pos_id < $counti ? $res_pos_id+1 : 0);

            $res_pos_id = ($res_pos_id == $counti ? $res_pos_id-1 : $res_pos_id);

            // var_dump($pos_id);
            // var_dump($res_pos_id);
            // var_dump($counti); die();

		$q = $this->db->select("a.*",false)
            ->from('member a')
            ->join('users b','a.id=b.id','left')
            ->where('a.kota_id',$kota_id)
            ->where('a.jenjang_id >=', 2)
            ->where('b.banned_id',0)
            ->order_by('a.created','ASC')
            ->limit(1, $res_pos_id)
            ->get();
            //echo $this->db->last_query(); die();
        if($q->num_rows() < 1){

            $qry = "SELECT a.*
                FROM (`member a`)
                LEFT JOIN user b
                WHERE `a.kota_id`IN
                    (SELECT a.id as kota_id
                        FROM `kota` a
                        LEFT JOIN propinsi b
                        ON b.id = a.propinsi_id
                        WHERE propinsi_id = (select propinsi_id FROM kota Where kota.id = $kota_id)
                        )
                AND `a.jenjang_id` >= 2
                AND `b.banned_id` = 0
                ORDER BY `a.created` ASC
                LIMIT 1";
            
                $qp = $this->db->query($qry);

                return $var = ($qp->num_rows() > 0) ? $qp->row_array() : 0;

        }else{

            $this->db->query("update kota set reg_position = '$pos_id' where id='$kota_id'");

            return $var = ($q->num_rows() > 0) ? $q->row_array() : 0;
            
        }
            
    }

    protected function _getCountActivationCode(){
        $q=$this->db->select("count(id) as l_count",false)->from('activation_code')->where('status !=','reject')->get();
        return $var = ($q->num_rows()>0) ? $q->row() : false;
    }
    protected function _check_memberid($memberid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $memberid)
            ->get();
            //echo $this->db->last_query();
            
        if($i->num_rows() >0){
            $i->free_result();    
            return true;
        }else{
            $q = $this->db->select("member_id")	
				->from('activation_code')
                ->where('member_id',$memberid)
                ->where('status !=','reject')
                ->get();
            if($q->num_rows() >0){
                $q->free_result();        
                return true;
            }
            return false;        
        }
    }
    public function searchActivationCode($keywords=0,$num,$offset){
        $data = array();
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.member_id LIKE '$keywords%' or a.status LIKE '$keywords%' )";
        }
        else {
            //4 = Fiannce Cabang / Kasir
            if($this->session->userdata('group_id') == 4)$where = "a.warehouse_id = '".$this->session->userdata('whsid')."' AND ( a.member_id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' or a.status LIKE '$keywords%' )";
            elseif($this->session->userdata('group_id') == 11)$where = "a.warehouse_id = '1' AND a.stockiest_id ='0' and ( a.member_id LIKE '$keywords%' or a.status LIKE '$keywords%' )";
            else $where = "a.member_id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' or a.status LIKE '$keywords%'";
        }
        $this->db->select("a.id,a.useddate,a.member_id, i.`name`,a.status,a.stockiest_id,a.code,s.no_stc,a.warehouse_id,date_format(a.created,'%d-%b-%Y')as created,a.createdby,m.nama",false);
        $this->db->from('activation_code a');
        $this->db->join('member m','a.member_id=m.id','left');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
		$this->db->join('item i','a.item_id=i.id','left');
        $this->db->where($where);
        $this->db->order_by('a.status','asc');
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }    
    public function countActivationCode($keywords=0){
        if($this->session->userdata('group_id')>100){
            $memberid=$this->session->userdata('userid');
            $where = "a.stockiest_id = '$memberid' AND ( a.member_id LIKE '$keywords%' or a.status LIKE '$keywords%' )";
        }
        else {
            //4 = Fiannce Cabang / Kasir
            if($this->session->userdata('group_id') == 4)$where = "a.warehouse_id = '".$this->session->userdata('whsid')."' AND ( a.member_id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' or a.status LIKE '$keywords%' )";
            elseif($this->session->userdata('group_id') == 11)$where = "a.warehouse_id = '1' AND a.stockiest_id ='0' and ( a.member_id LIKE '$keywords%' or a.status LIKE '$keywords%' )";
            else $where = "a.member_id LIKE '$keywords%' or s.no_stc LIKE '$keywords%' or a.status LIKE '$keywords%'";
        }
        
        $this->db->select("a.id",false);
        $this->db->from('activation_code a');
        $this->db->join('stockiest s','a.stockiest_id=s.id','left');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    public function getTitipanKit($member_id){
		$jns = $this->input->post('skit');
        $q = $this->db->select("sum(t.qty)as qty",false)
            ->from('titipan t')
            ->join('item i','t.item_id=i.id','left')
            ->where('t.member_id',$member_id)
            ->where('i.type_id',1)
			->where('i.id',$jns)
            ->group_by('t.item_id')
            ->get();
            //echo $this->db->last_query();
        return $var = ($q->num_rows() > 0) ? $q->row() : 0;
    }
    public function getACNotUsed($member_id){
		$jns = $this->input->post('skit');
        $q = $this->db->select("sum(account)as account",false)
            ->from('activation_code')
            ->where('stockiest_id',$member_id)
            ->where('status','not used')
			->where('item_id',$jns)
            ->group_by('stockiest_id')
            ->get();
            //echo $this->db->last_query();
        return $var = ($q->num_rows() > 0) ? $q->row() : 0;
    }
   
    public function reject(){
        if($this->input->post('p_id')){
            $row = array();
            
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $memberid = $this->session->userdata('userid');
            if($this->session->userdata('group_id')>100)$option = $where. " and status = 'not used' and stockiest_id = '$memberid'";
            else $option = $where. " and status = 'not used'";
            
            
            $row = $this->_getAccount($option);
            if($row['account'] > 0){
                //$account = $row['account'];
                
                //$this->db->query("update activation code set ewallet_inv = ewallet_inv + ($account * 39) where id = '$memberid'");
                $data = array(
                    'status'=> 'reject',
                    'useddate' => date('Y-m-d H:i:s', now())
                );
                $this->db->update('activation_code',$data,$option);
                $this->session->set_flashdata('message','Reject activation code successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to reject!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to reject!');
        }
    }
    protected function _getAccount($option){        
        $this->db->select("sum(account) as account",false);
        $this->db->where($option);
        $q = $this->db->get('activation_code');
        if($q->num_rows() > 0){
            return $q->row_array();
        }
        return false;
    }
    
    public function getWarehouse(){
        $data = array();
        $this->db->select("id,name",false)->from('warehouse')->order_by('id','asc');
        //if($this->session->userdata('group_id') != 1)$this->db->where('id',$this->session->userdata('whsid'));
        if($this->session->userdata('group_id') > 100)$this->db->where('id',$this->session->userdata('whsid'));
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }
	
	public function getVarianSKit(){
		$memberid = $this->session->userdata('userid');
        $data = array();
		if($this->session->userdata('group_id')>100){
			$this->db	->select("i.id,i.name",false)
						->from('titipan t')
						->join('item i', 't.item_id = i.id', 'left')
						->where('i.type_id','1')
						->where('t.member_id',$memberid);
		}else{
			$this->db	->select("id,name",false)
						->from('item')
						->where('type_id','1');
			$this->db->where('sales','yes');
		}
		$this->db->order_by('id','asc');
        $q=$this->db->get();
        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[$row['id']] = $row['name'];
            }
        }
        $q->free_result();
        return $data;
    }
	
	/* Created by Boby 20130620 */
	public function getYesNo(){
		$data = array();
		$data[0] = 'No';
		$data[1] = 'Yes';
        return $data;
    }
	/* End created by Boby 20130620 */
}
?>