<?php
class MPromo extends CI_Model
{
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
    function __construct()
    {
        parent::__construct();
    }


    public function gettopupvalue($totalamount){
        $data = array();
        //$data = null;
        $q=$this->db->select("a.id as topup_id,b.item_code as item_id,c.name as item_name, case when a.multiple = 1 then FLOOR($totalamount/a.condition_value) else b.qty end as qty,c.price, a.valid_from as periode_start, a.valid_to as periode_end",false)
            ->from('topupbyvalue a')
            ->join('topupbyvalue_detail b','b.parent_id = a.topupno','LEFT')
            ->join('item c','c.id = b.item_code','LEFT')
            ->like('valid_for', '3', 'both')
            ->where('(valid_from <="'.date('Y-m-d').'" AND valid_to >= "'.date('Y-m-d').'")')
            //->where('valid_to <=',date('Y-m-d'))
            ->where('condition_value <=',$totalamount)
            ->where('status','1')
            ->where('condition_type','1')
            ->limit(1,0)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        //
        
        //echo $this->db->last_query(); die();
        return $data;
    }       

    public function gettopupbydemand($member_id){
        $data = array();
        //$data = null;
        $q=$this->db->select("a.member_id, a.item_code as item_id,c.name as item_name, a.qty,c.price, b.valid_from as periode_start, b.valid_to as periode_end",false)
            ->from('topupbydemand_detail a')
            ->join('topupbydemand b','a.parent_upload = b.topupno','LEFT')
            ->join('item c','c.id = a.item_code','LEFT')
            ->where('(b.valid_from <="'.date('Y-m-d').'" AND b.valid_to >= "'.date('Y-m-d').'")')
            ->where('a.member_id', $member_id)
            ->where('b.status','1')
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        //echo $this->db->last_query();
        return $data;
    }       

}
?>