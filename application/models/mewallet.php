<?php
class MEwallet extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    
    /*
    |--------------------------------------------------------------------------
    | signup member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-25
    |
    */
    // START Ewallet Deposit ASP 20200624
    public function getMemberIdFEByIdBe($id){
        //$data = array();
        $data = null;
        $q=$this->db->select("member_id_fe",false)
            ->from('member')
            ->where('id',$id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('member_id_fe');
        }
        return $data;
    }

    public function getBankIdByIdFe($bank_id){
        //$data = array();
        $data = null;
        $q=$this->db->select("id",false)
            ->from('bank')
            ->where('bank_id_fe',$bank_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('id');
        }
        return $data;
    }

    public function getDepositByIdFE($deposit_id_fe){
        //$data = array();
        $data = null;
        $q=$this->db->select("id",false)
            ->from('deposit')
            ->where('deposit_id_fe',$deposit_id_fe)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('id');
        }
        return $data;
    }       

    public function addDeposit($data_member){

        $data=array(
            'member_id'     => $data_member['member_id'],
            'transfer'      => $data_member['total_amount'],
            'total'         => $data_member['total_amount'],
            'bank_id'       => $data_member['bankid'],
            'tgl_transfer'  =>  $data_member['transfer_date'],
            'remark'        => $this->db->escape_str($data_member['remark']),
            'deposit_id_fe' => $data_member['deposit_id_fe'],
            'flag'          => 'mem',
            'event_id'      => 'DP1',
            'created'       => date('Y-m-d H:i:s',now()),
            'createdby'     => $data_member['member_id']
        );
            
        $this->db->insert('deposit',$data);
        
        return $data;

    }

    public function addWithdrawalApproved($data_withdrawal){
        $member_id=$this->input->post('member_id');
        $amount = str_replace(".","",$this->input->post('amount'));
        
        if($this->input->post('type_withdrawal_id') == 'wd1' or $this->input->post('type_withdrawal_id') == 'wd2')$biayaadm=5000;
        else $biayaadm=0;
        $flag = $this->input->post('flag');
        $empid=$this->session->userdata('user');
        $row = $data_withdrawal['member_id'];
        $data=array(
            'member_id' => $member_id,
            'tgl' => date('Y-m-d',now()),
            'amount' => $amount,
            'biayaadm' => $biayaadm,
            'event_id' => $this->input->post('type_withdrawal_id'),
            'flag' => $flag,
            '`status`' => 'approved',
            'account_id' => $row->account_id,
            'remarkapp' => $this->db->escape_str($this->input->post('remark')),
            'approved' => date('Y-m-d',now()),
            'approvedby' => $empid,
            'created' => date('Y-m-d H:i:s',now()),
            'createdby' => $empid,
			'tgl_transfer' => $this->input->post('dFromdate')
        );
        $this->db->insert('withdrawal',$data);
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$member_id','company','$amount','$flag','Withdrawal','$empid')");
    }

    public function getAccountID($memberid){
        $q=$this->db->select("id,account_id",false)
            ->from('member')
            ->where('id',$memberid)
            ->where('account_id >',0)
            ->get();
        return $var = ($q->num_rows()>0) ? $q->row() : false;
    }

    public function insert_deposit_ewallet($data_deposit){

        $data=array(
            'member_id'     => $data_deposit['member_id'],
            'transfer'      => $data_deposit['total_amount'],
            'total'         => $data_deposit['total_amount'],
            'bank_id'       => $data_deposit['bankid'],
            'tgl_transfer'  =>  $data_deposit['transfer_date'],
            'remark_fin'    => 'Sales Order',
            'remark'        => $this->db->escape_str($data_deposit['remark']),
            'deposit_id_fe' => $data_deposit['deposit_id_fe'],
            'flag'          => 'mem',
            'event_id'      => 'DP1',
            'approved'      => 'approved',
            'created'       => date('Y-m-d H:i:s',now()),
            'createdby'     => $data_deposit['member_id']
        );
        
        $this->db->insert('deposit',$data);

        $member_id = $data_deposit['member_id'];
        $totalbayar = $data_deposit['total_amount'];

        $deposit_id = $this->db->insert_id();

        $this->db->query("call sp_deposit('$deposit_id','$member_id','$totalbayar','mem','system')");

        //Update Saldo Ewallet
        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id"=> intval($member_id_fe),
            "walletamount"=> intval($totalbayar)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'credit',
            "walletnote"        => 'Deposit SO',
        );

        $token = get_token();
      
        $url = get_url_api().'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);
    
        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token"; 
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $result = curl_exec($ch); 
        curl_close($ch); 

        activity_log('Deposit SO', 'outbound', $post, $url, $result);
  
        return $deposit_id;

    }

    public function insert_debet_ewallet($member_id, $totalharga, $deposit_id){

        $this->db->query("call sp_ewallet_mem('company','$member_id','SO', '$deposit_id','$totalharga','$member_id')");

    }

    public function update_ewallet($member_id, $deposit_id, $totalharga, $empid, $total_trx){
        $this->db->query("call sp_ewallet_mem_ecommerce('company','$member_id','Top Up MEM', '$deposit_id','$totalharga','$empid', '$total_trx')");
    }

    public function deposit_confirmation($deposit_id_fe, $payment_type){

        $q=$this->db->select("*", false)
            ->from('deposit')
            ->where('deposit_id_fe', $deposit_id_fe)
            ->get();

        if($q->num_rows()>0){

            $deposit_id = $q->row('id');
            $member_id = $q->row('member_id');
            $total = $q->row('total');
            $tgl_transfer = $q->row('created');
            $remark = $q->row('remark');

            $this->db->query("call sp_deposit('$deposit_id','$member_id','$total','mem','system')");
            
            $order_array = array(
                "entity_id"=>"$deposit_id_fe",
                "state"=>"completed",
                "status"=>"completed"
            );

            $data_order['entity'] = $order_array;

            if($payment_type == "1"){
        
                //Update status Order ke FE
                //$this->update_status_order_ewallet($post_order);
                //$this->update_status_order_shipping($order_array);
                $this->update_status_order($data_order);

            }elseif($payment_type == "2"){
        
                //Update status Order ke FE
                //$this->update_status_order_shipping($order_array);
                $this->update_status_order($data_order);

                //Update Saldo Ewallet
                $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

                $walletdata = array(
                "customer_id"=> intval($member_id_fe),
                "walletamount"=> intval($total)
                );

                $data_deposit_post = array(
                    "walletdata"        => array($walletdata),
                    "walletactiontype"  => 'credit',
                    "walletnote"        => 'Top Up MEM',
                );

                $token = get_token();
      
                $url = get_url_api().'rest/V1/ewallet/adjustment/data';
                $ch = curl_init($url);
                    
                $post = json_encode($data_deposit_post);

                $authorization = "Authorization: Bearer $token"; 
            
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POST, 1); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post); 
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
                $result = curl_exec($ch); 
                curl_close($ch); 

                activity_log('Deposit Confirmation', 'outbound', $post, $url, $result);

                if($result){
                    $data_update = array(
                        'approved'=>'approved',
                        'total_approved'=>$total,
                        'tgl_transfer'=>date("Y-m-d", strtotime($tgl_transfer)),
                        'tgl_approved'=>date('Y-m-d H:i:s',now()),
                        'approvedby'=>'system',
                        'remark'=>$remark,
                    );
                    $this->db->update('deposit',$data_update,array('deposit_id_fe'=>$deposit_id_fe));
                }
            }else{
                //$this->update_status_order_shipping($order_array);
                $this->update_status_order($data_order);
            }

            return $deposit_id;

        }else{

            return false;

        }

    }

    public function update_status_order_shipping($post_order){

        $token = get_token();
        $entity_id = $post_order['entity_id'];

        $url = get_url_api().'rest/default/V1/order/' . $entity_id . '/ship';
        $ch = curl_init($url);
        
        $authorization = "Authorization: Bearer " . $token; 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1); 
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $result = curl_exec($ch); 

        curl_close($ch); 

        activity_log('Update Shipping Order', 'outbound', null, $url, $result);

        return $result;

    }

    public function update_status_order($data_order){

        $post_order = json_encode($data_order);
        
        $token = get_token();
      
        $url = get_url_api().'rest/all/V1/orders';
        $ch = curl_init($url);
        
        $authorization = "Authorization: Bearer " . $token;         
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json' , $authorization ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $result = curl_exec($ch); 

        curl_close($ch); 
       
        activity_log('Update Status Order', 'outbound', $post_order, $url, $result);

        return $result;

    }

    public function get_deposit_ewallet($member_id, $deposit_id){
        $deposit = array();
        $q=$this->db->select("*", false)
            ->from('deposit')
            ->where('deposit_id_fe', $deposit_id)
            ->where('member_id', $member_id)
            ->get();

        if($q->num_rows()>0){
            $deposit = $q->row_array();
        }

        return $deposit;

    }

    public function get_deposit_ewallet_pending($member_id, $deposit_id){

        $deposit = false;
        $q=$this->db->select("*", false)
            ->from('deposit')
            ->where('deposit_id_fe', $deposit_id)
            ->where('member_id', $member_id)
            ->where('approved', 'pending')
            ->get();

        if($q->num_rows()>0){
            $deposit = $q->row_array();
        }

        //echo $this->db->last_query(); die();
        return $deposit;

    }
    // EOF Ewallet Deposit ASP 20200624

    public function get_saldo_ewallet($memberid,$fromdate,$todate){
        $data=array();
        if($memberid)$where = array('s.member_id'=>$memberid,'e.date >='=>$fromdate,'e.date <='=>$todate);
        else $where = array('s.member_id'=>'company','e.date >='=>$fromdate,'e.date <='=>$todate);

        //$q=$this->db->select("'$memberid' as member_id, '$fromdate' as periode_start, '$todate' as periode_end,  s.id as ewallet_mem_d_id, concat(s.noref,' - ',s.description)as description,s.created,createdby,format(s.saldoawal,0)as fsaldoawal,format(s.kredit,0)as fkredit,format(s.debet,0)as fdebet,format(s.saldoakhir,0)as fsaldoakhir", false)
        $q=$this->db->select("'$memberid' as member_id, '$fromdate' as periode_start, '$todate' as periode_end,  s.id as ewallet_mem_d_id, s.created as createddate, s.noref as noref, s.description as description, format(s.saldoawal,0) as saldoawal,format(s.debet,0) as debet, format(s.kredit,0) as kredit,format(s.saldoakhir,0) as saldoakhir, createdby as createdby", false)
            ->from('ewallet_mem_d s')
            ->join('ewallet_mem e','s.ewallet_mem_id=e.id','left')
            ->where($where)
            ->order_by('s.id','asc')
            ->get();

        if($q->num_rows()>0){
            foreach($q->result_array() as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
	
}
