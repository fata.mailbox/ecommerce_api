<?php
class MSignup extends CI_Model{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
    |--------------------------------------------------------------------------
    | signup member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-25
    |
    */

    public function add_temp_member($data_member){        

        $cek_member_id = $this->check_memberid($data_member['member_id_fe']);

        if($cek_member_id==false){

            $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/inbound/sales_order/cancelorderppending_signup";
            $ch  = curl_init($url);

            $post = json_encode(array("member_id_fe" => $data_member['member_id_fe']));

            $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $result = curl_exec($ch);
            curl_close($ch);

        }

        # Generate dynamic salt
        $hash = sha1(microtime()); 
        
        # Hash password
        //$password = substr($data_member['ktp'],0,8);
        $password = base64_decode($data_member['password']);
		$pin = $data_member['temp_member_id'];
        $password_enc = substr(sha1(sha1(md5($hash.$password))),5,15);
        $pin_enc = substr(sha1(sha1(md5($hash.$pin))),3,7);

		$data = array(
            'temp_member_id'    =>$data_member['temp_member_id'],
            'sponsor_id'        =>$data_member['sponsor_id'],
            'stockiest_id'      =>0,
            'enroller_id'       =>$data_member['upline_id'],
            'password'          =>$password_enc,
            'hash'              =>$hash,
            'nama'              =>$data_member['full_name'],
            'email'              =>$data_member['email'],
            'jk'                =>$data_member['gender'],
            'tgllahir'          =>$data_member['dob'],
            'tempatlahir'       =>$data_member['pob'],
            'noktp'             =>$data_member['ktp'],
            'hp'                =>$data_member['hp'],
            'alamat'            =>$data_member['address'],
            'kota_id'           =>$data_member['city'],
            'activation_code'   =>$data_member['activation_code'],
            'kit'               =>$data_member['item_id'],
            'created'           =>date('Y-m-d',now()),
            'createdby'         =>$data_member['createdby'],
            'member_id_fe'      =>$data_member['member_id_fe']
        );
        $this->db->insert('temp_member_fe',$data);

        return $data;
    }

    public function add_temp_so($data_so_header){
       $data = array(
            'so_id_fe'                =>$data_so_header['so_id_fe'],
            'tgl'        =>$data_so_header['so_date'],
            'stockiest_id'      =>$data_so_header['stockiest_id'],
            'member_id'          =>$data_so_header['member_id'],
            'totalharga_'        =>$data_so_header['total_harga_'],
            'totalharga'         =>$data_so_header['total_harga'],
            'total_pv_'              =>$data_so_header['total_pv_'],
            'total_bv_'                =>$data_so_header['total_bv_'],
            'total_discount_rp'          =>$data_so_header['total_discount_rp'],
            'total_discount_rp_voucher'       =>$data_so_header['total_discount_rp_voucher'],
            'total_discount_pv_voucher'             =>$data_so_header['total_discount_pv_voucher'],
            'total_discount_bv_voucher'                =>$data_so_header['total_discount_bv_voucher'],
            'totalpv'           =>$data_so_header['total_pv'],
            'totalbv'          =>$data_so_header['total_bv'],
            'vouchercode'               =>$data_so_header['vouchercode'],
            'kit'               =>$data_so_header['kit'],
            'remark'               =>$data_so_header['remark'],
            'delivery_addr_id'               =>$data_so_header['delivery_addr_id'],
            'delivery_city_id'               =>$data_so_header['delivery_city_id'],
            'shipping_price'               =>$data_so_header['shipping_price'],
            'created'               =>$data_so_header['created'],
            'createdby'               =>$data_so_header['createdby'],
            'status1'               =>$data_so_header['status1'],
            'status2'               =>$data_so_header['status2'],
            'status'               =>$data_so_header['status'],
            'warehouse_id'               =>$data_so_header['warehouse_id'],
            'payment_type'               =>$data_so_header['payment_type']
        );
        $this->db->insert('temp_so',$data);

        $last_id = $this->db->insert_id();
        return $last_id;
    }


    public function add_temp_so_d($data_so_detail){
       $data = array(
            'so_d_id_fe'   =>$data_so_detail['so_d_id_fe'],
            'so_id'        =>$data_so_detail['so_id'],
            'so_id_fe'        =>$data_so_detail['so_id_fe'],
            'item_id'      =>$data_so_detail['item_id'],
            'qty'          =>$data_so_detail['qty'],
            'harga'        =>$data_so_detail['harga_'],
            'harga_'         =>$data_so_detail['harga_'],
            'pv_'              =>$data_so_detail['pv_'],
            'bv_'                =>$data_so_detail['bv_'],
            'discount_rp'          =>$data_so_detail['discount_rp'],
            'discount_rp_voucher'       =>$data_so_detail['discount_rp_voucher'],
            'discount_pv_voucher'             =>$data_so_detail['discount_pv_voucher'],
            'discount_bv_voucher'                =>$data_so_detail['discount_bv_voucher'],
            'harga'           =>$data_so_detail['harga'],
            'pv'          =>$data_so_detail['pv'],
            'bv'               =>$data_so_detail['bv'],
            'jmlharga_'               =>$data_so_detail['jmlharga_'],
            'jmlpv_'               =>$data_so_detail['jmlpv_'],
            'jmlbv_'               =>$data_so_detail['jmlbv_'],
            'jmldiscount_rp'               =>$data_so_detail['jmldiscount_rp'],
            'jmldiscount_rp_voucher'               =>$data_so_detail['jmldiscount_rp_voucher'],
            'jmldiscount_pv_voucher'               =>$data_so_detail['jmldiscount_pv_voucher'],
            'jmlharga'               =>$data_so_detail['jmlharga'],
            'jmlpv'               =>$data_so_detail['jmlpv'],
            'jmlpv'               =>$data_so_detail['jmlpv'],
            'warehouse_id'               =>$data_so_detail['warehouse_id'],
        );
        $this->db->insert('temp_so_d',$data);

        return true;
    }

    public function update_member_addr($member_id, $addr_id){

        $this->db->update('member',array('delivery_addr'=>$addr_id),array('id'=>$member_id));

        return true;
    }

    public function proses_tail($l_soid, $l_whsid, $empid){

        $this->db->query("CALL sp_so_temp_tail('$l_soid', '$l_whsid', '$empid')");

        return true;
    }
    public function get_temp_ncm($so_id, $member_id, $so_id_fe){
        
        $query = $this->db->query("SELECT nc.warehouse_id, nc.remarkapp, nc.status, nc.created as ncm_created, ref.periode, ref.trx, ncd.hpp, ncd.event_id, ncd.flag, ref.trx_id AS so_id_be, $so_id_fe as so_id_fe, ref.nc_id as ncm_id, nc.remark as ncm_remark, nc.date as ncm_createddate, ncd.id as ncm_d_id, ncd.item_id as ncm_item_id, ncd.qty as ncm_qty
                            FROM temp_ref_trx_nc ref
                            LEFT JOIN temp_ncm nc ON ref.nc_id = nc.id
                            LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
                            WHERE ref.member_id = '$member_id'
                            AND ref.trx_id = '$so_id'");

        // GROUP BY ref.id ");
        //$query->free_result();

        //echo $this->db->last_query(); die();

        if($query->num_rows() > 0){
            $var = $query->result_array();
        }else{
            $var = false;
        }

        return $var;
        
    }

    public function check_introducerid($introducerid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $introducerid)
            ->get();
            //echo $this->db->last_query();
			
	return $var = ($i->num_rows() > 0) ? false : true;
    }
    public function check_captcha($confirmCaptcha)
   {
           $captchaWord = $this->session->userdata('captchaWord');
           
           $this->session->unset_userdata('captchaWord');
           if(strcasecmp($captchaWord, $confirmCaptcha) == 0)
           {
                return true;
           } return false;
   }
    public function check_placementid($placementid){
        $i = $this->db->select("id")	
            ->from('member')
            ->where('id', $placementid)
            ->get();
            
        return $var = ($i->num_rows() > 0) ? false : true;
    
    }
			
    public function sponsor_validate($sponsor_id, $upline_id){

        $q = $this->db->select("a.*",false)
            ->from('member a')
            ->join('users b','a.id=b.id','left')
            ->where('a.id', $sponsor_id)
            ->where('b.banned_id',0)
            ->get();
        
        if($q->num_rows() > 0){

            $r = $this->db->select("a.*",false)
                ->from('member a')
                ->join('users b','a.id=b.id','left')
                ->where('a.id', $upline_id)
                ->where('b.banned_id',0)
                ->get();
            
            return $var = ($r->num_rows() > 0) ? 'sukses' : 'upline_id_gagal';

        }else{

            return 'sponsor_id_gagal';
        
        }
			
	    //return $var = ($q->num_rows() > 0) ? true : false;
    }
    
    public function cek_kit_yes_no($so_id_fe){

        $q = $this->db->select("*",false)
            ->from('temp_so')
            ->where('so_id_fe', $so_id_fe)
            ->where('kit','Y')
            ->get();
        
            //echo $this->db->last_query(); die();
            
        if($q->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function cek_item_skit($item_id){

        $q = $this->db->select("type_id",false)
            ->from('item')
            ->where('id', $item_id)
            ->where('type_id','1')
            ->get();
        
        $items = $q->row('type_id');
            
        if($q->num_rows() > 0){
            if($items == 1){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function password_validate($member_id, $password){

        $q = $this->db->select("*",false)
            ->from('users')
            ->where('id', $member_id)
            ->where('password', $password)
            ->where('banned_id',0)
            ->get();
        
        if($q->num_rows() > 0){
            
            return $var = ($q->num_rows() > 0) ? true : false;

        }else{

            return false;
        
        }
			
	    //return $var = ($q->num_rows() > 0) ? true : false;
    }
    
    public function member_activation($member_id, $so_id_fe){

        $q = $this->db->select("a.*, b.*",false)
            ->from('temp_member_fe a')
            ->join('temp_so b','a.temp_member_id=b.member_id','left')
            ->where('a.temp_member_id', $member_id)
            ->where('b.so_id_fe', $so_id_fe)
            ->get();
            
            //echo $this->db->last_query(); die();

        if($q->num_rows() > 0){

            $member = $q->row_array();

            $pin_enc = substr(sha1(sha1(md5($member['hash'].$member_id))),3,7);
            $hash = $member['hash'];
            $sponsor_id     = $member['sponsor_id'];
            $upline_id      = $member['enroller_id'];
            $password      = $member['password'];
            $warehouse_id   = $member['warehouse_id'];
            $activation_code= $member['activation_code'];
            $email      = $member['email'];
            $alamat      = $member['alamat'];
            $noktp      = $member['noktp'];
            $kota_id      = $member['kota_id'];
            $tempatlahir      = $member['tempatlahir'];
            $tgllahir      = $member['tgllahir'];
            $jk      = $member['jk'];
            $hp      = $member['hp'];
            $nama      = $member['nama'];
            $createdby      = $member['createdby'];
            $email      = $member['email'];
            $totalharga      = $member['totalharga'];
            $item_kit      = $member['kit'];

            $this->db->query("INSERT INTO users(id,username,PASSWORD,pin,HASH,group_id,warehouse_id,activation_code)
            VALUES('$member_id', '$member_id', '$member[password]', '$pin_enc', '$member[hash]', 101, '$member[warehouse_id]', '$member[activation_code]')");
            
            $this->db->query(" UPDATE activation_code SET STATUS='used',useddate=NOW() WHERE member_id='$member_id' AND CODE= '$member[activation_code]'");
            
            $qitem = $this->db->select("*",false)
            ->from('temp_so_d')
            ->where('so_id_fe', $so_id_fe)
            ->order_by('id', "ASC")
            ->limit(1)
            ->get();

            $items = $qitem->row_array();
            $item_id = $items['item_id'];

            $this->db->query("call sp_member_activation(
                '$upline_id',
                '$member_id',
                '$activation_code',
                '$sponsor_id',
                '$nama',
                '$hp',
                '$email',
                '$warehouse_id',
                '$password',
                '$pin_enc',
                '$hash',
                '$totalharga',
                '$item_id',
                '$alamat',
                '$noktp',
                '$kota_id',
                '$tempatlahir',
                '$tgllahir',
                '$jk',
                '$so_id_fe',
                'system')");
            
            //echo $this->db->last_query();
            
            $this->db->query("DELETE from temp_member_fe WHERE temp_member_id='$member_id'");

            $this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe')");

            return $member;

        }else{
            
            return false;
        
        }
			
	    //return $var = ($q->num_rows() > 0) ? true : false;
    }
    
    public function payment_confirmation($member_id, $so_id_fe){

        $q = $this->db->select("*",false)
            ->from('temp_so')
            ->where('member_id', $member_id)
            ->where('so_id_fe', $so_id_fe)
            ->get();

            //echo $this->db->last_query(); die();

        if($q->num_rows() > 0){

            $this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe')");

            $this->db->query("DELETE from temp_so WHERE so_id_fe='$so_id_fe'");
            $this->db->query("DELETE from temp_so_d WHERE so_id_fe='$so_id_fe'");

            $array_data = $q->row_array();

            return true;

        }else{
            
            return false;
        
        }
			
	    //return $var = ($q->num_rows() > 0) ? true : false;
    }
    
    public function check_crossline($p,$i){
        $data =array();
        $q = $this->db->query("SELECT f_check_crossline('$p','$i') as l_result");
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        //print_r($data);
        $q->free_result();
        return $data['l_result'];
    }
    
    public function _check_userid($p,$p2){
        $array = array('member_id'=>$p,'code'=>$p2,'status'=>'not used');
        $q = $this->db->select("account")	
            ->from('activation_code')
            ->where($array)
            ->get();
	    return $var = ($q->num_rows() > 0) ? false : true;
    }

    public function check_memberid($member_id_fe){
        $array = array('member_id_fe'=>$member_id_fe);
        $q = $this->db->select("temp_member_id")	
            ->from('temp_member_fe')
            ->where($array)
            ->get();
	    return $var = ($q->num_rows() > 0) ? false : true;
    }
    
    public function _check_ktp($noktp){
        $array = array('noktp'=>$noktp);
        $q = $this->db->select("noktp")   
            ->from('member')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? false : true;
    }

    /*
    |--------------------------------------------------------------------------
    | Signup member
    |--------------------------------------------------------------------------
    |
    | untuk signup/register member online
    |
    | @author taQwa qtakwa@yahoo.com@yahoo.com
    | @author 2009-04-27
    |
    */
	
	/*Modified by Boby : 2010-01-21*/
    //public function signup($name){
    public function stock_reserved($whsid,$soid,$item_id,$qty,$empid,$item_whsid){     
        
        $this->db->query("CALL sp_stock_summary_check($item_whsid)");
        $this->db->query("CALL sp_inventory_temp_member($item_whsid,$soid,'D','SO KIT','$item_id',$qty,'$empid')");
        
        return true;
    
    }

    public function insert_payment_confirmation($data_confirm){     

        $this->db->insert("payment_confirmation, $data_confirm)");
        
        return true;
    
    }

	/*End Modified by Boby : 2010-01-21*/
    protected function _getWarehouseID($memberid){
        $data = array();
        $q=$this->db->select("warehouse_id",false)
            ->from('users')
            ->where('id',$memberid)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        return $data;
    }       

    public function getWhsidByCityId($city_id){
        $q=$this->db->select("warehouse_id",false)
            ->from('kota')
            ->where('id',$city_id)
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('warehouse_id');
        }
        return $data;
    }       
        
    public function getKotaIdByIdFe($kota_id){
        $data = '';
       $q=$this->db->select("id",false)
            ->from('kota')
            ->where('id_fe',intval($kota_id))
            ->get();
        if($q->num_rows() > 0){
            $data = $q->row('id');
        }
        return $data;
    }       
        
    
    /*
    |--------------------------------------------------------------------------
    | login
    |--------------------------------------------------------------------------
    |
    | @param string $username Vaild username
    | @param string $password Password
    | @return mixed
    | @author taQwa
    | @author 2008-11-04
    |
    */
    public function login ($username, $password){
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_login($username);
        if ($result)
        {
            if ($password === $result->password)
            {
                $this->db->set('last_login', 'NOW()', false);
                $this->db->where('id',$result->id);
                $this->db->update('users',$data=array());
                $this->session->set_userdata(array('logged_in'=>'true','userid'=>$result->id,'last_login'=>$result->last_login,'name' => $result->nama,'username'=> $result->username,'group_id'=> $result->group_id));
                return 'SUCCESS';
            }
        }
        return false;
   }
   
   protected function _login ($username)
   {
        $i = $this->db->select("u.password,u.username,u.last_login,m.nama,u.id,u.group_id")	
            ->from('users u')
        ->join('member m', 'u.id = m.id', 'left')
        ->where('u.id', $username)
        ->limit(1)
        ->get();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
   }
   
    public function getParentID($placementid){
        $data = array();
        $i = $this->db->select("id")	
            ->from('networks')
            ->where('member_id', $placementid)
            ->get();
	if($i->num_rows() > 0){
            $data = $i->row_array();
        }
        $i->free_result();
	return $data;
    }
    
    /*
    |--------------------------------------------------------------------------
    | add Register Member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-24
    |
    */
    public function add_register(){
		//$portal = $this->input->post('zip');
		//$info = $this->input->post('infofrom');
		$info_ = 0;
		$portal = 0;
        $data = array(
            'tgl'=>date('Y-m-d',now()),
            'fullname'=>$this->input->post('name'),
            'hp'=>$this->input->post('hp'),
            'email'=>$this->input->post('email'),
            'address' => $this->db->escape_str($this->input->post('address')),
            'zip'=>$portal,
            'delivery'=>'0',
            'infofrom'=>$info_
        );
        $this->db->insert('register',$data);
    }
    public function searchRegister($keywords=0,$num,$offset){
        $data = array();
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.fullname,a.hp,a.email,delivery,a.infofrom",false);
        $this->db->from('register a');
        $this->db->where($where);
        $this->db->order_by('a.id','desc');
        $this->db->limit($num,$offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
            foreach($q->result_array() as $row){
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function countRegister($keywords=0){
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->from('register a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }
    
    public function getRegister($id){
        $data = array();
        $q = $this->db->get_where('register',array('id'=>$id));
        if($q->num_rows() > 0){
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;        
    }
    public function register_online_del(){
        if($this->input->post('p_id')){
            $row = array();
            
            $idlist = implode(",",array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";
            
            $row = $this->_countDelete($where);
            if($row){
                
                $this->db->delete('register',$where);
                
                $this->session->set_flashdata('message','Delete register online successfully');
            }else{
                $this->session->set_flashdata('message','Nothing to delete register online!');
            }
        }else{
            $this->session->set_flashdata('message','Nothing to delete register online!');
        }
    }
    private function _countDelete($option){
        $data=array();
        $this->db->select("id",false);
        $this->db->where($option);
        $q = $this->db->get('register');
        if($q->num_rows() > 0){
            foreach($q->result_array()as $row){
                $data[]=$row;
            }
        }
        $q->free_result();
        return $data;
    }
    
    public function updateNetwork($memberid,$parentid){
		$this->db->update('networks',array('parentid'=>$parentid),array('member_id'=>$memberid));
    }
    //'updated' => date('Y-m-d H:i:s',now()),
    //'updatedby' => $this->session->userdata('user')
	
	public function cek_stock_item_whsid($item_id, $qty, $whsid){
		
		$where_ = "item_id = '$item_id' AND ";
		//$where_ .= "qty >= $qty AND ";
		$where_ .= "warehouse_id = '$whsid'";
		$debe = "stock";
		$where_ .= "ORDER BY qty DESC LIMIT 0,1";
		$this->db->select("qty",false);
        $this->db->where($where_);
        $q = $this->db->get($debe);
        //echo $this->db->last_query();
        if($q->num_rows() > 0){
			$row1 = $q->row_array();
		}else{
			return false;
		}
		$q->free_result();
		
		if($row1["qty"]>0){return true;}else{
			return false;
        }
        //echo $this->db->last_query();
    }

	public function cekSKit($member_id,$actCode){
		/*
		"SELECT item_id FROM activation_code WHERE `status` = 'not used' AND member_id = '00001977' AND `code` = '225f524c86bb';"
		"SELECT IFNULL(qty,0)AS qty FROM titipan WHERE member_id = '00000009' AND item_id = '101';"
		*/
		
        $this->db->select("item_id, stockiest_id",false);
        $this->db->where("`status` = 'not used' AND member_id = '$member_id' AND `code` = '$actCode'");
        $q = $this->db->get('activation_code');
        if($q->num_rows() > 0){$row = $q->row_array();}
		$item_id = $row["item_id"];
		$stc = $row["stockiest_id"];
        $q->free_result();
		
		if($stc=='0'){
			$where_ = "item_id = '$item_id'";
			$debe = "stock";
		}else{
			$where_ = "member_id = '$stc' AND item_id = '$item_id'";
			$debe = "titipan";
		}
		$where_ .= "ORDER BY qty DESC LIMIT 0,1";
		$this->db->select("qty",false);
        $this->db->where($where_);
        $q = $this->db->get($debe);
        if($q->num_rows() > 0){
			$row1 = $q->row_array();
		}else{
			return true;
		}
		$q->free_result();
		
		if($row1["qty"]>0){return false;}else{
			return true;
			echo $this->db->last_query();
		}
    }
	/* end created by Boby 2011-01-13 */
	
	public function check_norekening($norek){
        $i = $this->db->select("id")	
            ->from('account')
            ->where('no', $norek)
            ->get();
            //echo $this->db->last_query();
			
		return $var = ($i->num_rows() > 0) ? true : false;
    }

    public function getListMember()
    {
        $qry = "SELECT m.id, m.nama
       -- ,CASE WHEN m.email = ''  OR m.email IS NULL THEN CONCAT(m.id,'@uni-health.com') ELSE m.email END AS email
        ,CONCAT(m.id,'@uni-health.com') AS email
        ,CASE WHEN m.jk = 'Perempuan' THEN '5432' ELSE '5431' END AS jk
        ,IFNULL(b.bank_id_fe,'5466') AS bankidfe -- 5466 default bca
        ,m.sponsor_id, m.enroller_id
        ,IFNULL(u.activation_code,'0') AS activation_code
        ,CASE WHEN m.tempatlahir = '' OR m.tempatlahir IS NULL THEN '-' ELSE m.tempatlahir END AS tempatlahir
        ,CASE WHEN m.tgllahir = '0000-00-00' THEN '1900-01-01' ELSE m.tgllahir END AS tgllahir
        ,CASE WHEN m.noktp = '' THEN '-' ELSE m.noktp END AS noktp
        ,CASE WHEN m.hp = '' THEN '-' ELSE m.hp END AS hp
        ,IFNULL(a.no,'-') AS norekening
        ,CASE WHEN a.area IS NULL OR a.area = '' THEN '-' ELSE a.area END AS area
        -- ,IFNULL(m.alamat,'-') AS alamat
        ,CASE WHEN m.alamat = ''  OR m.alamat IS NULL THEN '-' ELSE m.alamat END AS alamat
        ,CASE WHEN m.kecamatan = '' THEN '-' ELSE m.kecamatan END AS kecamatan
        ,CASE WHEN m.kelurahan = '' THEN '-' ELSE m.kelurahan END AS kelurahan 
        ,CASE WHEN m.kodepos = '' THEN '-' ELSE m.kodepos END AS kodepos
        ,CASE WHEN m.ahliwaris = '' THEN '-' ELSE m.ahliwaris END AS ahliwaris
        ,p.name AS propinsi
        ,k.id_fe AS kota_id_fe, k.name AS kota
        ,CASE WHEN u.banned_id > 0 THEN 0 ELSE 1 END AS banned
        FROM member m
        LEFT JOIN account a ON m.account_id = a.id
        LEFT JOIN bank b ON a.bank_id = b.id
        LEFT JOIN member mu ON m.sponsor_id = mu.id
        LEFT JOIN member ms ON m.enroller_id = ms.id
        LEFT JOIN users u ON u.id = m.id
        LEFT JOIN kota k ON k.id = m.kota_id
        LEFT JOIN propinsi p ON p.id = k.propinsi_id
        where m.member_id_fe = '' or m.member_id_fe is NULL";

        $q = $this->db->query($qry);

        if($q->num_rows() > 0){
            return $q->result_array();
        }else{
            return false;
        }
    }
	
}
