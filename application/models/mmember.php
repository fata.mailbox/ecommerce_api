<?php
class Mmember extends CI_Model
{
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
    function __construct()
    {
        parent::__construct();
    }

    public function signup(
        $activation_code,
        $member_id,
        $sponsor_id,
        $upline_id,
        $passsword,
        $hash,
        $full_name,
        $dob,
        $pob,
        $ktp,
        $hp,
        $address,
        $city,
        $gender
    ) {

        # Generate dynamic salt
        $hash = sha1(microtime());

        # Hash password
        $password = substr($ktp, 0, 8);
        //$password = substr('1234567890',0,8);
        $pin = $member_id;
        $password_enc = substr(sha1(sha1(md5($hash . $password))), 5, 15);
        $pin_enc = substr(sha1(sha1(md5($hash . $pin))), 3, 7);

        # Automatic Signup
        $row = $this->getWarehouseID($upline_id);
        $whsid = $row['warehouse_id'];
        $this->db->query("call sp_signup('$sponsor_id','$member_id','$activation_code','$upline_id','$full_name','$hp','-','$whsid','$password_enc','$pin_enc','$hash','0','system')");

        $this->db->query("UPDATE member SET noktp = '$ktp', alamat = '$address', ahliwaris = '-',jk = '$gender',tempatlahir = '$pob',tgllahir = '$dob',kota_id = '$city' WHERE id = '$member_id'");

        return 'REGISTRATION_SUCCESS';
    }

    function getWarehouseID($memberid)
    {
        $data = array();
        $q = $this->db->select("warehouse_id", false)
            ->from('users')
            ->where('id', $memberid)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        return $data;
    }

    public function check_ktp($noktp)
    {
        $array = array('noktp' => $noktp);
        $q = $this->db->select("noktp")
            ->from('member')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? false : true;
    }

    public function check_userid($p, $p2)
    {
        $array = array('member_id' => $p, 'code' => $p2, 'status' => 'not used');
        $q = $this->db->select("account")
            ->from('activation_code')
            ->where($array)
            ->get();

        return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function getKotaIdByIdFe($kota_id)
    {
        //$data = array();
        $data = null;
        $q = $this->db->select("id", false)
            ->from('kota')
            ->where('id_fe', $kota_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('id');
        }
        return $data;
    }

    public function getBankIdByIdFe($bank_id)
    {
        //$data = array();
        $data = null;
        $q = $this->db->select("id", false)
            ->from('bank')
            ->where('bank_id_fe', $bank_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('id');
        }
        return $data;
    }

    public function updateProfile($data_member)
    {
        $data = array(
            //'email'              =>$data_member['email'],
            'alamat'            => $data_member['address'],
            'kecamatan'         => $data_member['kecamatan'],
            'kelurahan'         => $data_member['kelurahan'],
            'kota_id'           => $data_member['city'],
            'kodepos'           => $data_member['kodepos'],
            'noktp'             => $data_member['ktp'],
            'tempatlahir'       => $data_member['pob'],
            'tgllahir'          => $data_member['dob'],
            //'jk'                =>$data_member['gender'],
            'hp'                => $data_member['hp'],
            'ahliwaris'         => $data_member['ahliwaris'],
            'updated'           => date('Y-m-d', now()),
            'updatedby'         => $data_member['member_id']
        );

        $this->db->update('member', $data, array('id' => $data_member['member_id']));
        return $data;
    }

    public function updateBank($data_member)
    {
        $data = array(
            'bank_id' => $data_member['bankid'],
            'member_id' => $data_member['member_id'],
            'name' => $data_member['nama_nasabah'],
            'no' => $data_member['no_rekening'],
            'area' => $data_member['bankarea'],
            'flag' => 0,
            'createdby' => $data_member['member_id'],
            'created' => date('Y-m-d H:i:s', now())
        );
        $this->db->insert('account', $data);
        $id = $this->db->insert_id();
        $this->db->update('member', array('account_id' => $id), array('id' =>  $data_member['member_id']));


        return $data;
    }
    public function updateNpwp($data_member)
    {
        $data = array(
            'npwp'          => $data_member['npwp_no'],
            'npwp_date'     => $data_member['npwp_date'],
            'member_id'     => $data_member['member_id'],
            'nama'          => $data_member['npwp_name'],
            'alamat'        => $data_member['npwp_address'],
            'office'        => "-",
            'menikah'       => "0",
            'anak'          => "0",
            'created_date'  => date('Y-m-d H:i:s', now()),
            'created_by'    => $data_member['member_id']
        );
        $this->db->insert('npwp', $data);

        return $data;
    }

    public function getListMemberNonIdFe()
    {
        $qry = "SELECT a.*, b.banned_id
        FROM member a LEFT JOIN a.id=b.id
        Where b.banned_id > 0
        AND a.member_id_fe ISNULL";

        $q = $this->db->query($qry);

        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return false;
        }
    }
}
