<?php
class Auth_model extends CI_Model
{
    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */
    function __construct()
    {
        parent::__construct();
    }
        
     function validationEmail($email_send){
         $query = $this->db->query("
            SELECT email
            from user_login where email= '$email_send'
            ");
        return $query->result();
    }

     function insert($data_param)
    {
        $this->db->insert('user_login', $data_param);
        return $this->db->affected_rows() > 0;
    }

    function login($email, $password){
    $query = $this->db->query("
        select l.*, g.name_users_group, b.name_building, b.address_building, p.name_position, mf.name_floor from user_login l LEFT JOIN users_group g ON l.id_users_group = g.id_users_group 
        LEFT JOIN mst_building b ON l.id_building = b.id_building LEFT JOIN mst_position p ON l.id_position = p.id_position LEFT JOIN mst_floor mf
        ON l.id_floor = mf.id_floor
        WHERE l.email = '".$email."' AND l.password = '".$password."' AND l.status = 1
        ");
    return $query->row();
    }
    function UserId($user_id){
    $query = $this->db->query("
        select l.*, g.name_users_group, b.name_building, b.address_building, p.name_position, mf.name_floor from user_login l LEFT JOIN users_group g ON l.id_users_group = g.id_users_group 
        LEFT JOIN mst_building b ON l.id_building = b.id_building LEFT JOIN mst_position p ON l.id_position = p.id_position LEFT JOIN mst_floor mf
        ON l.id_floor = mf.id_floor
        WHERE l.user_id = '".$user_id."'
        ");
    return $query->row();
    }

    function UpdateRegisterId($user_id, $register_id)
    {
        $this->db->set('registration_gcm', $register_id); //value that used to update column  
        $this->db->where('user_id', $user_id); //which row want to upgrade  
        $this->db->update('user_login');  //table name
        return $this->db->affected_rows() > 0;
    }

    function list_all_usergroup()
    {
        $this->db->select('*');
        $this->db->where('deleted', '0');
        $query = $this->db->get('users_group');
        return $query->result();
    }

    function DropRegisterGcm($user_id){
        $this->db->set('registration_gcm', 'gcmdefault'); //value that used to update column  
        $this->db->where('user_id', $user_id); //which row want to upgrade  
        $this->db->update('user_login');  //table name
        return $this->db->affected_rows() > 0;
    }

     function UpdateTypeVersion($user_id, $id_type_version){
        $this->db->set('type_version', $id_type_version); //value that used to update column  
        $this->db->where('user_id', $user_id); //which row want to upgrade  
        $this->db->update('user_login');  //table name
        return $this->db->affected_rows() > 0;
    }
    function CheckGcm($user_id){
        //$this->db->select('registration_gcm');
        //$this->db->select('user_id');
        //$this->db->where('user_id', $user_id);
        //$query = $this->db->get('user_login');
        $query = $this->db->query("select * from user_login where user_id = '$user_id'");
        return $query->row();
    }
    function GetVersion($type_version){
        $query = $this->db->query("select * from type_version where version_code = '$type_version'");
        return $query->row();
    }


}

?>