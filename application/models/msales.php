<?php
class MSales extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('date', 'my'));
    }

    /*
    |--------------------------------------------------------------------------
    | signup member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @created 2009-04-25
    |
    */
    // Start ASP 20200709 Cancel So Pending

    public function delete_so_temp_by_so_id_fe($so_id_fe)
    {
        $this->db->query("DELETE FROM temp_so where so_id_fe = " . $so_id_fe);

        return true;
    }

    public function delTempNcmD($so_id)
    {
        $q = $this->db->select("c.id, c.warehouse_id, c.remark", false)
            ->from('temp_ref_trx_nc a')
            ->join('temp_ncm c', 'a.nc_id = c.id', 'left')
            ->where('a.trx_id', $so_id)
            ->get();

        if ($q->num_rows() > 0) {

            foreach ($q->result() as $row) {

                $ncm_id = $row->id;

                $this->db->query("DELETE FROM temp_ncm_d where ncm_id = " . $ncm_id);
            }
        } else {

            return false;
        }
        $q->free_result();
        return true;
    }
    public function delTempNcm($so_id)
    {
        $q = $this->db->select("c.id, c.warehouse_id, c.remark", false)
            ->from('temp_ref_trx_nc a')
            ->join('temp_ncm c', 'a.nc_id = c.id', 'left')
            ->where('a.trx_id', $so_id)
            ->get();

        if ($q->num_rows() > 0) {

            foreach ($q->result() as $row) {

                $ncm_id = $row->id;

                $this->db->query("DELETE FROM temp_ncm where id = " . $ncm_id);
            }
        } else {

            return false;
        }
        $q->free_result();
        return true;
    }
    public function delTempreftrxnc($so_id)
    {
        $this->db->query("DELETE FROM temp_ref_trx_nc where trx_id = " . $so_id);

        return true;
    }
    public function delTempSod($so_id)
    {
        $this->db->query("DELETE FROM temp_so_d where so_id = " . $so_id);

        return true;
    }
    public function delTempSo($so_id)
    {
        $this->db->query("DELETE FROM temp_so where id = " . $so_id);

        return true;
    }

    public function cancelTempSo($so_id)
    {
        //$data = false;
        $q = $this->db->select("a.so_id, a.id as so_d_id, a.item_id,a.qty, b.warehouse_id, b.remark", false)
            ->from('temp_so_d a')
            ->join('temp_so b', 'a.so_id = b.id', 'left')
            ->where('a.so_id', $so_id)
            ->get();

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                //$data[] = $row;

                $so_id_       = $row['so_id'];
                $so_d_id_       = $row['so_d_id'];
                $item_id_        = $row['item_id'];
                $qty_            = $row['qty'];
                $warehouse_id_   = $row['warehouse_id'];
                $remark_         = $row['remark'];

                $data = array('stockItems' => array(array('sku' => $item_id_, 'qty_plus' => intval($qty_), 'qty_minus' => 0)));

                $post = array("data" => $data, "key" => "Cancel Temp SO");

                $this->update_stock_api($post);

                $this->db->query("CALL sp_inventory_temp_member(" . $warehouse_id_ . ", " . $so_id_ . ",'K','cancel temp SO " . $so_id_ . "','" . $item_id_ . "'," . $qty_ . ",'system')");
            }
            $this->db->query("UPDATE temp_so a set a.remark = 'CANCEL - " . $remark_ . "' where id = " . $so_id);
        } else {

            return false;
        }
        $q->free_result();
        return true;
    }

    public function cancelTempMember($member_id)
    {
        $this->db->query("DELETE FROM temp_member_fe where temp_member_id = " . $member_id);
        $this->db->query("DELETE FROM activation_code where member_id = " . $member_id);

        return true;
    }

    public function cancelTempNcmD($so_id)
    {
        //$data = false;
        $q = $this->db->select("b.ncm_id, b.id as ncm_d_id, b.item_id,b.qty, c.warehouse_id, c.remark", false)
            ->from('temp_ref_trx_nc a')
            ->join('temp_ncm c', 'a.nc_id = c.id', 'left')
            ->join('temp_ncm_d b', 'b.ncm_id = c.id', 'left')
            ->where('a.trx_id', $so_id)
            ->group_by('ncm_id')
            ->get();

        // echo $this->db->last_query();
        // var_dump($q); die();

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                //$data[] = $row;

                $ncm_id       = $row['ncm_id'];
                $ncm_d_id       = $row['ncm_d_id'];
                $item_id        = $row['item_id'];
                $qty            = $row['qty'];
                $warehouse_id   = $row['warehouse_id'];
                $remark         = $row['remark'];
                //var_dump($row);die;

                $data = array('stockItems' => array(array('sku' => $item_id, 'qty_plus' => intval($qty), 'qty_minus' => 0)));

                $post = array("data" => $data, "key" => "Cancel Temp NCM");

                $this->update_stock_api($post);

                $this->db->query("CALL sp_inventory_temp_member(" . $warehouse_id . ", " . $ncm_id . ",'K','cancel free temp SO " . $so_id . "','" . $item_id . "'," . $qty . ",'system')");
            }
            $this->db->query("UPDATE temp_ncm a set a.remark = 'CANCEL - " . $remark . "' where id = " . $so_id);
        } else {

            return false;
        }
        $q->free_result();
        return true;
    }
    function update_stock_api($data)
    {

        $data_post = json_encode($data['data']);

        $key = $data['key'];

        $token = get_token();

        $url = get_url_api() . 'rest/V1/updateBatchStock';
        $ch = curl_init($url);

        //$post = json_encode($post);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log($key, 'outbound', $data_post, $url, $result);

        return $result;
    }
    public function getTempSoid($so_id_fe)
    {
        //$data = array();
        $data = null;
        $q = $this->db->select("id", false)
            ->from('temp_so')
            ->where('so_id_fe', $so_id_fe)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('id');
        }
        return $data;
    }

    public function getTempSoByMemberId($member_id)
    {
        //$data = array();
        $data = null;
        $q = $this->db->select("id", false)
            ->from('temp_so')
            ->where('member_id', $member_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->result();
        }

        return $data;
    }

    function getSoIdFeFromIdBe($so_id_be)
    {

        $array = array('id' => $so_id_be);
        $q = $this->db->select("so_id_fe")
            ->from('so')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row('so_id_fe') : false;
    }

    function getSoIdFeFromIdBeTemp($so_id_be_temp)
    {

        $array = array('id' => $so_id_be_temp);
        $q = $this->db->select("so_id_fe")
            ->from('temp_so')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row('so_id_fe') : false;
    }

    function get_so_from_so_id_be($so_id_be)
    {

        $array = array('id' => $so_id_be);
        $q = $this->db->select("*")
            ->from('so')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    function get_member_detail_by_member_id($member_id)
    {

        $array = array('id' => $member_id);
        $q = $this->db->select("*")
            ->from('member')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    function get_memberdelv_by_delivery_addr($delivery_addr)
    {

        $array = array('id' => $delivery_addr);
        $q = $this->db->select("*")
            ->from('member_delivery')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    function get_memberdelv_by_delivery_by_member_id($member_id)
    {

        $array = array('member_id' => $member_id);
        $q = $this->db->select("*")
            ->from('member_delivery')
            ->where($array)
            ->order_by('id', 'DESC')
            ->limit(1)
            ->get();

        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    function get_kota_from_id_be($kota_id_be)
    {

        $array = array('id' => $kota_id_be);
        $q = $this->db->select("*")
            ->from('kota')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    function get_so_detail_by_so_id($so_id_be)
    {

        $array = array('so_id' => $so_id_be);
        $q = $this->db->select("*")
            ->from('so_d')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->result() : false;
    }

    function get_product_id_fe_by_id($item_id)
    {

        $array = array('id' => $item_id);
        $q = $this->db->select("product_id_fe")
            ->from('item')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row('product_id_fe') : false;
    }

    public function getTempMemberId($member_id_fe)
    {
        //$data = array();
        $data = null;
        $q = $this->db->select("temp_member_id", false)
            ->from('temp_member_fe')
            ->where('member_id_fe', $member_id_fe)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('temp_member_id');
        }
        return $data;
    }
    public function getTempreftrxncid($so_id)
    {
        //$data = array();
        $data = 0;
        $q = $this->db->select("count(nc_id) as jml", false)
            ->from('temp_ref_trx_nc')
            ->where('trx_id', $so_id)
            ->where('trx', 'SO')
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('jml');
        }
        return $data;
    }
    // EOF ASP 20200709 Cancel So Pending
    public function add_temp_member($data_member)
    {

        $cek_member_id = $this->check_memberid($data_member['temp_member_id']);
        if ($cek_member_id == false) {
            return false;
        }

        $data = array(
            'temp_member_id'    => $data_member['temp_member_id'],
            'sponsor_id'        => $data_member['sponsor_id'],
            'stockiest_id'      => 0,
            'enroller_id'       => $data_member['upline_id'],
            'password'          => $data_member['password'],
            'hash'              => $data_member['hash'],
            'nama'              => $data_member['full_name'],
            'jk'                => $data_member['gender'],
            'tgllahir'          => $data_member['dob'],
            'tempatlahir'       => $data_member['pob'],
            'noktp'             => $data_member['ktp'],
            'hp'                => $data_member['hp'],
            'alamat'            => $data_member['address'],
            'kota_id'           => $data_member['city'],
            'activation_code'   => $data_member['activation_code'],
            'kit'               => $data_member['item_id'],
            'created'           => date('Y-m-d', now()),
            'createdby'         => 'system'
        );
        $this->db->insert('temp_member_fe', $data);

        return $data;
    }

    public function insert_payment_confirm($data_payment)
    {

        $this->db->insert('payment_confirmation', $data_payment);

        $last_id = $this->db->insert_id();

        return true;
    }

    public function add_temp_so($data_so_header)
    {
        $data = array(
            'so_id_fe'                => $data_so_header['so_id_fe'],
            'tgl'        => $data_so_header['so_date'],
            'stockiest_id'      => $data_so_header['stockiest_id'],
            'member_id'          => $data_so_header['member_id'],
            'totalharga_'        => $data_so_header['total_harga_'],
            'totalharga'         => $data_so_header['total_harga'],
            'total_pv_'              => $data_so_header['total_pv_'],
            'total_bv_'                => $data_so_header['total_bv_'],
            'total_discount_rp'          => $data_so_header['total_discount_rp'],
            'total_discount_rp_voucher'       => $data_so_header['total_discount_rp_voucher'],
            'total_discount_pv_voucher'             => $data_so_header['total_discount_pv_voucher'],
            'total_discount_bv_voucher'                => $data_so_header['total_discount_bv_voucher'],
            'totalpv'           => $data_so_header['total_pv'],
            'totalbv'          => $data_so_header['total_bv'],
            'vouchercode'               => $data_so_header['vouchercode'],
            'kit'               => $data_so_header['kit'],
            'remark'               => $data_so_header['remark'],
            'delivery_addr_id'     => $data_so_header['delivery_addr_id'],
            'delivery_city_id'               => $data_so_header['delivery_city_id'],
            'shipping_price'               => $data_so_header['shipping_price'],
            'created'               => $data_so_header['created'],
            'createdby'               => 'system',
            'status1'               => $data_so_header['status1'],
            'status2'               => $data_so_header['status2'],
            'status'               => $data_so_header['status'],
            'warehouse_id'               => $data_so_header['warehouse_id'],
            'payment_type'               => $data_so_header['payment_type']
        );
        $this->db->insert('temp_so', $data);

        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function add_temp_delivery_addr($data_delivery)
    {

        $insert_temp_delv = $this->db->insert('temp_member_delivery', $data_delivery);

        return true;
    }

    public function add_temp_so_d($data_so_detail)
    {
        $data = array(
            'so_d_id_fe'                => $data_so_detail['so_d_id_fe'],
            'so_id'        => $data_so_detail['so_id'],
            'so_id_fe'        => $data_so_detail['so_id_fe'],
            'item_id'      => $data_so_detail['item_id'],
            'qty'          => $data_so_detail['qty'],
            'harga'        => $data_so_detail['harga_'],
            'harga_'         => $data_so_detail['harga_'],
            'pv_'              => $data_so_detail['pv_'],
            'bv_'                => $data_so_detail['bv_'],
            'discount_rp'          => $data_so_detail['discount_rp'],
            'discount_rp_voucher'       => $data_so_detail['discount_rp_voucher'],
            'discount_pv_voucher'             => $data_so_detail['discount_pv_voucher'],
            'discount_bv_voucher'                => $data_so_detail['discount_bv_voucher'],
            'harga'           => $data_so_detail['harga'],
            'pv'          => $data_so_detail['pv'],
            'bv'               => $data_so_detail['bv'],
            'jmlharga_'               => $data_so_detail['jmlharga_'],
            'jmlpv_'               => $data_so_detail['jmlpv_'],
            'jmlbv_'               => $data_so_detail['jmlbv_'],
            'jmldiscount_rp'               => $data_so_detail['jmldiscount_rp'],
            'jmldiscount_rp_voucher'               => $data_so_detail['jmldiscount_rp_voucher'],
            'jmldiscount_pv_voucher'               => $data_so_detail['jmldiscount_pv_voucher'],
            'jmlharga'               => $data_so_detail['jmlharga'],
            'jmlpv'               => $data_so_detail['jmlpv'],
            'jmlpv'               => $data_so_detail['jmlpv'],
            'warehouse_id'               => $data_so_detail['warehouse_id'],
        );
        $this->db->insert('temp_so_d', $data);

        //Push To API

        /*
        $url =  "http://" . $_SERVER['HTTP_HOST'] . "/ecommerce_api/outbound/outbound_api/update_product_stock";
        $ch = curl_init($url);

        $data_post = array(
            "sku" => $data_so_detail['item_id'],
            "qty_plus" => "0",
            "qty_minus" => $data_so_detail['qty']
        );

        $data = array("stockItems" => array($data_post));
        $post = json_encode(array("data" => $data, "key" => "SO"));

        $authorization =  'Authorization: Basic ' . base64_encode("s0h0uhn:p7vazfzx");

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
        */

        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function add_member_delivery($data_delivery)
    {

        $this->db->insert('member_delivery', $data_delivery);

        $last_id = $this->db->insert_id();

        return $last_id;
    }

    public function check_introducerid($introducerid)
    {
        $i = $this->db->select("id")
            ->from('member')
            ->where('id', $introducerid)
            ->get();
        //echo $this->db->last_query();

        return $var = ($i->num_rows() > 0) ? false : true;
    }

    public function cek_id_fe_exsist($so_id_fe)
    {
        $i = $this->db->select("*")
            ->from('temp_so')
            ->where('so_id_fe', $so_id_fe)
            ->get();
        //echo $this->db->last_query();

        return $var = ($i->num_rows() > 0) ? true : false;
    }

    public function check_captcha($confirmCaptcha)
    {
        $captchaWord = $this->session->userdata('captchaWord');

        $this->session->unset_userdata('captchaWord');
        if (strcasecmp($captchaWord, $confirmCaptcha) == 0) {
            return true;
        }
        return false;
    }
    public function check_placementid($placementid)
    {
        $i = $this->db->select("id")
            ->from('member')
            ->where('id', $placementid)
            ->get();

        return $var = ($i->num_rows() > 0) ? false : true;
    }

    public function sponsor_validate($sponsor_id, $upline_id)
    {

        $q = $this->db->select("a.*", false)
            ->from('member a')
            ->join('users b', 'a.id=b.id', 'left')
            ->where('a.id', $sponsor_id)
            ->where('b.banned_id', 0)
            ->get();

        if ($q->num_rows() > 0) {

            $r = $this->db->select("a.*", false)
                ->from('member a')
                ->join('users b', 'a.id=b.id', 'left')
                ->where('a.id', $upline_id)
                ->where('b.banned_id', 0)
                ->get();

            return $var = ($r->num_rows() > 0) ? 'sukses' : 'upline_id_gagal';
        } else {

            return 'sponsor_id_gagal';
        }

        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function cek_kit_yes_no($so_id_fe, $so_id_be)
    {

        $q = $this->db->select("*", false)
            ->from('temp_so')
            ->where('so_id_fe', $so_id_fe)
            ->where('id', $so_id_be)
            ->where('kit', 'Y')
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {

            return true;
        } else {

            return false;
        }

        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function password_validate($member_id, $password)
    {

        $q = $this->db->select("*", false)
            ->from('users')
            ->where('id', $member_id)
            ->where('password', $password)
            ->where('banned_id', 0)
            ->get();

        if ($q->num_rows() > 0) {

            return $var = ($q->num_rows() > 0) ? true : false;
        } else {

            return false;
        }

        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function member_activation($member_id, $so_id_fe, $so_id_be)
    {

        $q = $this->db->select("a.*, b.*", false)
            ->from('temp_member_fe a')
            ->join('temp_so b', 'a.temp_member_id=b.member_id', 'left')
            ->where('a.temp_member_id', $member_id)
            ->where('b.so_id_fe', $so_id_fe)
            ->where('b.id', $so_id_be)
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {

            $member = $q->row_array();

            $pin_enc = substr(sha1(sha1(md5($member['hash'] . $member_id))), 3, 7);
            $hash = $member['hash'];
            $sponsor_id     = $member['sponsor_id'];
            $upline_id      = $member['enroller_id'];
            $password      = $member['password'];
            $warehouse_id   = $member['warehouse_id'];
            $activation_code = $member['activation_code'];
            $email      = $member['email'];
            $alamat      = $member['alamat'];
            $noktp      = $member['noktp'];
            $kota_id      = $member['kota_id'];
            $tempatlahir      = $member['tempatlahir'];
            $tgllahir      = $member['tgllahir'];
            $jk      = $member['jk'];
            $hp      = $member['hp'];
            $nama      = $member['nama'];
            $createdby      = $member['createdby'];
            $email      = $member['email'];
            $totalharga      = $member['totalharga'];
            $item_kit      = $member['kit'];

            $this->db->query("INSERT INTO users(id,username,PASSWORD,pin,HASH,group_id,warehouse_id,activation_code)
            VALUES('$member_id', '$member_id', '$member[password]', '$pin_enc', '$member[hash]', 101, '$member[warehouse_id]', '$member[activation_code]')");

            $this->db->query(" UPDATE activation_code SET STATUS='used',useddate=NOW() WHERE member_id='$member_id' AND CODE= '$member[activation_code]'");

            $q_mem = $this->db->select("*", false)
                ->from('temp_member_fe')
                ->where('temp_member_id', $member_id)
                ->get();

            if ($q_mem->num_rows() > 0) {

                $data_member = $q_mem->row_array();
                $data_member['id'] = $data_member['temp_member_id'];
                unset($data_member['temp_member_id']);
                unset($data_member['password']);
                unset($data_member['hash']);
                unset($data_member['activation_code']);
            }

            $insert_member = $this->db->insert('member', $data_member);

            $this->db->query("insert into member_kelengkapan (id, form, rek, ktp) values ('$member_id',0,0,0)");

            //$this->db->query("DELETE from temp_member_fe WHERE temp_member_id='$member_id'");

            //$this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe')");

            $member_id_fe = $this->getMemberIdFe($member_id);
            $member_name = $this->getMemberName($member_id);
            $member_email = $this->getMemberEmail($member_id);

            $detail_member = $this->get_member_by_id($member_id);

            $firstname = implode(" ", array_slice(explode(" ", $member_name), 0, 1));
            $lastname = implode(" ", array_slice(explode(" ", $member_name), 1));

            $customer_array = array(
                "id" => "$member_id_fe",
                "email" => "$member_email",
                "firstname" => "$firstname",
                "lastname" => "$lastname",
                "website_id" => "1"
            );

            $ca_status_aktif_member = array("attribute_code" => "ca_status_aktif_member", "value" => "1");
            $ca_nomor_ktp = array("attribute_code" => "ca_nomor_ktp", "value" => "$detail_member->noktp");
            $ca_tempat_lahir = array("attribute_code" => "ca_tempat_lahir", "value" => "$detail_member->tempatlahir");
            $ca_nomor_handphone = array("attribute_code" => "ca_nomor_handphone", "value" => "$detail_member->hp");

            $customer_array['custom_attributes'] = array($ca_status_aktif_member, $ca_nomor_ktp, $ca_tempat_lahir, $ca_nomor_handphone);

            $customer['customer'] = $customer_array;

            $post = json_encode($customer);

            $order_array = array(
                "entity_id" => "$so_id_fe",
                "state" => "processing",
                "status" => "processing"
            );

            $data_order['entity'] = $order_array;

            $post_order = json_encode($data_order);

            // print_r($post);
            // print_r($post_order);
            //Update status Aktif member ke FE
            $activated_member = $this->update_active_member($member_id_fe, $post);

            //Update status Order ke FE
            //$update_order = $this->update_status_order($post_order);

            return $member;
        } else {

            return false;
        }

        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    function get_member_by_id($member_id)
    {

        $array = array('id' => $member_id);
        $q = $this->db->select("*")
            ->from('member')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    function get_so_id_exec_by_so_id($so_id_cancel)
    {

        $array = array('so_id_canceled' => $so_id_cancel);
        $q = $this->db->select("so_id_execute")
            ->from('canceled_so')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row('so_id_execute') : false;
    }

    function insert_update_so_id_fe($so_id, $so_id_fe)
    {

        $this->db->update('so', array('so_id_fe' => $so_id_fe), array('id' => $so_id));
    }

    function update_member_delivery($delv_addr_id_be, $data_delivery)
    {

        $this->db->update('member_delivery', $data_delivery, array('id' => $delv_addr_id_be));
    }

    function payment_approved($id_list, $remark)
    {

        $row = array();
        $where = "id in ($id_list)";
        $option = $where . " and status = 'pending'";

        $row = $this->_countPaymentConfirmApproved($option);

        if ($row) {
            $data = array(
                'status' => 'approved',
                'remark' => $remark,
                'approvedby' => 'system',
                'tglapproved' => date('Y-m-d H:i:s', now())
            );

            $this->db->update('payment_confirmation', $data, $option);
        }

        return true;
    }

    function payment_rejected($id_list, $remark)
    {

        $row = array();
        $where = "id in ($id_list)";
        $option = $where . " and status = 'pending'";

        $row = $this->_countPaymentConfirmApproved($option);

        if ($row) {
            $data = array(
                'status' => 'rejected',
                'remark' => $remark,
                'approvedby' => 'system',
                'tglapproved' => date('Y-m-d H:i:s', now())
            );

            $this->db->update('payment_confirmation', $data, $option);
        }

        return true;
    }

    function get_payment_confirm_by_id_list($id_list)
    {

        $where = "id in ($id_list)";
        $option = $where;

        $data = array();
        $this->db->select("*", false);
        $this->db->where($option);
        $q = $this->db->get('payment_confirmation');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    protected function _countPaymentConfirmApproved($option)
    {
        $data = array();
        $this->db->select("id", false);
        $this->db->where($option);
        $q = $this->db->get('payment_confirmation');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function update_active_member($member_id_fe, $post)
    {

        $url = get_url_api() . 'rest/all/V1/customers/' . $member_id_fe;
        $ch = curl_init($url);

        //Update status aKtif member ke FE

        $token = get_token();

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Aktivasi Member', 'outbound', $post, $url, $result);

        return $result;
    }

    public function update_status_order_ewallet($post_order)
    {

        $url = get_url_api() . 'rest/all/V1/orders';
        $ch = curl_init($url);

        $token = get_token();

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        activity_log('Update Status Order', 'outbound', $post_order, $url, $result);

        curl_close($ch);

        return $result;
    }

    public function update_status_order_old($so_id_fe)
    {

        //Update Processing

        $order_array = array(
            "entity_id" => "$so_id_fe",
            "state" => "processing",
            "status" => "processing"
        );

        $data_order['entity'] = $order_array;

        $post_order = json_encode($data_order);

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/orders';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Processing', 'outbound', $post_order, $url, $result);

        //Update Shippping
        $url = get_url_api() . 'rest/default/V1/order/' . $so_id_fe . '/ship';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Shipping', 'outbound', null, $url, $result);

        return $result;
    }

    public function update_status_order_shipping($so_id_fe)
    {

        //Update Shippping
        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/order/' . $so_id_fe . '/ship';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Shipping', 'outbound', null, $url, $result);

        return $result;
    }

    public function update_status_order_cancel($so_id_fe)
    {

        //Update Canceled
        $token = get_token();

        $url = get_url_api() . 'rest/V1/orders/' . $so_id_fe . '/cancel';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Canceled', 'outbound', null, $url, $result);

        return $result;
    }

    public function update_status_order_cancel_cc_ewallet($so_id_fe)
    {

        $order_array = array(
            "entity_id" => "$so_id_fe",
            "state" => "canceled",
            "status" => "canceled"
        );

        $data_order['entity'] = $order_array;

        $post_order = json_encode($data_order);

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/orders';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Canceled', 'outbound', $post_order, $url, $result);

        return $result;
    }

    public function refund_payment_to_ewallet($member_id, $so_id_fe, $total_harga)
    {

        //Update Ewallet FE
        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($total_harga)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'credit',
            "walletnote"        => 'Refund Payment Order id :' . $so_id_fe
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Refund Payment to Ewallet', 'outbound', $post, $url, $result);

        return $result;
    }

    public function send_message_order_cancel($so_id_fe)
    {

        $message_array = array(
            "comment" => "Pesanan Anda Telah Di Batalkan karena Out of Stock di daerah pengiriman Anda.",
            "parent_id" => $so_id_fe,
            "is_customer_notified" => 1,
            "is_visible_on_front" => 1,
            "status" => "canceled"
        );

        $data_order['statusHistory'] = $message_array;

        $post_order = json_encode($data_order);


        //Update Canceled
        $token = get_token();

        $url = get_url_api() . 'rest/V1/orders/' . $so_id_fe . '/comments';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Send Mesage Order Canceled', 'outbound', $post_order, $url, $result);

        return $result;
    }

    public function send_message_order_cancel_item_notfound($so_id_fe)
    {

        $message_array = array(
            "comment" => "Pesanan Anda Telah Di Batalkan karena ID Item tidak ditemukan.",
            "parent_id" => $so_id_fe,
            "is_customer_notified" => 1,
            "is_visible_on_front" => 1,
            "status" => "canceled"
        );

        $data_order['statusHistory'] = $message_array;

        $post_order = json_encode($data_order);


        //Update Canceled
        $token = get_token();

        $url = get_url_api() . 'rest/V1/orders/' . $so_id_fe . '/comments';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Send Mesage Order Canceled', 'outbound', $post_order, $url, $result);

        return $result;
    }

    public function send_message_order_cancel_price($so_id_fe)
    {

        $message_array = array(
            "comment" => "Pesanan Anda Telah Di Batalkan karena harga barang tidak cocok, silahkan melakukan odrer ulang.",
            "parent_id" => $so_id_fe,
            "is_customer_notified" => 1,
            "is_visible_on_front" => 1,
            "status" => "canceled"
        );

        $data_order['statusHistory'] = $message_array;

        $post_order = json_encode($data_order);


        //Update Canceled
        $token = get_token();

        $url = get_url_api() . 'rest/V1/orders/' . $so_id_fe . '/comments';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Send Mesage Order Canceled', 'outbound', $post_order, $url, $result);

        return $result;
    }

    public function get_detail_product_fe($sku)
    {

        //Get Detail Product From FE
        $token = get_token();

        $url = get_url_api() . '/rest/all/V1/products/' . $sku;
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        return json_decode($result, TRUE);
    }

    public function get_item_stock_fe($sku)
    {

        //Get Detail Product From FE
        $token = get_token();

        $url = get_url_api() . '/rest/all/V1/products/' . $sku;
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        if ($result) {

            $result_product = json_decode($result, TRUE);
            $item_stock = $result_product['extension_attributes']['stock_item']['qty'];
            $result = $item_stock;
        }

        activity_log('Get Item Stock', 'outbound', null, $url, $result);

        return $result;
    }

    public function update_status_order_completed($so_id_fe)
    {

        //Update Completed

        $order_array = array(
            "entity_id" => "$so_id_fe",
            "state" => "completed",
            "status" => "completed"
        );

        $data_order['entity'] = $order_array;

        $post_order = json_encode($data_order);

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/orders';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Completed', 'outbound', $post_order, $url, $result);

        return $result;
    }

    public function update_status_order($so_id_fe)
    {

        //Update Processing
        $token = get_token();

        $order_array = array(
            "entity_id" => "$so_id_fe",
            "state" => "processing",
            "status" => "processing"
        );

        $data_order['entity'] = $order_array;

        $post_order = json_encode($data_order);

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/orders';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Processing', 'outbound', $post_order, $url, $result);

        //Update Shippping
        $url = get_url_api() . 'rest/default/V1/order/' . $so_id_fe . '/ship';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Shipping', 'outbound', null, $url, $result);

        return $result;
    }

    public function update_status_order_transfer($so_id_fe)
    {

        //Update Processing

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/order/' . $so_id_fe . '/invoice';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Invoice', 'outbound', null, $url, $result);

        $order_array = array(
            "entity_id" => "$so_id_fe",
            "state" => "processing",
            "status" => "processing"
        );

        $data_order['entity'] = $order_array;

        $post_order = json_encode($data_order);

        $token = get_token();

        $url = get_url_api() . 'rest/default/V1/orders';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Processing', 'outbound', $post_order, $url, $result);

        //Update Shippping
        $url = get_url_api() . 'rest/default/V1/order/' . $so_id_fe . '/ship';
        $ch = curl_init($url);

        $authorization = "Authorization: Bearer " . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $post_order); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);

        curl_close($ch);

        activity_log('Update Status Shipping', 'outbound', null, $url, $result);

        return $result;
    }

    public function getMemberIdFe($member_id)
    {
        $q = $this->db->select("member_id_fe", false)
            ->from('member')
            ->where('id', $member_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('member_id_fe');
        }
        return $data;
    }

    public function getMemberName($member_id)
    {
        $data = array();
        $q = $this->db->select("nama", false)
            ->from('member')
            ->where('id', $member_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('nama');
        }
        return $data;
    }

    public function getMemberEmail($member_id)
    {
        $data = array();
        $q = $this->db->select("email", false)
            ->from('member')
            ->where('id', $member_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('email');
        }
        return $data;
    }

    public function get_delv_addr_be($deliv_addr_fe)
    {
        $q = $this->db->select("id", false)
            ->from('member_delivery')
            ->where('id_fe', $deliv_addr_fe)
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {
            return $data = $q->row('id');
        } else {
            return false;
        }
    }

    public function payment_confirmation_order($member_id, $so_id_fe, $so_id_temp)
    {
        // var_dump($member_id, $so_id_fe, $so_id_temp); 
        $last_so = $this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe', '$so_id_temp')");

        $so_id_be = $last_so->row()->so_id_be;
        $total = intval($last_so->row()->total_harga) + intval($last_so->row()->shipping_price);
        $total_harga = $last_so->row()->total_harga;
        $shipping_price = $last_so->row()->shipping_price;

        $this->db->close();

        $data_temp_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_temp_ncm != false) {
            foreach ($data_temp_ncm as $row) {

                $data_nc = array(
                    "warehouse_id"  => $row['warehouse_id'],
                    "date"          => $row['ncm_createddate'],
                    "remark"        => 'Free Of SO No. ' . $so_id_be,
                    "createdby"     => 'system',
                    "status"       => $row['status'],
                    "created"       => $row['ncm_created']
                );

                $this->db->insert('ncm', $data_nc);

                $ncm_id = $this->db->insert_id();

                $data_ncd = array(
                    "ncm_id"       => $ncm_id,
                    "item_id"      => $row['ncm_item_id'],
                    "qty"          => $row['ncm_qty'],
                    "hpp"          => $row['hpp'],
                    "event_id"     => $row['event_id'],
                    "flag"         => $row['flag']
                );

                $this->db->insert('ncm_d', $data_ncd);
                $ncd_id = $this->db->insert_id();

                $data_ref = array(
                    "periode"       => $row['periode'],
                    "member_id"     => $member_id,
                    "trx"           => $row['trx'],
                    "trx_id"        => $so_id_be,
                    "nc_id"        => $ncm_id,
                    "created_date"  => $row['ncm_created']
                );

                $this->db->insert('ref_trx_nc', $data_ref);
                $ref_trx_id = $this->db->insert_id();
            }
        }

        $this->db->query("CALL sp_ewallet_mem('$member_id','company','SO','$so_id_be','$total_harga','system')");

        $q = $this->db->select("a.so_id, a.id as so_d_id, a.item_id,a.qty, b.warehouse_id, b.remark", false)
            ->from('so_d a')
            ->join('so b', 'a.so_id = b.id', 'left')
            ->where('a.so_id', $so_id_be)
            ->get();

        $warehouse_id = 1;
        if ($q->num_rows() > 0) {

            foreach ($q->result_array() as $row) {

                $so_id_         = $row['so_id'];
                $so_d_id_       = $row['so_d_id'];
                $item_id_       = $row['item_id'];
                $qty_           = $row['qty'];
                $warehouse_id_  = $row['warehouse_id'];
                $remark_        = $row['remark'];

                $warehouse_id = $warehouse_id_;
                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $so_id_ . ",'D','SO " . $so_id_ . "','" . $item_id_ . "'," . $qty_ . ",'system')");
            }
        }

        $data_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_ncm != false) {
            foreach ($data_ncm as $row) {

                $ncm_id_         = $row['ncm_id'];
                $ncm_d_id_       = $row['ncm_d_id'];
                $ncm_item_id_    = $row['ncm_item_id'];
                $ncm_qty_        = $row['ncm_qty'];
                $warehouse_id_  = $warehouse_id;

                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $ncm_id_ . ",'D','SO " . $ncm_id_ . "','" . $ncm_item_id_ . "'," . $ncm_qty_ . ",'system')");
            }

            $this->delTempNcmD($so_id_temp);
            $this->delTempNcm($so_id_temp);
            $this->delTempreftrxnc($so_id_temp);
        }

        //Update Ewallet FE
        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($total_harga)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Pembayaran SO',
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Order', 'outbound', $post, $url, $result);

        //INsert Widthdawal Ongkir
        $account = $this->getAccountID($member_id);
        $account_id = (($account == false) ? 0 : $account->account_id);

        $data_widthdrawal = array(
            'member_id' => $member_id,
            'tgl' => date('Y-m-d', now()),
            'amount' => $shipping_price,
            'biayaadm' => 0,
            'event_id' => 'wd3',
            'flag' => 'mem',
            '`status`' => 'approved',
            'account_id' => $account_id,
            'remarkapp' => 'SO - ' . $so_id_be,
            'approved' => date('Y-m-d', now()),
            'approvedby' => 'system',
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => 'system',
            'tgl_transfer' => date('Y-m-d')
        );

        $this->db->insert('withdrawal', $data_widthdrawal);
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$member_id','company','$shipping_price','mem','Withdrawal','system')");

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($shipping_price)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Biaya penanganan id order :' . $so_id_fe
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Ongkir', 'outbound', $post, $url, $result);

        //Update Order

        //Update status Order ke FE
        $update_order = $this->update_status_order_transfer($so_id_fe);

        $this->db->query("DELETE from temp_so WHERE so_id_fe='$so_id_fe'");
        $this->db->query("DELETE from temp_so_d WHERE so_id_fe='$so_id_fe'");

        return $so_id_be;
        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function payment_confirmation_order_va($member_id, $so_id_fe, $so_id_temp)
    {
        $last_so = $this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe', '$so_id_temp')");

        $so_id_be = $last_so->row()->so_id_be;
        $total = intval($last_so->row()->total_harga) + intval($last_so->row()->shipping_price);
        $total_harga = intval($last_so->row()->total_harga);
        $shipping_price = intval($last_so->row()->shipping_price);

        $this->db->close();

        $data_temp_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_temp_ncm != false) {
            foreach ($data_temp_ncm as $row) {

                $data_nc = array(
                    "warehouse_id"  => $row['warehouse_id'],
                    "date"          => $row['ncm_createddate'],
                    "remark"        => 'Free Of SO No. ' . $so_id_be,
                    "createdby"     => 'system',
                    "status"       => $row['status'],
                    "created"       => $row['ncm_created']
                );

                $this->db->insert('ncm', $data_nc);

                $ncm_id = $this->db->insert_id();

                $data_ncd = array(
                    "ncm_id"       => $ncm_id,
                    "item_id"      => $row['ncm_item_id'],
                    "qty"          => $row['ncm_qty'],
                    "hpp"          => $row['hpp'],
                    "event_id"     => $row['event_id'],
                    "flag"         => $row['flag']
                );

                $this->db->insert('ncm_d', $data_ncd);
                $ncd_id = $this->db->insert_id();

                $data_ref = array(
                    "periode"       => $row['periode'],
                    "member_id"     => $member_id,
                    "trx"           => $row['trx'],
                    "trx_id"        => $so_id_be,
                    "nc_id"        => $ncm_id,
                    "created_date"  => $row['ncm_created']
                );

                $this->db->insert('ref_trx_nc', $data_ref);
                $ref_trx_id = $this->db->insert_id();
            }
        }

        $this->db->query("CALL sp_ewallet_mem('$member_id','company','SO','$so_id_be','$total_harga','system')");

        $q = $this->db->select("a.so_id, a.id as so_d_id, a.item_id,a.qty, b.warehouse_id, b.remark", false)
            ->from('so_d a')
            ->join('so b', 'a.so_id = b.id', 'left')
            ->where('a.so_id', $so_id_be)
            ->get();

        $warehouse_id = 1;
        if ($q->num_rows() > 0) {

            foreach ($q->result_array() as $row) {

                $so_id_         = $row['so_id'];
                $so_d_id_       = $row['so_d_id'];
                $item_id_       = $row['item_id'];
                $qty_           = $row['qty'];
                $warehouse_id_  = $row['warehouse_id'];
                $remark_        = $row['remark'];

                $warehouse_id = $warehouse_id_;
                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $so_id_ . ",'D','SO " . $so_id_ . "','" . $item_id_ . "'," . $qty_ . ",'system')");
            }
        }

        $data_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_ncm != false) {
            foreach ($data_ncm as $row) {

                $ncm_id_         = $row['ncm_id'];
                $ncm_d_id_       = $row['ncm_d_id'];
                $ncm_item_id_    = $row['ncm_item_id'];
                $ncm_qty_        = $row['ncm_qty'];
                $warehouse_id_  = $warehouse_id;

                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $ncm_id_ . ",'D','SO " . $ncm_id_ . "','" . $ncm_item_id_ . "'," . $ncm_qty_ . ",'system')");
            }

            $this->delTempNcmD($so_id_temp);
            $this->delTempNcm($so_id_temp);
            $this->delTempreftrxnc($so_id_temp);
        }

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($total_harga)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Pembayaran SO',
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Order', 'outbound', $post, $url, $result);

        //INsert Widthdawal Ongkir
        $account = $this->getAccountID($member_id);
        $account_id = (($account == false) ? 0 : $account->account_id);

        $data_widthdrawal = array(
            'member_id' => $member_id,
            'tgl' => date('Y-m-d', now()),
            'amount' => $shipping_price,
            'biayaadm' => 0,
            'event_id' => 'wd3',
            'flag' => 'mem',
            '`status`' => 'approved',
            'account_id' => $account_id,
            'remarkapp' => 'SO - ' . $so_id_be,
            'approved' => date('Y-m-d', now()),
            'approvedby' => 'system',
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => 'system',
            'tgl_transfer' => date('Y-m-d')
        );

        $this->db->insert('withdrawal', $data_widthdrawal);
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$member_id','company','$shipping_price','mem','Withdrawal','system')");

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($shipping_price)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Biaya penanganan id order :' . $so_id_fe
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Ongkir', 'outbound', $post, $url, $result);

        //Update status Order ke FE
        $update_order = $this->update_status_order($so_id_fe);

        $this->db->query("DELETE from temp_so WHERE so_id_fe='$so_id_fe'");
        $this->db->query("DELETE from temp_so_d WHERE so_id_fe='$so_id_fe'");

        return $so_id_be;
        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function payment_confirmation_order_cc($member_id, $so_id_fe, $so_id_temp)
    {

        $last_so = $this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe', '$so_id_temp')");
        $so_id_be = $last_so->row()->so_id_be;
        $total = intval($last_so->row()->total_harga) + intval($last_so->row()->shipping_price);
        $total_harga = $last_so->row()->total_harga;
        $shipping_price = $last_so->row()->shipping_price;

        $this->db->close();

        $data_temp_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_temp_ncm != false) {
            foreach ($data_temp_ncm as $row) {

                $data_nc = array(
                    "warehouse_id"  => $row['warehouse_id'],
                    "date"          => $row['ncm_createddate'],
                    "remark"        => 'Free Of SO No. ' . $so_id_be,
                    "createdby"     => 'system',
                    "status"       => $row['status'],
                    "created"       => $row['ncm_created']
                );

                $this->db->insert('ncm', $data_nc);

                $ncm_id = $this->db->insert_id();

                $data_ncd = array(
                    "ncm_id"       => $ncm_id,
                    "item_id"      => $row['ncm_item_id'],
                    "qty"          => $row['ncm_qty'],
                    "hpp"          => $row['hpp'],
                    "event_id"     => $row['event_id'],
                    "flag"         => $row['flag']
                );

                $this->db->insert('ncm_d', $data_ncd);
                $ncd_id = $this->db->insert_id();

                $data_ref = array(
                    "periode"       => $row['periode'],
                    "member_id"     => $member_id,
                    "trx"           => $row['trx'],
                    "trx_id"        => $so_id_be,
                    "nc_id"        => $ncm_id,
                    "created_date"  => $row['ncm_created']
                );

                $this->db->insert('ref_trx_nc', $data_ref);
                $ref_trx_id = $this->db->insert_id();
            }
        }

        $this->db->query("CALL sp_ewallet_mem('$member_id','company','SO','$so_id_be','$total_harga','system')");

        $q = $this->db->select("a.so_id, a.id as so_d_id, a.item_id,a.qty, b.warehouse_id, b.remark", false)
            ->from('so_d a')
            ->join('so b', 'a.so_id = b.id', 'left')
            ->where('a.so_id', $so_id_be)
            ->get();

        $warehouse_id = 1;
        if ($q->num_rows() > 0) {

            foreach ($q->result_array() as $row) {

                $so_id_         = $row['so_id'];
                $so_d_id_       = $row['so_d_id'];
                $item_id_       = $row['item_id'];
                $qty_           = $row['qty'];
                $warehouse_id_  = $row['warehouse_id'];
                $remark_        = $row['remark'];

                $warehouse_id = $warehouse_id_;
                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $so_id_ . ",'D','SO " . $so_id_ . "','" . $item_id_ . "'," . $qty_ . ",'system')");
            }
        }

        $data_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_ncm != false) {
            foreach ($data_ncm as $row) {

                $ncm_id_         = $row['ncm_id'];
                $ncm_d_id_       = $row['ncm_d_id'];
                $ncm_item_id_    = $row['ncm_item_id'];
                $ncm_qty_        = $row['ncm_qty'];
                $warehouse_id_  = $warehouse_id;

                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $ncm_id_ . ",'D','SO " . $ncm_id_ . "','" . $ncm_item_id_ . "'," . $ncm_qty_ . ",'system')");
            }

            $this->delTempNcmD($so_id_temp);
            $this->delTempNcm($so_id_temp);
            $this->delTempreftrxnc($so_id_temp);
        }

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($total_harga)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Pembayaran SO',
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Order', 'outbound', $post, $url, $result);

        //INsert Widthdawal Ongkir
        $account = $this->getAccountID($member_id);
        $account_id = (($account == false) ? 0 : $account->account_id);

        $data_widthdrawal = array(
            'member_id' => $member_id,
            'tgl' => date('Y-m-d', now()),
            'amount' => $shipping_price,
            'biayaadm' => 0,
            'event_id' => 'wd3',
            'flag' => 'mem',
            '`status`' => 'approved',
            'account_id' => $account_id,
            'remarkapp' => 'SO - ' . $so_id_be,
            'approved' => date('Y-m-d', now()),
            'approvedby' => 'system',
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => 'system',
            'tgl_transfer' => date('Y-m-d')
        );

        $this->db->insert('withdrawal', $data_widthdrawal);
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$member_id','company','$shipping_price','mem','Withdrawal','system')");

        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($shipping_price)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Biaya penanganan id order :' . $so_id_fe
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Ongkir', 'outbound', $post, $url, $result);

        //Update status Order ke FE
        $update_order = $this->update_status_order($so_id_fe);

        $this->db->query("DELETE from temp_so WHERE so_id_fe='$so_id_fe'");
        $this->db->query("DELETE from temp_so_d WHERE so_id_fe='$so_id_fe'");

        return $so_id_be;
        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function payment_confirmation_order_ewallet($member_id, $so_id_fe, $so_id_temp)
    {

        $last_so = $this->db->query("CALL sp_insert_so_from_temp('$member_id', '$so_id_fe', '$so_id_temp')");

        $so_id_be = $last_so->row()->so_id_be;
        $total = intval($last_so->row()->total_harga) + intval($last_so->row()->shipping_price);
        $total_harga = $last_so->row()->total_harga;
        $shipping_price = $last_so->row()->shipping_price;

        $this->db->close();

        $data_temp_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_temp_ncm != false) {
            foreach ($data_temp_ncm as $row) {

                $data_nc = array(
                    "warehouse_id"  => $row['warehouse_id'],
                    "date"          => $row['ncm_createddate'],
                    "remark"        => 'Free Of SO No. ' . $so_id_be,
                    "createdby"     => 'system',
                    "status"       => $row['status'],
                    "created"       => $row['ncm_created']
                );

                $this->db->insert('ncm', $data_nc);

                $ncm_id = $this->db->insert_id();

                $data_ncd = array(
                    "ncm_id"       => $ncm_id,
                    "item_id"      => $row['ncm_item_id'],
                    "qty"          => $row['ncm_qty'],
                    "hpp"          => $row['hpp'],
                    "event_id"     => $row['event_id'],
                    "flag"         => $row['flag']
                );

                $this->db->insert('ncm_d', $data_ncd);
                $ncd_id = $this->db->insert_id();

                $data_ref = array(
                    "periode"       => $row['periode'],
                    "member_id"     => $member_id,
                    "trx"           => $row['trx'],
                    "trx_id"        => $so_id_be,
                    "nc_id"        => $ncm_id,
                    "created_date"  => $row['ncm_created']
                );

                $this->db->insert('ref_trx_nc', $data_ref);
                $ref_trx_id = $this->db->insert_id();
            }
        }

        $this->db->query("CALL sp_ewallet_mem('$member_id','company','SO','$so_id_be','$total_harga','system')");

        $q = $this->db->select("a.so_id, a.id as so_d_id, a.item_id,a.qty, b.warehouse_id, b.remark", false)
            ->from('so_d a')
            ->join('so b', 'a.so_id = b.id', 'left')
            ->where('a.so_id', $so_id_be)
            ->get();

        $warehouse_id = 1;
        if ($q->num_rows() > 0) {

            foreach ($q->result_array() as $row) {

                $so_id_         = $row['so_id'];
                $so_d_id_       = $row['so_d_id'];
                $item_id_       = $row['item_id'];
                $qty_           = $row['qty'];
                $warehouse_id_  = $row['warehouse_id'];
                $remark_        = $row['remark'];

                $warehouse_id = $warehouse_id_;
                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $so_id_ . ",'D','SO " . $so_id_ . "','" . $item_id_ . "'," . $qty_ . ",'system')");
            }
        }

        $data_ncm = $this->get_temp_ncm($so_id_temp, $member_id, $so_id_fe);

        if ($data_ncm != false) {
            foreach ($data_ncm as $row) {

                $ncm_id_         = $row['ncm_id'];
                $ncm_d_id_       = $row['ncm_d_id'];
                $ncm_item_id_    = $row['ncm_item_id'];
                $ncm_qty_        = $row['ncm_qty'];
                $warehouse_id_  = $warehouse_id;

                $this->db->query("CALL sp_inventory_payment_confirm(" . $warehouse_id_ . ", " . $ncm_id_ . ",'D','SO " . $ncm_id_ . "','" . $ncm_item_id_ . "'," . $ncm_qty_ . ",'system')");
            }

            $this->delTempNcmD($so_id_temp);
            $this->delTempNcm($so_id_temp);
            $this->delTempreftrxnc($so_id_temp);
        }

        //INsert Widthdawal Ongkir
        $account = $this->getAccountID($member_id);
        $account_id = (($account == false) ? 0 : $account->account_id);

        $data_widthdrawal = array(
            'member_id' => $member_id,
            'tgl' => date('Y-m-d', now()),
            'amount' => $shipping_price,
            'biayaadm' => 0,
            'event_id' => 'wd3',
            'flag' => 'mem',
            '`status`' => 'approved',
            'account_id' => $account_id,
            'remarkapp' => 'SO - ' . $so_id_be,
            'approved' => date('Y-m-d', now()),
            'approvedby' => 'system',
            'created' => date('Y-m-d H:i:s', now()),
            'createdby' => 'system',
            'tgl_transfer' => date('Y-m-d')
        );

        $this->db->insert('withdrawal', $data_widthdrawal);
        $id = $this->db->insert_id();
        $this->db->query("call sp_withdrawal('$id','$member_id','company','$shipping_price','mem','Withdrawal','system')");

        /*
        $member_id_fe = $this->MEwallet->getMemberIdFEByIdBe($member_id);

        $walletdata = array(
            "customer_id" => intval($member_id_fe),
            "walletamount" => intval($shipping_price)
        );

        $data_deposit_post = array(
            "walletdata"        => array($walletdata),
            "walletactiontype"  => 'debit',
            "walletnote"        => 'Biaya penanganan id order :' . $so_id_fe
        );

        $token = get_token();

        $url = get_url_api() . 'rest/V1/ewallet/adjustment/data';
        $ch = curl_init($url);

        $post = json_encode($data_deposit_post);

        $authorization = "Authorization: Bearer $token";

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        activity_log('Withdrawal Ongkir', 'outbound', $post, $url, $result);
        */

        //Update status Order ke FE
        $update_order = $this->update_status_order($so_id_fe);

        $this->db->query("DELETE from temp_so WHERE so_id_fe='$so_id_fe'");
        $this->db->query("DELETE from temp_so_d WHERE so_id_fe='$so_id_fe'");

        return $so_id_be;
        //return $var = ($q->num_rows() > 0) ? true : false;
    }

    public function getAccountID($memberid)
    {
        $q = $this->db->select("id,account_id", false)
            ->from('member')
            ->where('id', $memberid)
            ->where('account_id >', 0)
            ->get();
        return $var = ($q->num_rows() > 0) ? $q->row() : false;
    }

    public function get_sales_order($member_id, $so_id_fe)
    {

        $q = $this->db->select("*", false)
            ->from('temp_so')
            ->where('member_id', $member_id)
            ->where('so_id_fe', $so_id_fe)
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {

            $data = $q->row_array();

            return $data;
        } else {

            return false;
        }
    }

    public function get_sales_order_by_so_id_be($so_id_be)
    {

        $q = $this->db->select("*", false)
            ->from('so')
            ->where('id', $so_id_be)
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {

            $data = $q->row_array();

            return $data;
        } else {

            return false;
        }
    }

    public function get_so_detail_by_so_id_be($so_id_be)
    {

        $q = $this->db->select("*", false)
            ->from('so_d')
            ->where('so_id', $so_id_be)
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {

            $data = $q->result_array();

            return $data;
        } else {

            return false;
        }
    }

    public function get_sales_order_by_so_id_fe($so_id_fe)
    {

        $q = $this->db->select("*", false)
            ->from('so')
            ->where('so_id_fe', $so_id_fe)
            ->get();

        echo $this->db->last_query();
        die();

        if ($q->num_rows() > 0) {

            $data = $q->row_array();

            return $data;
        } else {

            return false;
        }
    }

    public function get_sales_order_pending($member_id, $so_id_fe)
    {

        $q = $this->db->select("*", false)
            ->from('temp_so')
            ->where('member_id', $member_id)
            ->where('so_id_fe', $so_id_fe)
            ->where('status', 'pending')
            ->get();

        if ($q->num_rows() > 0) {

            $data = $q->row_array();

            return $data;
        } else {

            return false;
        }
    }

    public function get_so_id_temp($member_id, $so_id_fe)
    {

        $q = $this->db->select("*", false)
            ->from('temp_so')
            ->where('member_id', $member_id)
            ->where('so_id_fe', $so_id_fe)
            ->where('status', 'pending')
            ->get();

        //echo $this->db->last_query(); die();

        if ($q->num_rows() > 0) {

            $so_id_temp = $q->row('id');

            return $so_id_temp;
        } else {

            return false;
        }
    }

    public function get_sales_order_detail($so_id_be)
    {

        $q = $this->db->select("*", false)
            ->from('so_d')
            ->where('so_id', $so_id_be)
            ->get();

        if ($q->num_rows() > 0) {
            $data = $q->result();
            return $data;
        } else {
            return false;
        }
    }

    public function check_crossline($p, $i)
    {
        $data = array();
        $q = $this->db->query("SELECT f_check_crossline('$p','$i') as l_result");
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        //print_r($data);
        $q->free_result();
        return $data['l_result'];
    }

    public function _check_userid($p, $p2)
    {
        $array = array('member_id' => $p, 'code' => $p2, 'status' => 'not used');
        $q = $this->db->select("account")
            ->from('activation_code')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? false : true;
    }

    public function check_memberid($member_id)
    {
        $array = array('temp_member_id' => $member_id);
        $q = $this->db->select("temp_member_id")
            ->from('temp_member_fe')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? false : true;
    }

    public function _check_ktp($noktp)
    {
        $array = array('noktp' => $noktp);
        $q = $this->db->select("noktp")
            ->from('member')
            ->where($array)
            ->get();
        return $var = ($q->num_rows() > 0) ? false : true;
    }

    public function get_temp_ncm($so_id, $member_id, $so_id_fe)
    {

        $query = $this->db->query("SELECT nc.warehouse_id, nc.remarkapp, nc.status, nc.created as ncm_created, ref.periode, ref.trx, ncd.hpp, ncd.event_id, ncd.flag, ref.trx_id AS so_id_be, $so_id_fe as so_id_fe, ref.nc_id as ncm_id, nc.remark as ncm_remark, nc.date as ncm_createddate, ncd.id as ncm_d_id, ncd.item_id as ncm_item_id, ncd.qty as ncm_qty
                            FROM temp_ref_trx_nc ref
                            LEFT JOIN temp_ncm nc ON ref.nc_id = nc.id
                            LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
                            WHERE ref.member_id = '$member_id'
                            AND ref.trx_id = '$so_id'");

        // GROUP BY ref.id ");
        //$query->free_result();

        //echo $this->db->last_query(); die();

        if ($query->num_rows() > 0) {
            $var = $query->result_array();
        } else {
            $var = false;
        }

        return $var;
    }

    public function get_temp_so_by_so_id_fe($id_list)
    {

        $where = "so_id_fe in ($id_list)";
        $option = $where;

        $data = array();
        $this->db->select("*", false);
        $this->db->where($option);
        $q = $this->db->get('temp_so');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }

        $q->free_result();
        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | Signup member
    |--------------------------------------------------------------------------
    |
    | untuk signup/register member online
    |
    | @author taQwa qtakwa@yahoo.com@yahoo.com
    | @author 2009-04-27
    |
    */

    /*Modified by Boby : 2010-01-21*/
    //public function signup($name){
    public function stock_reserved($whsid, $soid, $item_id, $qty, $empid, $item_whsid)
    {

        $this->db->query("CALL sp_stock_summary_check($item_whsid)");
        $this->db->query("CALL sp_inventory_temp_member($item_whsid,$soid,'D','SO KIT Temp','$item_id',$qty,'$empid')");

        return true;
    }

    /*End Modified by Boby : 2010-01-21*/
    protected function _getWarehouseID($memberid)
    {
        $q = $this->db->select("warehouse_id", false)
            ->from('users')
            ->where('id', $memberid)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        return $data;
    }

    public function getWhsidByCityId($city_id)
    {
        $data = 0;
        $q = $this->db->select("warehouse_id", false)
            ->from('kota')
            ->where('id', $city_id)
            ->get();
        if ($q->num_rows() > 0) {
            $data = $q->row('warehouse_id');
        }
        return $data;
    }


    /*
    |--------------------------------------------------------------------------
    | login
    |--------------------------------------------------------------------------
    |
    | @param string $username Vaild username
    | @param string $password Password
    | @return mixed
    | @author taQwa
    | @author 2008-11-04
    |
    */
    public function login($username, $password)
    {
        # Grab hash, password, id, activation_code and banned_id from database.
        $result = $this->_login($username);
        if ($result) {
            if ($password === $result->password) {
                $this->db->set('last_login', 'NOW()', false);
                $this->db->where('id', $result->id);
                $this->db->update('users', $data = array());
                $this->session->set_userdata(array('logged_in' => 'true', 'userid' => $result->id, 'last_login' => $result->last_login, 'name' => $result->nama, 'username' => $result->username, 'group_id' => $result->group_id));
                return 'SUCCESS';
            }
        }
        return false;
    }

    protected function _login($username)
    {
        $i = $this->db->select("u.password,u.username,u.last_login,m.nama,u.id,u.group_id")
            ->from('users u')
            ->join('member m', 'u.id = m.id', 'left')
            ->where('u.id', $username)
            ->limit(1)
            ->get();
        return $var = ($i->num_rows() > 0) ? $i->row() : false;
    }

    public function getParentID($placementid)
    {
        $data = array();
        $i = $this->db->select("id")
            ->from('networks')
            ->where('member_id', $placementid)
            ->get();
        if ($i->num_rows() > 0) {
            $data = $i->row_array();
        }
        $i->free_result();
        return $data;
    }

    /*
    |--------------------------------------------------------------------------
    | add Register Member
    |--------------------------------------------------------------------------
    |
    | @author qtakwa@yahoo.com@yahoo.com
    | @poweredby www.smartindo-technology.com
    | @created 2009-06-24
    |
    */
    public function add_register()
    {
        //$portal = $this->input->post('zip');
        //$info = $this->input->post('infofrom');
        $info_ = 0;
        $portal = 0;
        $data = array(
            'tgl' => date('Y-m-d', now()),
            'fullname' => $this->input->post('name'),
            'hp' => $this->input->post('hp'),
            'email' => $this->input->post('email'),
            'address' => $this->db->escape_str($this->input->post('address')),
            'zip' => $portal,
            'delivery' => '0',
            'infofrom' => $info_
        );
        $this->db->insert('register', $data);
    }
    public function searchRegister($keywords = 0, $num, $offset)
    {
        $data = array();
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->select("a.id,date_format(a.tgl,'%d-%b-%Y')as tgl,a.fullname,a.hp,a.email,delivery,a.infofrom", false);
        $this->db->from('register a');
        $this->db->where($where);
        $this->db->order_by('a.id', 'desc');
        $this->db->limit($num, $offset);
        $q = $this->db->get();
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function countRegister($keywords = 0)
    {
        $where = "( a.id LIKE '$keywords%' or a.fullname LIKE '$keywords%' )";
        $this->db->from('register a');
        $this->db->where($where);
        return $this->db->count_all_results();
    }

    public function getRegister($id)
    {
        $data = array();
        $q = $this->db->get_where('register', array('id' => $id));
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }
        $q->free_result();
        return $data;
    }
    public function register_online_del()
    {
        if ($this->input->post('p_id')) {
            $row = array();

            $idlist = implode(",", array_values($this->input->post('p_id')));
            $where = "id in ($idlist)";

            $row = $this->_countDelete($where);
            if ($row) {

                $this->db->delete('register', $where);

                $this->session->set_flashdata('message', 'Delete register online successfully');
            } else {
                $this->session->set_flashdata('message', 'Nothing to delete register online!');
            }
        } else {
            $this->session->set_flashdata('message', 'Nothing to delete register online!');
        }
    }
    private function _countDelete($option)
    {
        $data = array();
        $this->db->select("id", false);
        $this->db->where($option);
        $q = $this->db->get('register');
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $data[] = $row;
            }
        }
        $q->free_result();
        return $data;
    }

    public function updateNetwork($memberid, $parentid)
    {
        $this->db->update('networks', array('parentid' => $parentid), array('member_id' => $memberid));
    }
    //'updated' => date('Y-m-d H:i:s',now()),
    //'updatedby' => $this->session->userdata('user')

    /* created by Boby 2011-01-13 */
    public function cek_stock_item_whsid($item_id, $qty, $whsid)
    {

        $where_ = "item_id = '$item_id' AND ";
        //$where_ .= "qty >= $qty AND ";
        $where_ .= "warehouse_id = '$whsid'";
        $debe = "stock";
        $where_ .= "ORDER BY qty DESC LIMIT 0,1";
        $this->db->select("qty", false);
        $this->db->where($where_);
        $q = $this->db->get($debe);
        //echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            $row1 = $q->row_array();
        } else {
            return false;
        }
        $q->free_result();

        if ($row1["qty"] >= $qty) {
            return true;
        } else {
            return false;
        }
        //echo $this->db->last_query();
    }

    public function get_detail_item_by_id($item_id)
    {

        $where = "id = '$item_id'";
        $debe = "item";
        $this->db->select("*", false);
        $this->db->where($where);
        $q = $this->db->get($debe);

        if ($q->num_rows() > 0) {
            return $row1 = $q->row_array();
        } else {
            return false;
        }
        $q->free_result();
    }

    public function cekSKit($member_id, $actCode)
    {
        /*
		"SELECT item_id FROM activation_code WHERE `status` = 'not used' AND member_id = '00001977' AND `code` = '225f524c86bb';"
		"SELECT IFNULL(qty,0)AS qty FROM titipan WHERE member_id = '00000009' AND item_id = '101';"
		*/

        $this->db->select("item_id, stockiest_id", false);
        $this->db->where("`status` = 'not used' AND member_id = '$member_id' AND `code` = '$actCode'");
        $q = $this->db->get('activation_code');
        if ($q->num_rows() > 0) {
            $row = $q->row_array();
        }
        $item_id = $row["item_id"];
        $stc = $row["stockiest_id"];
        $q->free_result();

        if ($stc == '0') {
            $where_ = "item_id = '$item_id'";
            $debe = "stock";
        } else {
            $where_ = "member_id = '$stc' AND item_id = '$item_id'";
            $debe = "titipan";
        }
        $where_ .= "ORDER BY qty DESC LIMIT 0,1";
        $this->db->select("qty", false);
        $this->db->where($where_);
        $q = $this->db->get($debe);
        if ($q->num_rows() > 0) {
            $row1 = $q->row_array();
        } else {
            return true;
        }
        $q->free_result();

        if ($row1["qty"] > 0) {
            return false;
        } else {
            return true;
            echo $this->db->last_query();
        }
    }
    /* end created by Boby 2011-01-13 */

    public function check_norekening($norek)
    {
        $i = $this->db->select("id")
            ->from('account')
            ->where('no', $norek)
            ->get();
        //echo $this->db->last_query();

        return $var = ($i->num_rows() > 0) ? true : false;
    }

    public function getListTempSo()
    {
        $qry = "SELECT *
                FROM
	                ( `temp_so` ) 
                WHERE
	                created < NOW() - INTERVAL 1 DAY 
                ORDER BY
	                `id` DESC";

        $q = $this->db->query($qry);

        if($q->num_rows() > 0){
            return $q->result_array();
        }else{
            return false;
        }
    }

    public function getListProductStock()
    {
        $qry = "SELECT sum(b.qty) as qty, a.id, a.product_id_fe, a.name
                FROM item a
                JOIN stock b ON b.item_id=a.id
                GROUP BY b.item_id";

        $q = $this->db->query($qry);

        if($q->num_rows() > 0){
            return $q->result_array();
        }else{
            return false;
        }
    }

    public function getListProduct()
    {
        $qry = "SELECT *
                FROM item";

        $q = $this->db->query($qry);

        if($q->num_rows() > 0){
            return $q->result_array();
        }else{
            return false;
        }
    }

}
