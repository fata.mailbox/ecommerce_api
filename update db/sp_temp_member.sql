DELIMITER $$

DROP PROCEDURE IF EXISTS `sp_inventory_temp_member`$$

CREATE PROCEDURE `sp_inventory_temp_member`(in_whsid INTEGER,in_descid INTEGER,in_dk CHAR(1),in_desc VARCHAR(50),in_itemid VARCHAR(10),in_qty INTEGER,in_empid VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	DECLARE l_saldoawal INTEGER DEFAULT 0;
	DECLARE l_saldoawal_r INTEGER DEFAULT 0;
	DECLARE l_date DATE DEFAULT NOW();
	START TRANSACTION;
	
	SELECT qty INTO l_saldoawal FROM stock WHERE warehouse_id=in_whsid AND item_id=in_itemid LIMIT 0,1; 
	SELECT qty_reserved INTO l_saldoawal_r FROM stock WHERE warehouse_id=in_whsid AND item_id=in_itemid LIMIT 0,1; 
	
	IF in_dk = 'K' THEN
		UPDATE stock SET qty=qty + in_qty, qty_reserved=qty_reserved-in_qty WHERE warehouse_id=in_whsid AND item_id=in_itemid;
		INSERT INTO stock_card(desc_id,DATE,description,warehouse_id,item_id,saldo_awal,saldo_in,saldo_akhir,created,createdby)
			VALUES(in_descid,CURDATE(),in_desc,in_whsid,in_itemid,l_saldoawal,in_qty,l_saldoawal + in_qty,NOW(),in_empid);
		UPDATE stock_summary SET saldoin=saldoin + in_qty,saldoakhir=saldoakhir + in_qty WHERE warehouse_id=in_whsid AND item_id=in_itemid AND tgl=l_date;
	ELSE
		UPDATE stock SET qty=qty - in_qty, qty_reserved=qty_reserved+in_qty WHERE warehouse_id=in_whsid AND item_id=in_itemid;
		INSERT INTO stock_card(desc_id,DATE,description,warehouse_id,item_id,saldo_awal,saldo_out,saldo_akhir,created,createdby)
			VALUES(in_descid,CURDATE(),in_desc,in_whsid,in_itemid,l_saldoawal,in_qty,l_saldoawal - in_qty,NOW(),in_empid);
		UPDATE stock_summary SET saldoout=saldoout + in_qty,saldoakhir=saldoakhir - in_qty WHERE warehouse_id=in_whsid AND item_id=in_itemid AND tgl=l_date;
	END IF;	
	COMMIT; 
END$$

DELIMITER ;
