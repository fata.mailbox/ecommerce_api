CREATE PROCEDURE `sp_so_temp_tail_value2`(in_id INTEGER, in_whsid VARCHAR(20), in_empid VARCHAR(20),in_promo_id_ INTEGER, in_promo_value_ FLOAT, in_free_item_id_ VARCHAR(100), in_free_item_qty_ INTEGER, in_multiplies_ INTEGER)
    MODIFIES SQL DATA
BEGIN
	DECLARE jml_ INTEGER DEFAULT 0;
	DECLARE member_id_ VARCHAR(100);
	START TRANSACTION;
	block_cur:BEGIN
	DECLARE member_id_, remark_, item_bns_ VARCHAR(35);
	DECLARE so_id_, nc_id_ INTEGER DEFAULT 0;
	DECLARE oms_, hpp_ DOUBLE DEFAULT 0;
	DECLARE tgl_ DATETIME DEFAULT 0;
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE target_ DOUBLE DEFAULT 1500000;
	DECLARE qty_bns_ DOUBLE DEFAULT 1;
	DECLARE cursor1 CURSOR FOR
		SELECT
		temp_so.member_id, FLOOR(so.totalharga/in_promo_value_) AS jml
		FROM temp_so
		WHERE temp_so.id = in_id
		;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done :=1;
	OPEN cursor1;
	loop1:LOOP FETCH cursor1 INTO member_id_, jml_;
		IF done THEN
			CLOSE cursor1;
			LEAVE loop1;
		END IF;
		IF jml_ > 0 THEN
			IF in_multiplies_ > 0 THEN
				SET qty_bns_ = jml_ * in_free_item_qty_;
			ELSE
				SET qty_bns_ = in_free_item_qty_;
			END IF;
			SELECT i.hpp INTO hpp_  FROM item i WHERE i.id = in_free_item_id_;
			SET item_bns_ = in_free_item_id_;
			SET remark_ = CONCAT("Promo Value of SO no.", in_id);
			INSERT INTO `temp_ncm`(`id`,`warehouse_id`,`date`,`remark`,`created`,`createdby`)
				VALUES(NULL,in_whsid,NOW(),remark_,NOW(),in_empid);
			SELECT LAST_INSERT_ID() INTO nc_id_;
			INSERT INTO `temp_ncm_d`(`id`,`ncm_id`,`item_id`,`qty`,`hpp`,`event_id`,`flag`)
				VALUES(NULL, nc_id_, item_bns_, qty_bns_, hpp_, 'NC1', '0');
			INSERT INTO `temp_ref_trx_nc`(`id`,`periode`,`member_id`,`trx`,`trx_id`,`nc_id`,`created_date`)
				VALUES(NULL,NOW(),member_id_,'SO',in_id,nc_id_,CURRENT_TIMESTAMP);
			CALL sp_inventory_temp_member(in_whsid,nc_id_,'D','NC',item_bns_,qty_bns_,in_empid);
		END IF;
	END LOOP loop1;
	END block_cur;
	COMMIT;
END