/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : db_soho

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 07/07/2020 16:11:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for temp_so
-- ----------------------------
DROP TABLE IF EXISTS `temp_so`;
CREATE TABLE `temp_so`  (
  `id` double NOT NULL AUTO_INCREMENT,
  `tgl` date NOT NULL,
  `stockiest_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `member_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `totalharga` double NOT NULL,
  `totalharga_` double NOT NULL DEFAULT 0 COMMENT 'Harga total asli (muncul di invoice so)',
  `totalpv` double NOT NULL,
  `totalbv` double NOT NULL,
  `kit` enum('N','Y') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'N',
  `remark` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `warehouse_id` int(2) NOT NULL DEFAULT 0,
  `totalppn` double NOT NULL,
  `totalsales` double NOT NULL,
  `created` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `createdby` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `delivery_addr` int(11) NOT NULL DEFAULT 0,
  `statuspu` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `status` enum('pending','delivery','done') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'pending',
  `status1` enum('pending','delivery') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'pending',
  `status2` enum('pending','delivery') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'pending',
  `remarkapp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tglapproved` date NOT NULL,
  `approvedby` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tglapproved1` datetime(0) NULL DEFAULT NULL,
  `approvedby1` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tglapproved2` datetime(0) NULL DEFAULT NULL,
  `approvedby2` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_canceled` enum('yes','no') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'no',
  `total_discount_rp` double NULL DEFAULT NULL,
  `total_discount_rp_voucher` double NULL DEFAULT NULL,
  `total_discount_pv_voucher` double NULL DEFAULT NULL,
  `total_discount_bv_voucher` double NULL DEFAULT NULL,
  `vouchercode` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `delivery_addr_id` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `delivery_city_id` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `shipping_price` double NULL DEFAULT NULL,
  `total_bv_` double NULL DEFAULT NULL,
  `total_pv_` double NULL DEFAULT NULL,
  `so_id_fe` double NULL DEFAULT NULL,
  `payment_type` int(11) NULL DEFAULT NULL COMMENT '0 = ewallet, 1= payment gateway, 2 = Manual Bank transfer',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `member_id_so_idx`(`member_id`) USING BTREE,
  INDEX `wh_id_idx`(`warehouse_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'tabel sales order master' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
