/*
 Navicat Premium Data Transfer

 Source Server         : omegaciptasolusi.com
 Source Server Type    : MySQL
 Source Server Version : 100323
 Source Host           : omegaciptasolusi.com:3306
 Source Schema         : omegacip_soho

 Target Server Type    : MySQL
 Target Server Version : 100323
 File Encoding         : 65001

 Date: 14/07/2020 14:32:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for temp_member
-- ----------------------------
DROP TABLE IF EXISTS `temp_member`;
CREATE TABLE `temp_member`  (
  `member_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `sponsor_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `posisi` int(11) NOT NULL DEFAULT 0,
  `bv` double NOT NULL DEFAULT 0,
  `ps` double NOT NULL DEFAULT 0,
  `pgs` double NOT NULL DEFAULT 0,
  `aps` double NOT NULL DEFAULT 0,
  `apsbak` double NOT NULL DEFAULT 0,
  `apgs` double NOT NULL DEFAULT 0,
  `apgsbak` double NOT NULL DEFAULT 0,
  `sumbangan` double NOT NULL DEFAULT 0,
  `pgs_cut` double NOT NULL DEFAULT 0,
  `qtyleader` int(11) NOT NULL DEFAULT 0,
  `q60` int(2) NOT NULL DEFAULT 0,
  `lq_tgpv` double NOT NULL DEFAULT 0,
  `lq` int(2) NOT NULL DEFAULT 0,
  `pgs_cut_old` double NULL DEFAULT NULL,
  `cut_sponsor` int(11) NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`member_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
