CREATE PROCEDURE `sp_so_temp_tail_value`(in_id INTEGER, in_whsid VARCHAR(20), in_empid VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	DECLARE promo_id_, free_item_qty_, multiplies_ INTEGER DEFAULT 0;
	DECLARE promo_value_ FLOAT DEFAULT 0;
	DECLARE free_item_id_ VARCHAR(100);
	START TRANSACTION;
	UPDATE promo_value SET expired = 1 WHERE end_periode < (NOW()-INTERVAL 1 DAY) AND expired < 1;
	block_cur:BEGIN
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE cursor1 CURSOR FOR
		SELECT
		a.id, a.value, a.free_item_id, a.free_item_qty, a.multiplies
		FROM promo_value a
		WHERE a.expired = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done :=1;
	OPEN cursor1;
	loop1:LOOP FETCH cursor1 INTO promo_id_, promo_value_, free_item_id_, free_item_qty_, multiplies_;
		IF done THEN
			CLOSE cursor1;
			LEAVE loop1;
		END IF;
		CALL sp_so_temp_tail_value2(in_id, in_whsid, in_empid ,promo_id_, promo_value_, free_item_id_, free_item_qty_, multiplies_);
	END LOOP loop1;
	END block_cur;
	COMMIT;
END