CREATE PROCEDURE `sp_so_temp_tail3`(in_id INTEGER)
    MODIFIES SQL DATA
BEGIN
	START TRANSACTION;
	block_cur:BEGIN
	DECLARE sod_id_, done INTEGER DEFAULT 0;
	DECLARE price2_, qty_ DOUBLE DEFAULT 0;
	DECLARE cursor1 CURSOR FOR
		SELECT sod.id, i.price2, sod.qty
		FROM temp_so_d sod
		LEFT JOIN item i ON sod.item_id = i.id
		LEFT JOIN temp_so ON sod.so_id = temp_so.id
		LEFT JOIN member_delivery md ON temp_so.delivery_addr = md.id
		LEFT JOIN kota k ON md.kota = k.id
		WHERE temp_so.id = in_id
		AND temp_so.statuspu = 0
		AND k.timur = 1
		;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done :=1;
	OPEN cursor1;
	loop1:LOOP FETCH cursor1 INTO sod_id_, price2_, qty_;
		IF done THEN
			CLOSE cursor1;
			LEAVE loop1;
		END IF;
		UPDATE temp_so_d SET harga_ = price2_, jmlharga_ = price2_ * qty_ WHERE id = sod_id_ AND pv > 0 AND so_id = in_id;
	END LOOP loop1;
	END block_cur;
	COMMIT;
END