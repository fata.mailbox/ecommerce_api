/*
 Navicat Premium Data Transfer

 Source Server         : omegaciptasolusi.com
 Source Server Type    : MySQL
 Source Server Version : 100323
 Source Host           : omegaciptasolusi.com:3306
 Source Schema         : omegacip_soho

 Target Server Type    : MySQL
 Target Server Version : 100323
 File Encoding         : 65001

 Date: 14/07/2020 14:31:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for temp_member_fe
-- ----------------------------
DROP TABLE IF EXISTS `temp_member_fe`;
CREATE TABLE `temp_member_fe`  (
  `temp_member_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenjang_id` tinyint(4) NOT NULL,
  `jenjang_id2` tinyint(2) NOT NULL,
  `tgljenjang` date NOT NULL,
  `highjenjang` tinyint(4) NOT NULL,
  `jmlpm` int(4) NOT NULL,
  `jmllpm` int(4) NOT NULL,
  `jmlmp` int(4) NOT NULL,
  `sponsor_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `enroller_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stockiest_id` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kecamatan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kelurahan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `delivery_addr` int(11) NOT NULL,
  `kodepos` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kota_id` int(6) NOT NULL,
  `telp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fax` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `noktp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tempatlahir` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgllahir` date NOT NULL,
  `jk` enum('Laki-laki','Perempuan') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `suami` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `npwp` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tglberlaku` date NOT NULL,
  `ahliwaris` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ewallet` double NOT NULL,
  `tglaplikasi` date NOT NULL,
  `posisi` int(6) NOT NULL COMMENT 'pisisi level dari 0 / perusahaan',
  `flag` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT 'Jml member',
  `pv_kesuksesan` double NOT NULL DEFAULT 0,
  `pvg_kesuksesan` double NOT NULL DEFAULT 0,
  `pvg_hit_cut` int(11) NOT NULL DEFAULT 0,
  `pvg_cut_kesuksesan` double NOT NULL DEFAULT 0,
  `bv` double NOT NULL,
  `bvbak` double NOT NULL,
  `ps` double NOT NULL COMMENT 'PS bulan sekarang',
  `psbak` double NOT NULL,
  `aps` double NOT NULL COMMENT 'akumulasi PS',
  `apsbak` double NOT NULL COMMENT 'backup akumulasi PS',
  `pgs` double NOT NULL COMMENT 'PGS bulan sekarang',
  `pgsbak` double NOT NULL,
  `apgs` double NOT NULL COMMENT 'akumulasi PGS',
  `apgsbak` double NOT NULL COMMENT 'Backup akumulasi pgs',
  `pgs_temp` double NOT NULL,
  `pgs_cut` double NOT NULL,
  `nextps` double NOT NULL,
  `bpg` tinyint(4) NOT NULL COMMENT '0:blum dpt 1:hp dll',
  `ips` double NOT NULL DEFAULT 0 COMMENT 'inject_point',
  `ipgs` double NOT NULL DEFAULT 0 COMMENT 'inject_point_group',
  `kit` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'nama pertama kit',
  `tp` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT 'flag tutup poin',
  `lb` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT '1: flag LB',
  `gpb` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT '1: flag GPB',
  `db` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `elb` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `png` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0' COMMENT '1: flag P&G',
  `png2` tinyint(4) NOT NULL COMMENT '1: sdh dpt HP 2: mtr dll',
  `pvbsn` double NOT NULL,
  `bsn` enum('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1:current active 2:active 3:reactive 9:nonactive',
  `qtyleader` int(11) NOT NULL,
  `sumbangan` decimal(16, 4) NOT NULL,
  `lengkap` tinyint(4) NOT NULL,
  `created` date NOT NULL,
  `createdby` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `updated` date NOT NULL,
  `updatedby` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lq` int(11) NOT NULL DEFAULT 0,
  `lq_tgpv` double NOT NULL DEFAULT 0,
  `real_pv_kesuksesan` double NULL DEFAULT 0,
  `real_pvg_kesuksesan` double NULL DEFAULT 0,
  `real_pvg_cut_kesuksesan` double NULL DEFAULT 0,
  `pgs_cut_old` double NULL DEFAULT 0,
  `cut_sponsor` int(11) NULL DEFAULT 0,
  `password` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hash` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `activation_code` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `member_id_fe` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_id_fe` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`temp_member_id`) USING BTREE,
  INDEX `nama`(`nama`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
