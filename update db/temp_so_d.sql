/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : db_soho

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 11/06/2020 20:41:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for temp_so_d
-- ----------------------------
DROP TABLE IF EXISTS `temp_so_d`;
CREATE TABLE `temp_so_d`  (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `so_id` INT(11) NOT NULL,
  `item_id` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qty` INT(8) NOT NULL,
  `harga` DOUBLE NOT NULL,
  `harga_` DOUBLE NOT NULL DEFAULT 0,
  `hpp` DOUBLE NOT NULL,
  `pv` DOUBLE NOT NULL,
  `bv` DOUBLE NOT NULL,
  `jmlharga` DOUBLE NOT NULL,
  `jmlharga_` DOUBLE NOT NULL DEFAULT 0,
  `jmlpv` DOUBLE NOT NULL,
  `jmlbv` DOUBLE NOT NULL,
  `titipan_id` INT(11) NOT NULL,
  `event_id` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `flag` ENUM('0','1') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `warehouse_id` INT(11) NULL DEFAULT NULL,
  `topup_id` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pv_` DOUBLE NULL DEFAULT NULL,
  `bv_` DOUBLE NULL DEFAULT NULL,
  `discount_rp` DOUBLE NULL DEFAULT NULL,
  `discount_rp_voucher` DOUBLE NULL DEFAULT NULL,
  `discount_pv_voucher` DOUBLE NULL DEFAULT NULL,
  `discount_bv_voucher` DOUBLE NULL DEFAULT NULL,
  `jmldiscount_rp` DOUBLE NULL DEFAULT NULL,
  `jmldiscount_rp_voucher` DOUBLE NULL DEFAULT NULL,
  `jmldiscount_pv_voucher` DOUBLE NULL DEFAULT NULL,
  `jmlpv_` DOUBLE NULL DEFAULT NULL,
  `jmlbv_` DOUBLE NULL DEFAULT NULL,
  `so_d_id_fe` DOUBLE NULL DEFAULT NULL,
  `so_id_fe` DOUBLE NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = INNODB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci COMMENT = 'sales order detail' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
