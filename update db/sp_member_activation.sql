CREATE PROCEDURE `sp_member_activation`(in_introducerid VARCHAR(20),in_memberid VARCHAR(20),in_activation VARCHAR(20),in_placementid VARCHAR(20),in_nama VARCHAR(50),in_hp VARCHAR(20),in_email VARCHAR(50),in_whsid INTEGER,in_password_enc VARCHAR(50),in_pin_enc VARCHAR(50),in_hash VARCHAR(100),in_totalharga VARCHAR(50),in_kit VARCHAR(50), in_alamat VARCHAR(200), in_noktp VARCHAR(20), in_city VARCHAR(20), in_tempatlahir VARCHAR(20), in_tgllahir  VARCHAR(20), in_jk VARCHAR(20), in_so_id_fe VARCHAR(20), in_empid VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	DECLARE l_jenjang INTEGER DEFAULT 1;
	DECLARE l_posisi,l_soid,l_whsid,l_deposit_id,l_wdr_id,l_parentid,l_lft,l_rgt INTEGER DEFAULT 0;
	START TRANSACTION;
	
	SELECT n.id,n.leftval,n.rightval,m.posisi INTO l_parentid,l_lft,l_rgt,l_posisi 
			FROM networks n
			LEFT JOIN member m ON n.member_id=m.id
			WHERE n.member_id=in_placementid LIMIT 0,1;
			
	INSERT INTO member(id,jenjang_id,tgljenjang,jenjang_id2,highjenjang,sponsor_id,enroller_id,stockiest_id,nama,alamat, noktp,hp,email,kota_id, tempatlahir, tglaplikasi,posisi,kit,created,createdby)
	VALUES
	 		(in_memberid,l_jenjang,LAST_DAY(DATE_ADD(CURDATE(),INTERVAL 11 MONTH)),l_jenjang,l_jenjang,in_placementid,in_introducerid,'0',in_nama,in_alamat, in_noktp,in_hp,in_email,in_city, in_tempatlahir, CURDATE(),l_posisi+1,in_kit,NOW(),in_empid);
	UPDATE networks SET leftval = leftval + 2 WHERE leftval > l_rgt;
	UPDATE networks SET rightval = rightval + 2 WHERE rightval >= l_rgt;
	INSERT INTO networks(parentid,member_id,leftval,rightval)VALUES
			(l_parentid,in_memberid,l_rgt,l_rgt+1);
		
		INSERT INTO deposit(member_id,tunai,total,total_approved,approved,tgl_approved,approvedby,remark_fin,flag,warehouse_id,event_id,created,createdby)VALUES
			(in_memberid,in_totalharga,in_totalharga,in_totalharga,'approved',NOW(),'system','Register Member','mem',l_whsid,'DP1',NOW(),in_empid);
		SELECT LAST_INSERT_ID() INTO l_deposit_id;
		
		CALL sp_ewallet_mem('company',in_memberid,'Top Up MEM ECOMERCE',l_deposit_id,in_totalharga,in_empid);
		
		CALL sp_ewallet_mem(in_memberid,'company','SO KIT ECOMERCE',in_so_id_fe,in_totalharga,in_empid);
		
		COMMIT;
END