/*
 Navicat Premium Data Transfer

 Source Server         : omegaciptasolusi.com
 Source Server Type    : MySQL
 Source Server Version : 100323
 Source Host           : omegaciptasolusi.com:3306
 Source Schema         : omegacip_soho

 Target Server Type    : MySQL
 Target Server Version : 100323
 File Encoding         : 65001

 Date: 13/08/2020 18:54:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for log_api_ecommerce
-- ----------------------------
DROP TABLE IF EXISTS `log_api_ecommerce`;
CREATE TABLE `log_api_ecommerce`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `input` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `response` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `ip_address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2551 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
