CREATE PROCEDURE `sp_so_temp_tail`(in_id INTEGER, in_whsid VARCHAR(20), in_empid VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	DECLARE so_id_, nc_id_, jml_ INTEGER DEFAULT 0;
	DECLARE jml1_, jml2_, jml3_, jml4_, jml5_, jml6_, jml7_, jml8_, jml9_, jml10_, jml11_, stock_qty_ INTEGER DEFAULT 0;
	START TRANSACTION;
	
	UPDATE promo SET expired = 1 WHERE end_periode < (NOW()-INTERVAL 1 DAY) AND expired < 1;
		
	block_cur:BEGIN
	DECLARE member_id_, remark_, item_bns_,item_id_ VARCHAR(35);
	DECLARE oms_, hpp_ DOUBLE DEFAULT 0;
	DECLARE tgl_ DATETIME DEFAULT 0;
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE target_ DOUBLE DEFAULT 1500000;
	DECLARE qty_bns_ DOUBLE DEFAULT 1;
	DECLARE whs_id_ INTEGER DEFAULT 1;
	
	DECLARE cursor1 CURSOR FOR
		SELECT dt.id, dt.item_id, dt.member_id, dt.free_item_id,dt.hpp, MAX(dt.free_qty) AS free_qty, whs_id
		FROM (
			SELECT temp_so.id, prm.id AS promo_id, prm.item_id, temp_so.member_id, prm.free_item_id, i.hpp
				
				, CASE WHEN multiples = 1 THEN (FLOOR(SUM(sod.qty) / prm.item_qty) * prm.free_item_qty) 
				WHEN FLOOR(sod.qty / prm.item_qty) > 0 THEN prm.free_item_qty
				ELSE 0 END AS free_qty
				
				, CASE WHEN prm.warehouse_id = 0 AND i.warehouse_id = 0 THEN in_whsid 
				       WHEN prm.warehouse_id = 0 AND i.warehouse_id > 0 THEN i.warehouse_id
				       WHEN prm.warehouse_id = 99 THEN in_whsid
				       ELSE prm.warehouse_id
				       END AS whs_id
				
			FROM temp_so
			LEFT JOIN temp_so_d sod ON temp_so.id = sod.so_id
			LEFT JOIN(
				SELECT *
				FROM promo
				WHERE (term = 1 OR term = 3)
					AND (end_periode + INTERVAL 1 DAY) > NOW()
					AND expired = 0
			)AS prm ON sod.item_id = prm.item_id
			LEFT JOIN item i ON prm.free_item_id = i.id
			WHERE temp_so.id = in_id
			GROUP BY prm.id 
			HAVING free_qty > 0
		) AS dt 
		GROUP BY dt.item_id, dt.free_item_id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done :=1;
	
	OPEN cursor1;
	loop1:LOOP FETCH cursor1 INTO so_id_, item_id_, member_id_, item_bns_, hpp_, qty_bns_, whs_id_;
		IF done THEN
			CLOSE cursor1;
			LEAVE loop1;
		END IF;
		
		
		SELECT 
		qty INTO stock_qty_	
		FROM stock s
		WHERE s.warehouse_id = whs_id_ AND s.item_id = item_bns_
		;
		
		IF (stock_qty_ >= qty_bns_ ) THEN
			SET remark_ = CONCAT("Free of SO No.", in_id);
			INSERT INTO `temp_ncm`(`id`,`warehouse_id`,`date`,`remark`,`created`,`createdby`)
				VALUES(NULL,whs_id_,NOW(),remark_,NOW(),in_empid);
			SELECT LAST_INSERT_ID() INTO nc_id_;
			INSERT INTO `temp_ncm_d`(`id`,`ncm_id`,`item_id`,`qty`,`hpp`,`event_id`,`flag`)
				VALUES(NULL, nc_id_, item_bns_, qty_bns_, hpp_, 'NC1', '0');
			INSERT INTO `temp_ref_trx_nc`(`id`,`periode`,`member_id`,`trx`,`trx_id`,`nc_id`,`created_date`)
				VALUES(NULL,NOW(),member_id_,'SO',in_id,nc_id_,CURRENT_TIMESTAMP);
			CALL sp_inventory_temp_member(whs_id_,nc_id_,'D','NC',item_bns_,qty_bns_,in_empid);
		END IF;
		
	END LOOP loop1;
	END block_cur;
	
	CALL sp_so_temp_tail2(in_id, in_whsid, in_empid);
	CALL sp_so_temp_tail3(in_id);
	CALL sp_so_temp_tail_value(in_id, in_whsid, in_empid);
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-04-01' AND '2015-04-30'
	AND ncd.item_id = 'BY030010';
	
	IF jml_ >= 500 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'BY030010'
		AND expired = 0;
	END IF;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml1_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'HF060009';
	
	IF jml1_ >= 400 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'HF060009'
		AND expired = 0;
	END IF;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml2_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'NT020004';
	
	IF jml2_ >= 450 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'NT020004'
		AND expired = 0;
	END IF;
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml3_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'NT020001';
	
	IF jml3_ >= 700 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'NT020001'
		AND item_id = 'PP080078'
		AND expired = 0;
	END IF;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml4_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'HF060001';
	
	IF jml4_ >= 300 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'HF060001'
		AND expired = 0;
	END IF;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml5_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'HF060007';
	
	IF jml5_ >= 200 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'HF060007'
		AND expired = 0;
	END IF;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml6_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'NT020009';
	
	IF jml6_ >= 300 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'NT020009'
		AND expired = 0;
	END IF;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml7_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-06-01' AND '2015-06-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'GM090022';
	
	IF jml7_ >= 190 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'GM090022'
		AND expired = 0;
	END IF;
	COMMIT;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml8_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-07-01' AND '2015-07-31'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'GM090023';
	
	IF jml8_ >= 196 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'GM090023'
		AND expired = 0;
	END IF;
	COMMIT;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml9_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-07-01' AND '2015-07-31'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'OP050003';
	
	IF jml9_ >= 32 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'OP050003'
		AND expired = 0;
	END IF;
	COMMIT;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml10_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2015-11-01' AND '2015-11-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'GM090027';
	
	IF jml10_ >= 50 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'GM090027'
		AND expired = 0;
	END IF;
	COMMIT;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml11_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2017-09-13' AND '2017-09-14'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'MT0143';
	
	IF jml11_ >= 1550 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'MT0143'
		AND expired = 0;
	END IF;
	COMMIT;
	
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml11_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2017-09-19' AND '2017-09-30'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'MT0143';
	
	IF jml11_ >= 1900 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'MT0143'
		AND expired = 0;
	END IF;
	COMMIT;
	SELECT IFNULL(SUM(qty),0)AS jml INTO jml11_
	FROM temp_ncm nc
	LEFT JOIN temp_ncm_d ncd ON nc.id = ncd.ncm_id
	WHERE nc.date BETWEEN '2017-10-01' AND '2017-12-31'
	AND remark LIKE 'Free of SO No.%'
	AND ncd.item_id = 'MT0143';
	
	IF jml11_ >= 200 THEN
		UPDATE promo
		SET expired = '1'
		WHERE free_item_id = 'MT0143'
		AND expired = 0;
	END IF;
	COMMIT;
END