ALTER TABLE stock ADD qty_reserved INT(11) NULL;
ALTER TABLE item ADD product_id_fe INT(11) NULL;
ALTER TABLE bank ADD bank_id_fe INT(11) NULL;
ALTER TABLE kota ADD reg_position VARCHAR(20) NULL;
ALTER TABLE kota ADD id_fe INT(11) NULL;
ALTER TABLE temp_member ADD member_id_fe VARCHAR(20) NULL;
ALTER TABLE temp_member ADD address_id_fe VARCHAR(20) NULL;
ALTER TABLE bank ADD bank_id_fe  INT(11) NULL;
ALTER TABLE deposit ADD deposit_id_fe  INT(11) NULL;

UPDATE kota SET reg_position=0;
UPDATE stock SET qty_reserved=0;

ALTER TABLE member ADD member_id_fe VARCHAR(20) NULL;
ALTER TABLE member ADD address_id_fe VARCHAR(20) NULL;
ALTER TABLE member_delivery ADD id_fe INT(11) NULL;
ALTER TABLE temp_so ADD payment_type INT(11) NULL;
ALTER TABLE so ADD so_id_fe  INT(11) NULL;

ALTER TABLE payment_confirmation ADD remark TEXT(0) NULL;
ALTER TABLE payment_confirmation ADD approvedby VARCHAR(20) NULL;
ALTER TABLE payment_confirmation ADD tglapproved DATETIME(6) NULL;

ALTER TABLE so ADD shipping_price DOUBLE DEFAULT 0;

/** Update SP Insert From Temp */
DELIMITER $$

DROP PROCEDURE IF EXISTS`sp_insert_so_from_temp`;

CREATE PROCEDURE `sp_insert_so_from_temp`(in_member_id VARCHAR(20),in_so_id_fe VARCHAR(20),in_so_id_be VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	DECLARE l_soid integer DEFAULT 0;
	DECLARE l_ncmid integer DEFAULT 0;
	DECLARE l_totalharga VARCHAR(20) DEFAULT 0;
	DECLARE l_shipping_price VARCHAR(20) DEFAULT 0;
	
	START TRANSACTION;
	
	INSERT INTO so(tgl,stockiest_id,member_id,totalharga,totalharga_,totalpv,totalbv,kit,remark,warehouse_id,delivery_addr,created,createdby,STATUS,status1,status2, shipping_price, so_id_fe)
  SELECT tgl,stockiest_id,member_id,totalharga,totalharga_,totalpv,totalbv,kit,remark,warehouse_id,delivery_addr_id,created,createdby,STATUS,status1,status2, shipping_price, so_id_fe FROM temp_so WHERE id=in_so_id_be AND member_id=in_member_id AND so_id_fe=in_so_id_fe;

SELECT LAST_INSERT_ID() INTO l_soid;

INSERT INTO so_d(so_id,item_id,qty,harga,harga_,pv,bv,jmlharga,jmlharga_,jmlpv,jmlbv,event_id,warehouse_id)
 SELECT l_soid,item_id,qty,harga,harga_,pv,bv,jmlharga,jmlharga_,jmlpv,jmlbv,event_id,warehouse_id FROM temp_so_d WHERE so_id=in_so_id_be AND so_id_fe=in_so_id_fe;
 
	SELECT totalharga into l_totalharga FROM so WHERE id = l_soid limit 0,1;
	SELECT shipping_price into l_shipping_price FROM so WHERE id = l_soid limit 0,1;

	SELECT l_soid as so_id_be, l_totalharga as total_harga, l_shipping_price as shipping_price;
	
		COMMIT;
END

// Update SP Signup

DELIMITER $$

DROP PROCEDURE IF EXISTS`sp_signup`;

CREATE PROCEDURE `sp_signup`(in_introducerid VARCHAR(10),in_memberid VARCHAR(10),in_activation VARCHAR(20),in_placementid VARCHAR(10),in_nama VARCHAR(50),in_hp VARCHAR(20),in_email VARCHAR(50),in_whsid INTEGER,in_password_enc VARCHAR(50),in_pin_enc VARCHAR(50),in_hash VARCHAR(100),in_questionid INTEGER,in_empid VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	DECLARE l_jenjang INTEGER DEFAULT 1;
	DECLARE l_posisi,l_soid,l_whsid,l_deposit_id,l_wdr_id,l_parentid,l_lft,l_rgt INTEGER DEFAULT 0;
	DECLARE l_harga,l_pv,l_bv DOUBLE DEFAULT 0;
	DECLARE harga1_ DOUBLE DEFAULT 0;
	DECLARE l_stcid, l_item, status_, status1_, status2_ VARCHAR(10);
	START TRANSACTION;
	
	INSERT INTO users(id,username,PASSWORD,pin,HASH,group_id,warehouse_id,questions_id,activation_code)VALUES(in_memberid,in_memberid,in_password_enc,in_pin_enc,in_hash,101,in_whsid,in_questionid,in_activation);
	UPDATE activation_code SET STATUS='used',useddate=NOW() WHERE member_id=in_memberid AND CODE=in_activation;
	
	SELECT stockiest_id,warehouse_id,item_id INTO l_stcid,l_whsid,l_item FROM activation_code WHERE member_id=in_memberid AND CODE=in_activation AND STATUS='used';
	SELECT price,pv,bv INTO l_harga, l_pv, l_bv FROM item WHERE id = l_item;
	
	IF l_stcid = 0 THEN
		SET harga1_ = l_harga;
		SET status_ = 'pending';
		SET status1_ = 'pending';
		SET status2_ = 'pending';
	ELSE
		SELECT harga INTO harga1_ FROM titipan WHERE item_id = l_item AND member_id = l_stcid AND qty > 0 ORDER BY qty LIMIT 1;
		SET status_ = 'delivery';
		SET status1_ = 'delivery';
		SET status2_ = 'delivery';
	END IF;
	
	INSERT INTO so(tgl,stockiest_id,member_id,totalharga,totalharga_,totalpv,totalbv,kit,remark,warehouse_id,created,createdby,STATUS,status1,status2)
		VALUES(CURDATE(),l_stcid,in_memberid,harga1_,l_harga,l_pv,l_bv,'Y','SO KIT',l_whsid,NOW(),in_empid,status_,status1_,status2_);
	SELECT LAST_INSERT_ID() INTO l_soid; 
	INSERT INTO so_d(so_id,item_id,qty,harga,harga_,pv,bv,jmlharga,jmlharga_,jmlpv,jmlbv,event_id,warehouse_id)
		VALUES(l_soid,l_item,1,harga1_,l_harga,l_pv,l_bv,l_harga,harga1_,l_pv,l_bv,'SO4',l_whsid);
	
	IF l_stcid = 0 THEN
		CALL sp_so_tail(l_soid, l_whsid, 'system');
	end if;
	
	SELECT n.id,n.leftval,n.rightval,m.posisi INTO l_parentid,l_lft,l_rgt,l_posisi 
			FROM networks n
			LEFT JOIN member m ON n.member_id=m.id
			WHERE n.member_id=in_placementid LIMIT 0,1;
	INSERT INTO member(id,jenjang_id,tgljenjang,jenjang_id2,highjenjang,sponsor_id,enroller_id,stockiest_id,nama,hp,email,tglaplikasi,posisi,kit,created,createdby)VALUES
	 		(in_memberid,l_jenjang,LAST_DAY(DATE_ADD(CURDATE(),INTERVAL 11 MONTH)),l_jenjang,l_jenjang,in_placementid,in_introducerid,l_stcid,in_nama,in_hp,in_email,CURDATE(),l_posisi+1,l_item,NOW(),in_empid);
	UPDATE networks SET leftval = leftval + 2 WHERE leftval > l_rgt;
	UPDATE networks SET rightval = rightval + 2 WHERE rightval >= l_rgt;
	INSERT INTO networks(parentid,member_id,leftval,rightval)VALUES
			(l_parentid,in_memberid,l_rgt,l_rgt+1);
	
	
	IF l_whsid > 0 THEN
		CALL sp_stock_summary_check(l_whsid);
		CALL sp_inventory(l_whsid,l_soid,'D','SO KIT',l_item,1,in_empid);
		
		INSERT INTO deposit(member_id,tunai,total,total_approved,approved,tgl_approved,approvedby,remark_fin,flag,warehouse_id,event_id,created,createdby)VALUES
			(in_memberid,l_harga,l_harga,l_harga,'approved',NOW(),'system','Register Member','mem',l_whsid,'DP1',NOW(),in_empid);
		SELECT LAST_INSERT_ID() INTO l_deposit_id; 
		CALL sp_ewallet_mem('company',in_memberid,'Top Up MEM',l_deposit_id,l_harga,in_empid);
		
		CALL sp_ewallet_mem(in_memberid,'company','SO KIT',l_soid,l_harga,in_empid);
	ELSE
		CALL sp_titipan_stc(l_soid,l_stcid,l_item,1,harga1_,l_pv,'out','SO KIT',in_empid);
	END IF;
		
		SELECT l_soid as so_id_be;
		
		COMMIT;
END



DELIMITER $$

DROP PROCEDURE IF EXISTS`sp_ewallet_mem_ecommerce`;

CREATE PROCEDURE `sp_ewallet_mem_ecommerce`(in_debet varchar(10),in_kredit varchar(10),in_desc varchar(50),in_noref integer,in_nominal decimal(14,2),in_empid varchar(20), in_nominal_trx decimal(14,2))
    MODIFIES SQL DATA
BEGIN
	declare l_ewallet,l_totalewallet decimal(14,2) default 0.00;
	declare l_tgl date DEFAULT now();
	declare l_count,l_id INT(8) default 0;
	declare l_debet varchar(5);
	declare l_kredit varchar(5);
	declare l_date date default now();
	START TRANSACTION;
	
	 
	if in_debet = 'company' then 
		SELECT saldoakhir into l_ewallet FROM ewallet_mem WHERE member_id=in_debet order by id desc limit 0,1;
				SELECT COUNT(*) INTO l_count FROM ewallet_mem WHERE member_id=in_debet AND date=l_tgl;
		IF l_count=0 THEN 
			INSERT INTO ewallet_mem(date,member_id,saldoawal,debet,saldoakhir)VALUES(now(),in_debet,l_ewallet,in_nominal_trx,l_ewallet - in_nominal_trx);
		ELSE
			UPDATE ewallet_mem SET debet = debet + in_nominal_trx, saldoakhir=saldoakhir - in_nominal_trx WHERE member_id=in_debet AND date=l_tgl; 
		END IF;
		SELECT id into l_id FROM ewallet_mem WHERE member_id=in_debet AND date=l_tgl limit 0,1;
		INSERT INTO ewallet_mem_d (ewallet_mem_id,member_id,noref,description,saldoawal,debet,saldoakhir,created,createdby)VALUES
		(l_id,in_debet,in_noref,in_desc,l_ewallet,in_nominal_trx,l_ewallet - in_nominal_trx,now(),in_empid);
	else 
		SELECT ewallet into l_ewallet FROM member WHERE id = in_debet;
		UPDATE member SET ewallet = ewallet - in_nominal_trx WHERE id= in_debet;
				SELECT COUNT(*) INTO l_count FROM ewallet_mem WHERE member_id = in_debet AND date=l_tgl;
		IF l_count=0 THEN 
			INSERT INTO ewallet_mem(date,member_id,saldoawal,debet,saldoakhir)VALUES(now(),in_debet,l_ewallet,in_nominal_trx,l_ewallet - in_nominal);
		ELSE
			UPDATE ewallet_mem SET debet = debet + in_nominal_trx, saldoakhir=saldoakhir - in_nominal_trx WHERE member_id=in_debet AND date=l_tgl; 
		END IF; 
		SELECT id into l_id FROM ewallet_mem WHERE member_id = in_debet AND date=l_tgl limit 0,1;
		INSERT INTO ewallet_mem_d (ewallet_mem_id,member_id,noref,description,saldoawal,debet,saldoakhir,created,createdby)VALUES
		(l_id,in_debet,in_noref,in_desc,l_ewallet,in_nominal_trx,l_ewallet - in_nominal_trx,now(),in_empid);
	end if;
	
	if in_kredit = 'company' then 
		SELECT saldoakhir into l_ewallet FROM ewallet_mem WHERE member_id=in_kredit order by id desc limit 0,1;
				SELECT COUNT(*) INTO l_count FROM ewallet_mem WHERE member_id=in_kredit AND date=l_tgl;
		IF l_count=0 THEN 
			INSERT INTO ewallet_mem(date,member_id,saldoawal,kredit,saldoakhir)VALUES(now(),in_kredit,l_ewallet,in_nominal,l_ewallet + in_nominal);
		ELSE
			UPDATE ewallet_mem SET kredit = kredit + in_nominal, saldoakhir=saldoakhir + in_nominal WHERE member_id=in_kredit AND date=l_tgl; 
		END IF;
		SELECT id into l_id FROM ewallet_mem WHERE member_id=in_kredit AND date=l_tgl limit 0,1;
		INSERT INTO ewallet_mem_d (ewallet_mem_id,member_id,noref,description,saldoawal,kredit,saldoakhir,created,createdby)VALUES
			(l_id,in_kredit,in_noref,in_desc,l_ewallet,in_nominal,l_ewallet + in_nominal,now(),in_empid);
	
	else
		SELECT ewallet into l_ewallet FROM member WHERE id = in_kredit;
		UPDATE member SET ewallet = ewallet + in_nominal WHERE id=in_kredit;
		SELECT saldoakhir into l_ewallet FROM ewallet_mem WHERE member_id=in_kredit order by id desc limit 0,1;
				SELECT COUNT(*) INTO l_count FROM ewallet_mem WHERE member_id=in_kredit AND date=l_tgl;
		IF l_count=0 THEN 
			INSERT INTO ewallet_mem(date,member_id,saldoawal,kredit,saldoakhir)VALUES(now(),in_kredit,l_ewallet,in_nominal,l_ewallet + in_nominal);
		ELSE
			UPDATE ewallet_mem SET kredit = kredit + in_nominal, saldoakhir=saldoakhir + in_nominal WHERE member_id=in_kredit AND date=l_tgl; 
		END IF;
		SELECT id into l_id FROM ewallet_mem WHERE member_id=in_kredit AND date=l_tgl limit 0,1;
		INSERT INTO ewallet_mem_d (ewallet_mem_id,member_id,noref,description,saldoawal,kredit,saldoakhir,created,createdby)VALUES
			(l_id,in_kredit,in_noref,in_desc,l_ewallet,in_nominal,l_ewallet + in_nominal,now(),in_empid);
	end if;
	COMMIT;
END

INSERT INTO menu(submenu_id, title, folder, url, comment, sort)
VALUES(7, 'Payment Confirmation Ecommerce',	'fin', 'payment_confirm_ecommerce', '', 15);

INSERT INTO menu(submenu_id, title, folder, url, comment, sort)
VALUES(6, 'Temp Order',	'order', 'temp_order', '', 32);

DELIMITER $$;

DROP PROCEDURE IF EXISTS`sp_inventory_payment_confirm`;

CREATE PROCEDURE `sp_inventory_payment_confirm`(in_whsid integer,in_descid integer,in_dk char(1),in_desc varchar(50),in_itemid varchar(10),in_qty integer,in_empid varchar(20))
    MODIFIES SQL DATA
begin
	declare l_saldoawal integer default 0;
	declare l_saldoawal_r integer default 0;
	declare l_date date default now();
	start transaction;
	
	select qty into l_saldoawal from stock where warehouse_id=in_whsid and item_id=in_itemid limit 0,1; 
	select qty_reserved into l_saldoawal_r from stock where warehouse_id=in_whsid and item_id=in_itemid limit 0,1; 
	
	if in_dk = 'K' then
		update stock set qty=qty + in_qty, qty_reserved=qty_reserved-in_qty where warehouse_id=in_whsid and item_id=in_itemid;
		insert into stock_card(desc_id,date,description,warehouse_id,item_id,saldo_awal,saldo_in,saldo_akhir,created,createdby)
			values(in_descid,curdate(),in_desc,in_whsid,in_itemid,l_saldoawal,in_qty,l_saldoawal + in_qty,now(),in_empid);
		update stock_summary set saldoin=saldoin + in_qty,saldoakhir=saldoakhir + in_qty where warehouse_id=in_whsid and item_id=in_itemid and tgl=l_date;
	else
		update stock set qty_reserved=qty_reserved-in_qty where warehouse_id=in_whsid and item_id=in_itemid;
	end if;	
	commit; 
end;

DELIMITER $$;

DROP PROCEDURE IF EXISTS `sp_inventory_payment_confirm_ncm`;

CREATE PROCEDURE `sp_inventory_payment_confirm_ncm`(in_whsid integer,in_descid integer,in_dk char(1),in_desc varchar(50),in_itemid varchar(10),in_qty integer,in_empid varchar(20))
    MODIFIES SQL DATA
begin
	declare l_saldoawal integer default 0;
	declare l_saldoawal_r integer default 0;
	declare l_date date default now();
	start transaction;
	
	select qty into l_saldoawal from stock where warehouse_id=in_whsid and item_id=in_itemid limit 0,1; 
	select qty_reserved into l_saldoawal_r from stock where warehouse_id=in_whsid and item_id=in_itemid limit 0,1; 
	
	if in_dk = 'K' then
		update stock set qty=qty + in_qty, qty_reserved=qty_reserved-in_qty where warehouse_id=in_whsid and item_id=in_itemid;
		insert into stock_card(desc_id,date,description,warehouse_id,item_id,saldo_awal,saldo_in,saldo_akhir,created,createdby)
			values(in_descid,curdate(),in_desc,in_whsid,in_itemid,l_saldoawal,in_qty,l_saldoawal + in_qty,now(),in_empid);
		update stock_summary set saldoin=saldoin + in_qty,saldoakhir=saldoakhir + in_qty where warehouse_id=in_whsid and item_id=in_itemid and tgl=l_date;
	else
		update stock set qty_reserved=qty_reserved-in_qty where warehouse_id=in_whsid and item_id=in_itemid;
	end if;	
	commit; 
end;

DELIMITER $$;

DROP PROCEDURE IF EXISTS `sp_inventory_temp_member`;

CREATE  PROCEDURE `sp_inventory_temp_member`(in_whsid INTEGER,in_descid INTEGER,in_dk CHAR(1),in_desc VARCHAR(50),in_itemid VARCHAR(10),in_qty INTEGER,in_empid VARCHAR(20))
    MODIFIES SQL DATA
begin
	declare l_saldoawal integer default 0;
	declare l_saldoawal_r integer default 0;
	declare l_date date default now();
	start transaction;
	
	select qty into l_saldoawal from stock where warehouse_id=in_whsid and item_id=in_itemid limit 0,1; 
	select qty_reserved into l_saldoawal_r from stock where warehouse_id=in_whsid and item_id=in_itemid limit 0,1; 
	
	if in_dk = 'K' then
		update stock set qty=qty + in_qty, qty_reserved=qty_reserved-in_qty where warehouse_id=in_whsid and item_id=in_itemid;
		insert into stock_card(desc_id,date,description,warehouse_id,item_id,saldo_awal,saldo_in,saldo_akhir,created,createdby)
			values(in_descid,curdate(),in_desc,in_whsid,in_itemid,l_saldoawal,in_qty,l_saldoawal + in_qty,now(),in_empid);
		update stock_summary set saldoin=saldoin + in_qty,saldoakhir=saldoakhir + in_qty where warehouse_id=in_whsid and item_id=in_itemid and tgl=l_date;
	else
		update stock set qty=qty - in_qty, qty_reserved=qty_reserved+in_qty where warehouse_id=in_whsid and item_id=in_itemid;
		insert into stock_card(desc_id,date,description,warehouse_id,item_id,saldo_awal,saldo_out,saldo_akhir,created,createdby)
			values(in_descid,curdate(),in_desc,in_whsid,in_itemid,l_saldoawal,in_qty,l_saldoawal - in_qty,now(),in_empid);
		update stock_summary set saldoout=saldoout + in_qty,saldoakhir=saldoakhir - in_qty where warehouse_id=in_whsid and item_id=in_itemid and tgl=l_date;
	end if;	
	commit; 
end


//Update Bank

UPDATE bank SET bank_id_fe = 5499 WHERE name = 'BANK CENTRAL ASIA';
UPDATE bank SET bank_id_fe = 5502 WHERE name = 'MANDIRI';
UPDATE bank SET bank_id_fe = 5501 WHERE name = 'BANK RAKYAT INDONESIA';
UPDATE bank SET bank_id_fe = 5500 WHERE name = 'BANK NEGARA INDONESIA';
UPDATE bank SET bank_id_fe = 5471 WHERE name = 'Bank Jawa Barat';
UPDATE bank SET bank_id_fe = 5472 WHERE name = 'BANK SUMATERA SELATAN';
UPDATE bank SET bank_id_fe = 5473 WHERE name = 'BANK INTERNATIONAL INDONESIA';
UPDATE bank SET bank_id_fe = 5474 WHERE name = 'EX LIPPO BANK';
UPDATE bank SET bank_id_fe = 5475 WHERE name = 'Bank Danamon';
UPDATE bank SET bank_id_fe = 5476 WHERE name = 'Bank Muamalat';
UPDATE bank SET bank_id_fe = 5477 WHERE name = 'Bank Capital';
UPDATE bank SET bank_id_fe = 5478 WHERE name = 'Bank Buana';
UPDATE bank SET bank_id_fe = 5479 WHERE name = 'Bank Ekonomi';
UPDATE bank SET bank_id_fe = 5480 WHERE name = 'BNI SYARIAH';
UPDATE bank SET bank_id_fe = 5481 WHERE name = 'Bank Panin';
UPDATE bank SET bank_id_fe = 5482 WHERE name = 'BANK SUMSEL SYARIAH';
UPDATE bank SET bank_id_fe = 5483 WHERE name = 'Bank Mega';
UPDATE bank SET bank_id_fe = 5484 WHERE name = 'BPD';
UPDATE bank SET bank_id_fe = 5470 WHERE name = 'BTN';
UPDATE bank SET bank_id_fe = 5485 WHERE name = 'Bank Permata';
UPDATE bank SET bank_id_fe = 5486 WHERE name = 'UOB';
UPDATE bank SET bank_id_fe = 5487 WHERE name = 'BUKOPIN';
UPDATE bank SET bank_id_fe = 5488 WHERE name = 'Bank BPN';
UPDATE bank SET bank_id_fe = 5489 WHERE name = 'Mandiri Syariah';
UPDATE bank SET bank_id_fe = 5490 WHERE name = 'Bank Nagari';
UPDATE bank SET bank_id_fe = 5491 WHERE name = 'CIMB Niaga';
UPDATE bank SET bank_id_fe = 5492 WHERE name = 'OCBC NISP';
UPDATE bank SET bank_id_fe = 5493 WHERE name = 'Bank Batara';
UPDATE bank SET bank_id_fe = 5494 WHERE name = 'BPR Harau';
UPDATE bank SET bank_id_fe = 5495 WHERE name = 'Bank Sumut';
UPDATE bank SET bank_id_fe = 5496 WHERE name = 'Bank DKI';
UPDATE bank SET bank_id_fe = 5497 WHERE name = 'Bank BTPN';
UPDATE bank SET bank_id_fe = 5498 WHERE name = 'Bank Syariah';
UPDATE bank SET bank_id_fe = 5503 WHERE name = 'BPR Surya Yudha';
UPDATE bank SET bank_id_fe = 5504 WHERE name = 'BRI Syariah';
UPDATE bank SET bank_id_fe = 5505 WHERE name = 'Bank Jatim';
UPDATE bank SET bank_id_fe = 5506 WHERE name = 'Bank Rabo';
UPDATE bank SET bank_id_fe = 5507 WHERE name = 'Bank Mestika';
UPDATE bank SET bank_id_fe = 5508 WHERE name = 'Bank Jawa Barat';
UPDATE bank SET bank_id_fe = 5509 WHERE name = 'BSM';
UPDATE bank SET bank_id_fe = 5510 WHERE name = 'Bank Riau Kepri';
UPDATE bank SET bank_id_fe = 5511 WHERE name = 'Bank Riau';
UPDATE bank SET bank_id_fe = 5512 WHERE name = 'Bank Sinar Mas';
UPDATE bank SET bank_id_fe = 5513 WHERE name = 'BRI-Britama';
UPDATE bank SET bank_id_fe = 5514 WHERE name = 'Bank BPR';
UPDATE bank SET bank_id_fe = 5515 WHERE name = 'Bank Jawa Tengah';
UPDATE bank SET bank_id_fe = 5516 WHERE name = 'Bank BPD Aceh';
UPDATE bank SET bank_id_fe = 5517 WHERE name = 'BCA Syariah';
UPDATE bank SET bank_id_fe = 5518 WHERE name = 'BRI SIMPEDES';
UPDATE bank SET bank_id_fe = 5519 WHERE name = 'Bank Kalimantan';
UPDATE bank SET bank_id_fe = 5520 WHERE name = 'HSBC';
UPDATE bank SET bank_id_fe = 5521 WHERE name = 'Bank KalBar';
UPDATE bank SET bank_id_fe = 5522 WHERE name = 'Bank KalTim';
UPDATE bank SET bank_id_fe = 5523 WHERE name = 'Bank Arta Graha';
UPDATE bank SET bank_id_fe = 5524 WHERE name = 'Bank Papua';
UPDATE bank SET bank_id_fe = 5525 WHERE name = 'Sulut';
UPDATE bank SET bank_id_fe = 5526 WHERE name = 'Bukopin Syariah';
UPDATE bank SET bank_id_fe = 5527 WHERE name = 'Bank Kaltim Syariah';
UPDATE bank SET bank_id_fe = 5528 WHERE name = 'Bank Pundi';
UPDATE bank SET bank_id_fe = 5529 WHERE name = 'BPD Kaltim';
UPDATE bank SET bank_id_fe = 5530 WHERE name = 'Bank 9 Jambi';
UPDATE bank SET bank_id_fe = 5531 WHERE name = 'BPD Jambi';
UPDATE bank SET bank_id_fe = 5532 WHERE name = 'Bank Mayora';
UPDATE bank SET bank_id_fe = 5533 WHERE name = 'Bank Commercial';
UPDATE bank SET bank_id_fe = 5534 WHERE name = 'BPD Syariah';
UPDATE bank SET bank_id_fe = 5535 WHERE name = 'Bank Sulawesi Utara';
UPDATE bank SET bank_id_fe = 5536 WHERE name = 'Bank Mega Syariah';
UPDATE bank SET bank_id_fe = 5537 WHERE name = 'Bank Bumi Artha';
UPDATE bank SET bank_id_fe = 5538 WHERE name = 'Bank Hasa Mitra ';
UPDATE bank SET bank_id_fe = 5539 WHERE name = 'Bank Sulselbar ';
UPDATE bank SET bank_id_fe = 5540 WHERE name = 'BNI Taplus';
UPDATE bank SET bank_id_fe = 5541 WHERE name = 'Bank Panin Junior';
UPDATE bank SET bank_id_fe = 5542 WHERE name = 'Bank Maspion';
UPDATE bank SET bank_id_fe = 5543 WHERE name = 'Koperasi Intidana';
UPDATE bank SET bank_id_fe = 5544 WHERE name = 'Bank Sulteng';
UPDATE bank SET bank_id_fe = 5545 WHERE name = 'Bank Commonwealth';
UPDATE bank SET bank_id_fe = 5546 WHERE name = 'BTN BATARA';
UPDATE bank SET bank_id_fe = 5547 WHERE name = 'Bank Mayapada';
UPDATE bank SET bank_id_fe = 5548 WHERE name = 'Bank Bahteramas';
UPDATE bank SET bank_id_fe = 5549 WHERE name = 'BPD DIY';
UPDATE bank SET bank_id_fe = 5550 WHERE name = 'Bank BPD Kalsel';
UPDATE bank SET bank_id_fe = 5551 WHERE name = 'CITIBANK';
UPDATE bank SET bank_id_fe = 5552 WHERE name = 'Bank Kalteng';
UPDATE bank SET bank_id_fe = 5553 WHERE name = 'Bank Woori';
UPDATE bank SET bank_id_fe = 5554 WHERE name = 'Bank Maybank';
UPDATE bank SET bank_id_fe = 5555 WHERE name = 'Bank Multiarta Sentosa';
UPDATE bank SET bank_id_fe = 5556 WHERE name = 'Bank ICB Bumiputera';
UPDATE bank SET bank_id_fe = 5557 WHERE name = 'Bank Sampoerna';
UPDATE bank SET bank_id_fe = 5558 WHERE name = 'BPD KALSEL SYARIAH';
UPDATE bank SET bank_id_fe = 5559 WHERE name = 'BPD KalBar Syariah';
UPDATE bank SET bank_id_fe = 5560 WHERE name = 'BPD SULTRA';
UPDATE bank SET bank_id_fe = 5561 WHERE name = 'Bank Jasa Jakarta';
UPDATE bank SET bank_id_fe = 5562 WHERE name = 'Bank Nobu';
UPDATE bank SET bank_id_fe = 5563 WHERE name = 'BANKSULUTGO';
UPDATE bank SET bank_id_fe = 5564 WHERE name = 'Bank Banten';
UPDATE bank SET bank_id_fe = 5565 WHERE name = 'Bank Prima';
UPDATE bank SET bank_id_fe = 5566 WHERE name = 'KEB Hana Bank  ';
UPDATE bank SET bank_id_fe = 5567 WHERE name = 'BANK SUMSEL BABEL';
UPDATE bank SET bank_id_fe = 5568 WHERE name = 'Bank DBS';
UPDATE bank SET bank_id_fe = 5569 WHERE name = 'Bank Woori Saudara';
UPDATE bank SET bank_id_fe = 5570 WHERE name = 'BANK NTT';
UPDATE bank SET bank_id_fe = 5571 WHERE name = 'Bank 9 Jambi Syariah';
UPDATE bank SET bank_id_fe = 5572 WHERE name = 'Bank Maluku';
UPDATE bank SET bank_id_fe = 5573 WHERE name = 'BPD Kaltim Syariah';
UPDATE bank SET bank_id_fe = 5574 WHERE name = 'Jabar Banten Syariah';
UPDATE bank SET bank_id_fe = 5575 WHERE name = 'BCA ';
UPDATE bank SET bank_id_fe = 5578 WHERE name = 'Mandiri';
UPDATE bank SET bank_id_fe = 5577 WHERE name = 'BRI';
UPDATE bank SET bank_id_fe = 5576 WHERE name = 'BNI';
