CREATE PROCEDURE `sp_so_temp_tail2`(in_id INTEGER, in_whsid VARCHAR(20), in_empid VARCHAR(20))
    MODIFIES SQL DATA
BEGIN
	START TRANSACTION;
	
	UPDATE temp_so_d sod
	LEFT JOIN(
		SELECT sod.id, temp_so.statuspu, k.timur, price2
		FROM temp_so_d sod
		LEFT JOIN temp_so ON sod.so_id = temp_so.id
		LEFT JOIN member_delivery md ON temp_so.delivery_addr = md.id
		LEFT JOIN kota k ON md.kota = k.id
		LEFT JOIN item i ON sod.item_id=i.id 
		WHERE temp_so.statuspu = 0
		AND k.timur = 1
		AND sod.so_id= in_id
		AND sod.pv > 0
	)AS dt ON sod.id = dt.id
	LEFT JOIN temp_so ON sod.so_id = temp_so.id
	SET sod.harga_ = dt.price2
	WHERE sod.pv > 0
	AND sod.id = dt.id
	;
	
	block_cur:BEGIN
	DECLARE member_id_, remark_, item_bns_ VARCHAR(35);
	DECLARE so_id_, nc_id_ INTEGER DEFAULT 0;
	DECLARE oms_, hpp_ DOUBLE DEFAULT 0;
	DECLARE tgl_ DATETIME DEFAULT 0;
	DECLARE done BOOLEAN DEFAULT 0;
	DECLARE target_ DOUBLE DEFAULT 1500000;
	DECLARE qty_bns_ DOUBLE DEFAULT 1;
	
	DECLARE cursor1 CURSOR FOR
	
		SELECT temp_so.id, temp_so.member_id, sod.item_id, i.hpp
			, (FLOOR(sod.qty/10)*2) + FLOOR((sod.qty - (FLOOR(sod.qty/10)*10))/6) AS qtyBonus
		FROM temp_so
		RIGHT JOIN temp_so_d sod ON temp_so.id = sod.so_id
		LEFT JOIN item i ON sod.item_id = i.id
		WHERE temp_so.id = in_id
		AND temp_so.tgl BETWEEN '2014-07-01' AND '2014-07-31'
		AND item_id IN(
			'PP080093'
			, 'PP080082'
			, 'PC040006'
			, 'PP080076'
			, 'BY030010'
			, 'PP080092'
			, 'PP080081'
			, 'PP080078'
		);
		
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done :=1;
	
	OPEN cursor1;
	loop1:LOOP FETCH cursor1 INTO so_id_, member_id_, item_bns_, hpp_, qty_bns_;
		IF done THEN
			CLOSE cursor1;
			LEAVE loop1;
		END IF;
		
		SET remark_ = CONCAT("Free of SO no.", in_id);
		INSERT INTO `temp_ncm`(`id`,`warehouse_id`,`date`,`remark`,`created`,`createdby`)
			VALUES(NULL,in_whsid,NOW(),remark_,NOW(),in_empid);
		SELECT LAST_INSERT_ID() INTO nc_id_;
		INSERT INTO `temp_ncm_d`(`id`,`ncm_id`,`item_id`,`qty`,`hpp`,`event_id`,`flag`)
			VALUES(NULL, nc_id_, item_bns_, qty_bns_, hpp_, 'NC1', '0');
		INSERT INTO `temp_ref_trx_nc`(`id`,`periode`,`member_id`,`trx`,`trx_id`,`nc_id`,`created_date`)
			VALUES(NULL,NOW(),member_id_,'SO',in_id,nc_id_,CURRENT_TIMESTAMP);
		CALL sp_inventory_temp_member(in_whsid,nc_id_,'D','NC',item_bns_,qty_bns_,in_empid);
		
	END LOOP loop1;
	END block_cur;
	
	COMMIT;
END